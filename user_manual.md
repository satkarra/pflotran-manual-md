Introduction
============

PFLOTRAN solves a system of generally nonlinear partial differential
equations describing multiphase, multicomponent and multiscale reactive
flow and transport in porous materials. The code is designed to run on
massively parallel computing architectures as well as workstations and
laptops (e.g. Hammond et al., 2011). Parallelization is achieved through
domain decomposition using the PETSc (Portable Extensible Toolkit for
Scientific Computation) libraries for the parallelization framework
(Balay et al., 1997).

PFLOTRAN has been developed from the ground up for parallel scalability
and has been run on up to $2^{18}$ processor cores with problem sizes up
to 2 billion degrees of freedom. Written in object oriented Fortran 90,
the code requires the latest compilers compatible with Fortran 2003. At
the time of this writing this requires, for example, gcc 4.7.x or Intel
13.x.x or later. As a requirement of running problems with a large
number of degrees of freedom, PFLOTRAN allows reading input data that
would be too large to fit into memory if allocated to a single processor
core. The current limitation to the problem size PFLOTRAN can handle is
the limitation of the HDF5 file format used for parallel IO to 32 bit
integers. Noting that $2^{32} = 4,294,967,296$, this gives an estimate
of several billion degrees of freedom for the maximum problem size that
can be currently run with PFLOTRAN. Hopefully this limitation will be
remedied in the near future.

Currently PFLOTRAN can handle a number of subsurface processes involving
flow and transport in porous media including Richards equation,
two-phase flow involving supercritical $\mathrm{CO_2}$, and multicomponent
reactive transport including aqueous complexing, sorption and mineral
precipitation and dissolution. Reactive transport equations are solved
using a fully implicit Newton-Raphson algorithm. Operator splitting is
currently not implemented. In addition to single continuum processes, a
novel approach is used to solve equations resulting from a multiple
interacting continuum method for modeling flow and transport in
fractured media. This implementation is still under development.
Finally, an elastic geomechanical model is implemented.

A novel feature of the code is its ability to run multiple input files
and multiple realizations simultaneously, for example with different
permeability and porosity fields, on one or more processor cores per
run. This can be extremely useful when conducting sensitivity analyses
and quantifying model uncertainties. When running on machines with many
cores this means that hundreds of simulations can be conducted in the
amount of time needed for a single realization.

Additional information can be found on the PFLOTRAN [wiki home
page](https://bitbucket.org/pflotran/pflotran-dev/wiki/Home) which
should be consulted for the most up-to-date information. Questions
regarding installing PFLOTRAN on workstations, small clusters, and super
computers, and bug reports, may be directed to: `pflotran-users at googlegroups
dot com`. For questions
regarding running PFLOTRAN contact: `pflotran-users at googlegroups
dot com`.

Quick Start
===========

For those adverse to reading manuals the following is a quick start
guide to getting up and running with PFLOTRAN in four easy steps. On the
MacOSX platform Xcode must be installed with the commandline tools.

Required Software Packages
--------------------------

-   Compilers (compatible with Fortran 2003: gcc 4.7.x,
        Intel 14.x.x)
        For MacOSX 10.10.x the latest Fortran compiler binaries
        (gcc/gfortran 4.9 at the time of this writing) may be downloaded
        from the web site [HPC for MacOSX](http://hpc.sourceforge.net).

-   Mercurial (hg) (version control system)

-   CMAKE (version 3.0.x, needed for installing METIS and ParMETIS)

-   Developer version of PETSc:
    [petsc-dev](http://www.mcs.anl.gov/petsc/developers/index.html)

-   MPI (for running in parallel)

-   BLAS/LAPACK libraries

-   HDF5 (parallel output)

-   METIS/ParMETIS (unstructured grids)

-   [PFLOTRAN](https://bitbucket.org/pflotran/pflotran-dev)

Installing PFLOTRAN
-------------------

Step 1: Installing PETSc

The first step is to download the developer version of PETSc,
[petsc-dev](http://www.mcs.anl.gov/petsc/developers/index.html). To
do this follow the directions at the PETSc Developer web site. The
recommended approach is to use `git`.

To compile PETSc it is first necessary to configure the makefile. To
this end define the environmental variables `PETSC_DIR`
and `PETSC_ARCH` pointing to the directory where
petsc-dev is installed and an identifying name for the
installation, respectively. E.g., if using the t-shell on MacOSX
10.10.x and gcc-4.9 enter into the .tcshrc file:

    setenv PETSC_DIR "path to petsc-dev"

and

    setenv PETSC_ARCH MacOSX-10.10.x-gcc4.9.

Finally, set the environmental variable `MPI_HOME` and
add to the `PATH` variable:

    setenv MPI_HOME ${PETSC_DIR}/${PETSC_ARCH}
    setenv PATH $MPI_HOME/bin:$PATH

Decide which options are needed for running PFLOTRAN: structured
grids are the simplest; if unstructured grids are needed then
install METIS and ParMETIS. See Table [tinstall] for several of
the different configure options possible.

For example, to configure PETSC to run in production mode and
install openmpi, HDF5, METIS and ParMETIS use:

    cd ./petsc-dev

    ./config/configure.py --with-debugging=0 --with-shared-libraries=0 --with-x=0
    --download-openmpi=1 --download-hdf5=1 --download-metis=1 --download-parmetis=1

Check to make sure `mpicc` and `mpif90` are
working and are recent versions that are compatible with Fortran
2003: e.g. GNU gfortran version 4.7.x, Intel version 12.x or PGI.

| Option   |   Type           |   Package                                |   Command           |
| -------- | ---------------- | ---------------------------------------- | ------------------- |
| MPI      |Parallel          |[OpenMPI](http://www.open-mpi.org/)       | –download-openmpi=1 |
|          |                  |[MPICH](http://www.mpich.org/)            | –download-mpich=1   |
| Grid     |Structured        | --                                       |                     |       
|          |Unstructured      |[METIS](http://glaros.dtc.umn.edu/gkhome/views/metis) & [ParMETIS](http://glaros.dtc.umn.edu/gkhome/views/metis)$^*$|–download-metis=1 –download-parmetis=1|
| Output   |TecPlot, VTK      | -- ||
|          |HDF5              | Parallel [HDF5](http://www.hdfgroup.org/HDF5/) |–download-hdf5=1 |
| Solvers  |Iterative/Direct  | -- ||
|           |Parallel Direct   |[MUMPS](http://graal.ens-lyon.fr/MUMPS/)  | –download-mumps=1  |
|          |Multigrid         |[ML](http://trilinos.sandia.gov/packages/ml/)| –download-ml=1   |
|          |Preconditioners   |[Hypre](http://acts.nersc.gov/hypre/)     | –download-hypre=1  |


Step 2: Downloading PFLOTRAN
To obtain the latest version of the PFLOTRAN source code use
[Mercurial](http://mercurial.selenic.com/). Mercurial (hg) is the
version control system used by both PETSc and PFLOTRAN. Often one
can find executables for particular platforms and there is no need
to compile the application from scratch. On a UNIX system you can
check to see if Mercurial is already installed by issuing the
command:

    which hg.

PFLOTRAN can be downloaded from bitbucket.org by issuing the command
line:

    hg clone https://bitbucket.org/pflotran/pflotran-dev.

Step 3: Compiling PFLOTRAN
Once PETSc and associated third party software has been installed,
it is easy to install PFLOTRAN. To compile PFLOTRAN first decide on
the options needed to run your problem. The most common of these are
listed in Table [topt].

Option         Description
----------- ----------------
scco2=1     MPHASE mode for supercritical $\mathrm{CO_2}$
scorpio=1   SCORPIO$^{*}$ parallel IO

    cd PLOTRAN_DIR/src/pflotran
    make [options] ./pflotran

For example:

    make scco2=1 ./ppflotran

Step 4: Running PFLOTRAN
PFLOTRAN is run in parallel on `N` cores using the
command:

    mpirun -np N ./pflotran -pflotranin
    input_file_name.in

with input file `input_file_name.in`. The default input
file name is simply `pflotran.in`.

To get the most out of PFLOTRAN subscribe to the PFLOTRAN User Group:

    pflotran-users@googlegroups.com

PFLOTRAN developers monitor this site and will as quickly as possible
respond to your queries.

The final challenge is setting up an input file. Various modes available
are listed in Table 4 and example input decks are stored in the
`./pflotran/example_problems` and directories.

| MODE        |                            |    Required Databases$^*$
| ----------- | -------------------------- | ----------------------------------
| RICHARDS    |Solves Richards equation    |              —
| MPHASE      |Supercritcal $\mathrm{CO_2}$         |  `co2data0.dat`
| TH          |Thermal-Hydrology mode      |              —
| CHEMISTRY   |Reactive Transport          | `hanford.dat`$^{**}$

  : Available modes in PFLOTRAN

An example input file is listed in §[exinput] for a coupled problem
using Richards and Chemistry modes.

Running on Big Iron Parallel Architectures
------------------------------------------

Generally these machines use `module` to load the computing
environment. Make sure the correct compilers are loaded that are
compatible with Fortran 2003. The following instructions apply to
Yellowstone running Red Hat linux using the Intel compiler. As MPI is
already installed it is not necessary to reinstall it through PETSc.

Use a recent version of CMAKE: `module load cmake/2.8.10.2`.

Set the environmental variable for the BLAS/LAPACK libraries to use MKL:
e.g.

`setenv BLAS_LAPACK_LIB_DIR
/ncar/opt/intel/12.1.0.233/composer_xe_2013.1.117/mkl`

Configure PETSc:

    ./config/configure.py --with-cc=mpicc --with-fc=mpif90 --with-cxx=mpicxx
    --with-clanguage=c --with-blas-lapack-dir=$BLAS_LAPACK_LIB_DIR
    --with-shared-libraries=0 --with-debugging=0 --download-hdf5=yes
    --download-parmetis=yes --download-metis=yes
Installation
============

The source code for PFLOTRAN can be downloaded from the bitbucket.org
web site using [Mercurial](http://mercurial.selenic.com/) (hg):

`hg clone https://bitbucket.org/pflotran/pflotran-dev`.

This requires first creating a free account on bitbucket.

It should be possible to build and run PFLOTRAN on essentially any
system with modern C and Fortran (2003 standard or later) compilers and
an available implementation of the Message Passing Interface (MPI)
system that has been built with Fortran bindings. Besides these
requirements, the major third-party library required is the open-source
library PETSc—the Portable, Extensible Toolkit for Scientific
Computation—that provides the parallel framework on which PFLOTRAN is
built. Most of the work involved in building PFLOTRAN lies in building
PETSc. PETSc uses a sophisticated Python-based build tool, BuildSystem,
to perform extensive platform discovery and configuration as well as
automatic download and build for any of the open-source third-party
libraries that PETSc can use. The PFLOTRAN makefiles use the information
generated by BuildSystem as part of the PETSc build process; once PETSc
is built, building PFLOTRAN is straightforward.

Besides [PETSc](http://www.mcs.anl.gov/petsc/), third party libraries
commonly installed are

-   MPI, message passing interface ([OpenMPI](http://www.open-mpi.org/)
    or [MPICH](http://www.mpich.org/))

-   [HDF5](http://www.hdfgroup.org/HDF5/), required for parallel I/O and
    reading HDF5-formatted input files

-   [Metis](http://glaros.dtc.umn.edu/gkhome/views/metis) and
    [ParMetis](http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview),
    graph partitioning libraries required for unstructured grids

-   [Hypre](http://acts.nersc.gov/hypre/), which provides a variety of
    preconditioners and multilevel solves

PETSc can be used to download, compile and install all of these third
party libraries during compilation of PETSc as explained below, so that
the user does not have to do this themselves individually for each
library.

Compilers
---------

The installation of PFLOTRAN on MacOSX requires compiler versions 4.7 or
later for gfortran and gcc to be compatible with Fortran 2003.

For MacOSX compilers can be obtained from several sources including
[MacPorts](http://www.macports.org/) and the web site [High Performance
Computing for MacOSX](http://hpc.sourceforge.net/).

Building PETSc
--------------

The first step to building PFLOTRAN is to configure and build the PETSc
toolkit. This requires, at minimum, working installations of C and
Fortran 95-compliant compilers. For users looking for an open-source
compiler, we recommend the gcc and gfortran compilers that are part of
the GNU Compiler Collection (GCC), version 4.7.x or later. Users may
also wish to install MPI and other libraries from source or via a
package manager, but the PETSc `./configure` script can be
used not only to install PETSc but also MPI, HDF5, ParMETIS/METIS, and
various solver libraries such as MUMPS for sparse direct solvers, Hypre
for a variety of preconditioners and multi-level solvers, and the
Trilinos multilevel solver ML. For systems that do not provide specially
optimized versions of these libraries, we recommend using PETSc’s
`configure` to install these third-party libraries. If you do
wish to install any of these third-party libraries yourself, you will
need to do so *before* installing PETSc to that the
necessary PETSc interfaces to these packages can be built.

The development branch of PFLOTRAN tracks the main development branch of
PETSc and hence requires this `petsc-dev` version, which can be either
downloaded from the PETSc web page
[petsc-dev](http://www.mcs.anl.gov/petsc/developers/index.html), or
installed using Mercurial following instructions on the PETSc developer
web page. We recommend that PETSc be obtained using the version control
system ([Mercurial](http://mercurial.selenic.com/wiki)).

Define environment variables `PETSC_DIR` and `PETSC_ARCH`
giving the location of the petsc-dev source and the directory where the
libraries for the particular architecture are stored after compiling and
installing PETSc.

### Mac OS X

#### Lion, Mavericks ($leq$ Mac OS X 10.9.x)

To install ParMETIS/METIS on MacOSX it is necessary to first install the
latest version (3.0.x) of CMAKE from e.g. Homebrew, MacPorts or Fink.

To install PETSc, MPI using openmpi, HDF5, and ParMETIS/METIS with
debugging turned off configure PETSc using:

    ./config/configure.py --with-debugging=0 --with-shared-libraries=0
    --with-x=0 --download-openmpi --download-hdf5=1
    --download-metis=1 --download-parmetis=1

followed by

    make all

and

    make test

Note that ParMETIS/METIS is only needed for using the unstructured grid
capability in PFLOTRAN. HDF5 is recommended for large problems for use
with Visit for (parallel) visualization.

#### Yosemite (Mac OS X 10.10.x)

Special considerations are required to install PETSc and PFLOTRAN on
Yosemite (MacOSX 10.10.x). It is necessary to install gcc-4.9 from
Homebrew [other installations such as HPC MacOS X are not bug free at
the time of this writing (10.23.14)]. The following steps are required:

1.  install Homebrew (standard location /usr/local must be clean to
    avoid collisions)

2.  install gcc (need version 4.9.x): `brew install gcc`

3.  install cmake (need version 3.0.x): `brew install cmake`

4.  configure PETSc using the Mac c and c++ compilers clang, clang++ and
    gfortran installed from Homebrew:

         cd petsc-dev\

        ./configure --with-cc=clang --with-cxx=clang++
                    --with-fc=gfortran --download-mpich=1
                    --download-hdf5=1 --download-metis=1
                    --download-parmetis=1 --with-debugging=0

5. make PETSc

          make all
          make test

Notes: At the time of this writing (10.23.14) openmpi did not compile.
HDF5 did not compile using gcc-4.9 installed from Homebrew.

### Windows

To install PETSc and PFLOTRAN on Windows see instructions on the
PFLOTRAN wiki
(`https://bitbucket.org/pflotran/pflotran-dev/wiki/Home`).

### ORNL’s Jaguar XT4/5

    ./config/configure.py PETSC_ARCH=cray-xt4-pgi_fast \
    --configModules=PETSc.Configure \
    --optionsModule=PETSc.compilerOptions \
    --known-level1-dcache-size=65536 \
    --known-level1-dcache-linesize=64 \
    --known-level1-dcache-assoc=2 \
    --known-memcmp-ok=1 \
    --known-sizeof-char=1 \
    --known-sizeof-void-p=8 \
    --known-sizeof-short=2 \
    --known-sizeof-int=4
    --known-sizeof-long=8 \
    --known-sizeof-long-long=8 \
    --known-sizeof-float=4 \
    --known-sizeof-double=8 \
    --known-sizeof-size_t=8 \
    --known-bits-per-byte=8 \
    --known-sizeof-MPI_Comm=4 \
    --known-sizeof-MPI_Fint=4 \
    --known-mpi-long-double=0 \
    --with-batch=1 \
    --with-shared-libraries=0 \
    --with-dynamic=0 \
    --with-cc=cc \
    --with-cxx=CC \
    --with-fc=ftn \
    --COPTFLAGS="-tp barcelona-64 -fastsse -Mipa=fast" \
    --CXXOPTFLAGS="-tp barcelona-64 -fastsse -Mipa=fast" \
    --FOPTFLAGS="-tp barcelona-64 -fastsse" \
    --with-debugging=0 \
    --with-blas-lib=sci \
    --with-lapack-lib=sci \
    --with-x=0 \
    --with-mpi-dir=$MPICH_DIR \
    --download-hypre=1 \
    --download-parmetis=1 \
    --with-hdf5=1 \
    --with-hdf5-dir=$HDF5_DIR \
    --known-mpi-shared=0

The user will need to load the following HDF5 module beforehand:
 `module load hdf5-parallel`.

Building PFLOTRAN
-----------------

PFLOTRAN is compiled with the command:

`make [options] pflotran`

where several possible options are:

  Compile Option   
  ---------------- ----------------------------------------------
  `scorpio=1`:     –enable parallel IO
  `coll=1`:        –enable colloid-facilitated transport option

Thus for compiling with the supercritical $\mathrm{CO_2}$ option use: `make
ssco2=1 pflotran`. See the PFLOTRAN `makefile` for additional
options.

Compiling in parallel can be achieved using the modified
`make` command:

`make -j # pflotran`

where `#` denotes the number of cores. A comparison of the speedup
possible on Linux-Mint and MacOSX 10.11 is shown in [Figure 1](#fig:fcompile).
The Linux-Mint results are obtained on an 8-core machine; whereas the
Mac has 4 cores which could explain the slightly better performance at 8
cores on Linux.

![Parallel compile time as a function of number of cores for Intel and
Gnu compilers on Linux-Mint and the Gnu compiler on MacOSX 10.11. The
Mac results are based on gfortran 4.9.1.](./figs/linux-mac.png){#fig:fcompile}
*Figure 1. Parallel compile time as a function of number of cores for Intel and
Gnu compilers on Linux-Mint and the Gnu compiler on MacOSX 10.11. The
Mac results are based on gfortran 4.9.1.*

Updating PFLOTRAN
-----------------

To update the PFLOTRAN source code type:

    hg pull -u

from within the PFLOTRAN source repository. Recompile PFLOTRAN using:

    make clean

Parallel I/O using Sarat Sreepathi’s SCORPIO library with PFLOTRAN {#parallelio}
------------------------------------------------------------------

The SCORPIO (parallel I/O library) enables a scalable general purpose
parallel I/O capability for HPC by taking advantage of existing parallel
I/O libraries, such as HDF5 which are being widely used by scientific
applications, and modifying these algorithms to better scale to larger
number of processors. The library has been tested with MPICH-3.0.2 and
OpenMPI-1.6.

It is recommended that values for the variables
`HDF5_READ_GROUP_SIZE` and be set in the input file. If
they are unset, `HDF5_READ_GROUP_SIZE` is set to total MPI
ranks and `HDF5_WRITE_GROUP_SIZE` is set to 1 by default.

Typical values are to set the write group size equal to the number of
processes on a compute node (typically 16 or 32). A much higher read
group size is preferred, e.g. use 512 when running on 512 cores so that
one process reads the input file and broadcasts relevant sections. Put
`HDF5_WRITE_GROUP_SIZE` under the `OUTPUT`
keyword:

    :======================== output options ==================
    OUTPUT
      TIMES y 5. 10. 15. 20.
      FORMAT HDF5
      HDF5_WRITE_GROUP_SIZE 16
    END

and `HDF5_READ_GROUP_SIZE` in the main body of the input
file:

    HDF5_READ_GROUP_SIZE 1024

For more details on the SCORPIO library, please see Appendix A in Sarat
Sreepathi’s `<admin@sarats.com>`;
[dissertation](http://www.lib.ncsu.edu/resolver/1840.16/8317).

Instructions for downloading and installing the SCORPIO library for use
with PFLOTRAN is provided below. Note that this software is separate
from PFLOTRAN and under a LGPL.

1.  Download source code for building the SCORPIO library:

        svn co http://ascem-io.secure-water.org/ascem-io/scorpio DIRNAME

    where `DIRNAME` is the installation directory (Default:
    `scorpio`).

    The username and password are:

        username: pflotran_dev
        password: gr0undw@t3r

2.  Compile SCORPIO library: First, set the environment variable
    `SCORPIO_DIR` to a directory where you wish to install
    the library files. Please make sure that you have the correct
    permissions to write to that location. For example, you can use
    something like `${HOME}/parallelIO/scorpio`.
    Depending on the shell (bash or tcsh/csh) you are using:

        export SCORPIO_DIR=<your-iolib-install-dir>

    or

        setenv SCORPIO_DIR <your-iolib-install-dir>

    To compile the library, check to make sure that the Makefile has the
    right settings for your machine. Typically, the current
    configuration suffices. So you can just follow the instructions
    below. For advanced users, please edit the section for
    `pflotran_machine` in the Makefile as desired. This
    assumes that you have let PETSc build MPI and `mpicc` and
    `mpif90` are located in `${PETSC_DIR}/${PETSC_ARCH}/bin`.
    If not, you may need to alter the `Makefile` to provide
    the correct path to the location of the MPI compilers.

        cd DIRNAME/src
        make MACHINE=pflotran_machine
        make install

    This will build the library `libscorpio.a` and copy
    corresponding files to `SCORPIO_DIR/lib` and
    `SCORPIO_DIR/include` directories.

    On machines with MPI installed modify the makefile to use the native
    `mpi` compilers:

        ifeq ($(MACHINE),pflotran_machine)
          CC=mpicc
          FC=mpif90
          LINKER=${FC}
          CFLAGS+= -I${PETSC_DIR}/${PETSC_ARCH}/include -O3
          FFLAGS+= -I${PETSC_DIR}/${PETSC_ARCH}/include -O3
          LDFLAGS+= -Wl,-L${PETSC_DIR}/${PETSC_ARCH}/lib -lhdf5 -lz
        endif

3.  Compile PFLOTRAN:

    Please ensure that environmental variable: `SCORPIO_DIR`
    is pointed to `<your-iolib-install-dir>`:

        cd PFLOTRAN_DIR/src/pflotran
        make scorpio=1 pflotran

<!-- -->

    -------------------------------------------------------------------------------
    SCORPIO
    Scalable Parallel I/O module for Environmental Management Applications
    -------------------------------------------------------------------------------
    This library provides software that read/write data sets from/to parallel file
    systems in an efficient and scalable manner.
    In this context, scalable means that the simulators read/write
    performance does not degrade significantly as the number of cores grows.

    -------------------------------------------------------------------------------
    COPYRIGHT AND LICENSE
    -------------------------------------------------------------------------------
    SCORPIO is distrubuted under the terms of the GNU Lesser General Public
    License (LGPL). The copyright is held jointly by North Carolina State University
    and Pacific Northwest National Laboratory.

    The copyright and license information is specified in the included file
    COPYRIGHT.

    -------------------------------------------------------------------------------
    Repository Access
    -------------------------------------------------------------------------------
    Please request write access by contacting Kumar Mahinthakumar (gmkumar@ncsu.edu).
    Use the following command to access repository:
        svn co http://ascem-io.secure-water.org/ascem-io

    -------------------------------------------------------------------------------
    Building Library
    -------------------------------------------------------------------------------
    The current stable release is 2.2.

    --------------
    Prerequisites:
    --------------
    MPI
    C compiler
    HDF5 libraries (preferably with parallel(MPI) support)
    Optional: Fortran (for Fortran example)

    After downloading SCORPIO and gathering details of HDF5 installation,
    the following commands can be used to build and install SCORPIO:

        cd <SCORPIO check out directory>/src
        make CC=<C-compiler> HDF5_INCLUDE_DIR=<location of the HDF5 include directory>
        make SCORPIO_INSTALL_DIR=<user defined install location> install

    In this case, CC refers to C compiler with MPI support, e.g., mpicc.

    -------------------------------------------------------------------------------

Running PFLOTRAN
----------------

PFLOTRAN can be run from the command line as

    mpirun -np 10 pflotran [options]

A number of command line options are available:

|       Option                          |          Description                                  |
| ------------------------------------- | ----------------------------------------------------- |
|`-pflotranin <string>`                 | specify input file [default: `pflotran.in`]|
|`-input_prefix <string>`               | specify input file prefix and path [default: `pflotran`]|
|`-output_prefix <string>`              | specify output file prefix and path to prefix directory which must already exist [default: `pflotran`]|
|`-screen_output off`                   | turn off screen output|
|`-realization_id <integer>`            | run specified realization ID|
|`-multisimulation`                     | run multiple input files in one run|
|`-stochastic`                          | Monte Carlo multiple realization run & print list of command line options |
|`-log_summary`                         | print out run performance|
|`-snes_converged_reason`               | print the reason for convergence/divergence after each solve|
|`-on_error_abort`                      | aborts run on hitting NaNs|
|`-v`                                   | <int> verbose|
|`-options_left`                        | for debugging: lists any PETSc objects that have not been freed |
|`-snes_monitor`                        | print to screen function norm with each iteration|
|`-snes_linesearch_monitor`             | print to screen line search information|
|`-snes_view`                           | print line search method|
|`-snes_ls`                             | options: [cubic, quadratic, basic, basicnonorms]|
|`-snes_type newtonls`                  ||
|`-snes_type newtontr`                  ||
|`-snes_tr_delta0`                      | <delta0>|
|`-snes_monitor_lg_residualnorm`        | draws line graph of the residual norm convergence using X11|
|`-ksp_monitor_lg_residualnorm`         ||

    Examples:

    mpirun -np 1 ./pflotran -output_prefix ./path_to/prefix

Multiple Realization Simulation Mode
------------------------------------

To launch 1000 realizations on 100 processor groups using 10,000
processor cores:

    mpirun -np 10000 pflotran -stochastic -num_realizations 1000 -num_groups 100

Each processor group will utilize 100 processor cores and run 10
realizations apiece (`num_realizations/num_groups`), one
after another. Thus, 100 realizations are executed simultaneously with
each processor group simulating a single realization on 100 processor
cores at a time. Each processor group continues to run realizations
until its allocation of 10 is completed.

To simulate a specific realization without running in multi-realization
stochastic mode use:

    mpirun -np 10000 pflotran -realization_id <integer>

where `<integer>` specifies the realization id.

Multiple Simulation Mode
------------------------

To run multiple input decks simultaneously, create a file e.g.
`filenames.in`, containing the list of filenames that are to
be run:

                                 sim1.in
                                 sim2.in
                                 ...
                                 sim100.in

where `simn.in` is the usual PFLOTRAN input file. The names
may be arbitrarily chosen. Then, launch the run as:

    mpirun -n XXX pflotran -pflotranin filenames.in -multisimulation

Note that all simulations run at once. The same logic used to allow a
processor group to run multiple simulations with
`multirealization`is *not* implemented.
**Choose the number of cores to be a multiple of the number of
input filenames listed in the input file (i.e. filenames.in).**
All output files have `Gn` appended to the file name, e.g.
`sim10G10-001.tec`.

PFLOTRAN Regression Test Manager
================================

The test manager for PFLOTRAN is a python program that is responsible
for reading a configuration file, identifying the tests declared in the
file, running PFLOTRAN on the appropriate input files, and then
comparing the results to a known *gold standard* output file.

Running the Test Manager
------------------------

The test manager can be run in two ways, either as part of the build
system using `make` or manually.

There are two options for calling the test manager through make: `make
check` and `make test`. The `check` target runs a small set of tests
that verify that PFLOTRAN is built and running on a given system. This
would be run by user to verify that their installation of PFLOTRAN is
working. The `test` target runs a fuller set of regression tests
intended to identify when changes to the code cause significant changes
to PFLOTRAN’s results.

Calling the test manager through make relies on make variables from
PETSc to determine the correct version of python to use, if PFLOTRAN was
build with MPI, and optional configurations such as unstructured meshes.
The version of python used to call the test manager can be changed from
the command line by specifying python:

    $ cd ${PFLOTRAN_DEV}/src/pflotran
    $ make PYTHON=/opt/local/bin/python3.3 check

To call the test manager manually:

    $ cd ${PFLOTRAN_DEV}/regression_tests
    $ python regression-tests.py \
        --executable ../src/pflotran/pflotran \
        --config-file shortcourse/copper_leaching/cu_leaching.cfg \
        --tests cu_leaching

Some important command line arguments when running manually are:

-   executable: the path to the PFLOTRAN executable

-   mpiexec: the name of the executable for launching parallel jobs,
    (mpiexec, mpirun, aprun, etc).

-   config-file: the path to the configuration file containing the
    tests you want to run

-   recursive-search: the path to a directory. The test manager
    searches the directory and all its sub-directories for
    configuration files.

-   tests: a list of test names that should be run

-   suites: a list of test suites that should be run

-   update: indicate that the the gold standard test file for a given
    test should be updated to the current output.

-   new-tests: indicate that the test is new and current output should
    be used for gold standard test file.

-   check-performance: include the performance metrics
    (`SOLUTION` blocks) in regression checks.

The full list of command line options and a brief description can be
found by running with the `--help` flag:

    $ python regression-tests.py --help

Test output
-----------

The test manager produces terse screen output, only printing critical
warnings, a progress bar as each test is run, and a summary of the
overall test results. Example results from running `make test` are:

      Test log file : pflotran-tests-2013-05-06_13-07-05.testlog

    ** WARNING ** : mpiexec was not provided on the command line.
                    All parallel tests will be skipped!

    Running pflotran regression tests :
    ..........S........SSSS.......F..........SSS
    ---------------------------------------------------------------------
    Regression test summary:
        Total run time: 179.259 [s]
        Total tests : 44
        Skipped : 8
        Tests run : 36
        Failed : 1

The progress bar records a period, `.`, for each successful test, an `S`
if a test was skipped, an `F` for failed tests, a `W` for a test that
generated a warning, and an `E` for a test that generated an internal
error.

Each time the test suite is run, a log file is generated in the
regression test directory. The log file contains a detailed record of
every test run, including: the directory containing the test, the
command line call to PFLOTRAN used to run the test, a diff command to
compare the regression files, and a list of failures. The log file can
quickly be searched for `skip`, `fail`, `error` to identify the tests
that generated the message.

The test directories contain any files generated by PFLOTRAN during the
run. Screen output for each test is contained in the file
`\${TEST\_NAME}.stdout`.

Configuration Files
-------------------

The regression test manager reads tests specified in a series of
configuration files in standard `cfg` (or windows `ini` file) format.
They consist of a series of sections with key-value pairs:

    [section-name]
    key = value

Section names should be all lower case, and spaces must be replaced by a
hyphen or underscore. Comments are specified by a `\#` character.

A test is declared as a section in the configuration file. It is assumed
that there will be a PFLOTRAN input file with the same name as the test
section. The key-value pairs in a test section define how the test is
run and the output is compared to the gold standard file.

    [calcite-kinetics]
    #look for an input file named `calcite-kinetics.in'
    np = 2
    timeout = 30.0
    concentration = 1.0e-10 absolute

-   np = N, (optional), indicates a parallel test run with N processors.
    Default is serial. If mpiexec in not provided on the command line,
    then parallel tests are skipped.

-   timeout = N, (optional), indicates that the test should be allowed
    to run for N seconds before it is killed. Default is 60.0 seconds.

-   TYPE = TOLERANCE COMPARISON, indicates that data in the regression
    file of type TYPE should be compared using a tolerance of TOLERANCE.
    Know data types are listed below.

The data types and default tolerances are:

-   time = 5 percent

-   concentration = $1\times 10^{-12}$ absolute

-   generic = $1\times 10^{-12}$ absolute

-   discrete = 0 absolute

-   rate = $1\times 10^{-12}$ absolute

-   volume\_fraction = $1\times 10^{-12}$ absolute

-   pressure = $1\times 10^{-12}$ absolute

-   saturation = $1\times 10^{-12}$ absolute

-   residual = $1\times 10^{-12}$ absolute

The default tolerances are deliberately set very tight, and are expected
to be overridden on a per-test or per configuration file basis. There
are three known comparisons: “absolute”, for absolute differences
($\delta=|c-g|$), “relative” for relative differences
($\delta={|c-g|}/{g}$), and “percent” for specifying a percent
difference ($\delta=100\cdot{|c-g|}/{g}$).

In addition there are two optional sections in configuration files. The
section “default-test-criteria” specifies the default criteria to be
used for all tests in the current file. Criteria specified in a test
section override these value. A section name “suites“ defines aliases
for a group of tests.

    [suites]
    serial = test-1 test-2 test-3
    parallel = test-4 test-5 test-6

Common test suites are `standard` and `standard_parallel`, used by `make
test`, and domain specific test suites, `geochemistry`, `flow`,
`transport`, `mesh`, et cetra.

Creating New Tests
------------------

We want running tests to become a habit for developers so that `make
pflotran` is always followed by `make test.` With that in mind, ideal
test cases are small and fast, and operate on a small subsection of the
code so it is easier to diagnose where a problem has occurred. While it
may (will) be necessary to create some platform specific tests, we want
as many tests as possible to be platform independent and widely used.
There is a real danger in having test output become stale if it requires
special access to a particular piece of hardware, operating system or
compiler to run.

The steps for creating new regression tests are:

-   Create the PFLOTRAN input file, and get the simulation
    running correctly.

-   Tell PFLOTRAN to generate a regression file by adding a regression
    block to the input file, e.g.:

        REGRESSION
          CELLS
            1
            3978
          /
          CELLS_PER_PROCESS 4
        END

-   Add the test to the configuration file

-   Refine the tolerances so that they will be tight enough to identify
    problems, but loose enough that they do not create a lot of false
    positives and discourage users and developers from running
    the tests.

-   Add the test to the appropriate test suite.

-   Add the configuration file, input file and “gold” file to
    revision control.

**Guidelines for setting tolerances go here, once we figure out what to
recommend.**

Updating Test Results
---------------------

The output from PFLOTRAN should be fairly stable, and we consider the
current output to be “correct”. Changes to regression output should be
rare, and primarily done for bug fixes. Updating the test results is
simply a matter of replacing the gold standard file with a new file.
This can be done with a simple rename in the file system:

    mv test_1.regression test_1.regression.gold

Or using the regression test manager:

    $ python regression-tests.py --executable ../src/pflotran/pflotran \
        --config-file my_test.cfg --tests test_1 --update

Updating through the regression test manager ensures that the output is
from your current executable rather than a stale file.

*Please document why you updated gold standard files in your revision
control commit message.*

Visualization
=============

Visualization of the results produced by PFLOTRAN can be achieved using
several different utilities including commercial and open source
software. Plotting 2D or 3D output files can be done using the
commercial package **Tecplot**, or the opensource packages
[VisIt](https://wci.llnl.gov/codes/visit/) and
[ParaView](http://www.paraview.org/). **Paraview** is
similar to **VisIt**, and both are capable of remote
visualization on parallel architectures.

Several potentially useful hints on using these packages are provided
below.

Tecplot
-------

-   In order to change the default file format in
   **TecPlot** so that it recognizes `.tec`
    files, place the following in the `tecplot.cfg` file:

`$!FileConfig FNameFilter {InputDataFile = "*.tec"}`

VisIt
-----

-   Inactive cells can be omitted by going to <span>Controls -&gt;
    Subset</span> and unchecking Material\_ID[0].

-   In 3D to scale the size of one of the coordinate axes go to
    <span>Controls -&gt; View</span> and check box <span>Scale 3D
    Axes</span> and set desired scaling factor in box to right.

-   To make 2D plots use <span>Operators -&gt; Slicing -&gt;
    Slice</span>.

-   For 3D plots <span>Operators -&gt; Slicing -&gt; ThreeSlice</span>
    is useful.

-   Instructions to set up geomechanical and flow data simultaneously in
    <span>**VisIt**</span>:

    1.  Open VisIt.

    2.  Select the $1\times 2$ layout button on Window 1 to open a new
        window (Window 2).

    3.  Now make Window 1 active by checking the first button on
        Window 1.

    4.  Click on Open and select \*-geomech-\*.xmf database.

    5.  Click on Add –&gt; Pseudocolor –&gt; rel\_disp\_z and click Draw
        (Set Auto apply to avoid clicking Draw repeatedly).

    6.  Select the domain and rotate it to a preferred angle.

    7.  Select Window 2 and make it active.

    8.  Click Open and select pflotran.h5 or pflotran-\*.h5 (This file
        contains all the subsurface flow data).

    9.  Click on e.g. Add –&gt; Pseudocolor –&gt; Gas Saturation (or
        other desired variable).

    10. Click on Lock view and Lock time on both windows (This will sync
        views and times on both windows).

    11. After a window pops up, select Yes.

    12. With Window 2 active select: Operators –&gt; Slicing
        –&gt; ThreeSlice.

    13. Double click on three slice and change x and y to
        appropriate values.

    14. Select Window 1.

    15. Select Controls –&gt; Expressions.

    16. Click New and add a name (e.g., disp\_vector). Select Vector
        Mesh Variable.

    17. Under Definition, add <span>&lt;rel\_disp\_x
        \_lb\_m\_rb\_&gt;,&lt;rel\_disp\_y
        \_lb\_m\_rb\_&gt;,&lt;rel\_disp\_z \_lb\_m\_rb\_&gt;</span>,
        Apply and Dismiss.

    18. Click Add –&gt; Vector –&gt; disp\_vector.

    19. Double click Vector under
        pflotran-geomech-\*.xmf database:disp\_vector.

    20. Select Form and set Scale to e.g. 0.5. Then select Rendering and
        change magnitude color from Default to difference (select a
        color scheme of your choice).

    21. Apply and Dismiss.

    22. Double click Pseudocolor –&gt; rel\_disp\_z. Select e.g.
        hot\_desaturated for color table. Set opacity to 50%. Apply
        and Dismiss.

    23. Next, for mesh movement click: Operators –&gt; Transforms
        –&gt; Displace. After a window pops up, dismiss it.

    24. Double click Displace, change Displacement multiplier to e.g.,
        $10000$. Click on Displacement Variable –&gt; Vectors
        –&gt; disp\_vectors. Dismiss the window saying no data etc.
        Apply and Dismiss.

    25. Finally, click the play button to watch movie. Rotate the domain
        to a convenient angle before doing so.

Gnuplot, MatPlotLib
-------------------

-   For 1D problems or for plotting PFLOTRAN observation, integral flux
    and mass balance output, the opensource software packages [gnuplot](http://www.gnuplot.info/) and
    [matplotlib](http://matplotlib.org/) are recommended.

-   With **gnuplot** and **matplotlib** it is
    possible to plot data from several files in the same plot. To do
    this with **gnuplot** it is necessary that the files
    have the same number of rows, e.g. time history points. The files
    can be merged during input by using the `paste` command
    as a pipe: e.g.

        plot '< paste file1.dat file2.dat' using 1:($n1*$n2)

    plots the product of variable in file 1 in column `n1`
    times the variable in file 2 in column `n2` of the
    merged file.

-   When using **gnuplot** it is possible to number the
    output file columns with `PRINT_COLUMN_IDS` added to
    the `OUTPUT` keyword. This is only useful, however, with
    `FORMAT TECPLOT POINT` output option.

Benchmark Problems
==================

In this section several benchmark problems are introduced illustrating
the capabilities of PFLOTRAN.

Ion Exchange
------------


Voegelin et al. (2000) present results of ion exchange in a soil column
for the system Ca-Mg-Na. Here PFLOTRAN is applied to this problem using
the Gaines-Thomas exchange model. Soil column C1 with length 48.1 cm and
diameter 0.3 cm was used for the simulations. A flow rate of 5.6 cm/min
was used in the experiment. The inlet solution was changed during the
coarse of the experiment at 20 and 65 pore volumes with cation
compositions listed in Table 2 of Voegelin et al. (2000). The CEC of the
soil used in the experiments was determined to have a value of
0.06 $\pm$ 0.002 mol/kg. As PFLOTRAN requires the CEC in units of
mol/m$^3$ this was obtained from the formula

\begin{align}
\omega = \frac{N_s}{V} =
\frac{N_s}{M_s}\frac{M_s}{V_s}\frac{V_s}{V} = \rho_s (1-\varphi) {\rm CEC}.
\end{align}

Using a porosity of 0.61, the solid grain density $\rho_s$ is given by

\begin{align}
\rho_s = \frac{\varphi \rho_l}{1-\varphi} = 3.0344 \text{g/cm$^3$},
\end{align}

![Breakthrough curves for Ca2+, Mg2+ and Na+ compared with experimental results
from Voegelin et al. (2000).](./figs/ionex.png){#fig:fionex}
*Figure 2. Breakthrough curves for Ca2+, Mg2+ and Na+ compared with experimental results
from Voegelin et al. (2000).*

for the mass density per pore volume $\rho_l$ = 1.94 g/cm$^3$ with
values from Voegelin et al. (2000). This gives for the site density per
bulk volume $\omega = 71.004$ mol/m$^3$. The results of the simulation
are shown in [Figure 2](#fig:fionex) along with data reported by Voegelin et
al. (2000). Self-sharpening fronts can be observed at approximately 10
and 71 at pore volumes, and a self-broadening front from 30-55 pore
volumes where agreement with experiment is not as good.

The input file for the simulation is listed in Table [tionex].

### PFLOTRAN Input File

    #Description: 1D ion exchange problem

    SIMULATION
      SIMULATION_TYPE SUBSURFACE
      PROCESS_MODELS
        SUBSURFACE_TRANSPORT transport
          GLOBAL_IMPLICIT
        /
      /
    END

    SUBSURFACE

    # m/s
    UNIFORM_VELOCITY 5.69333e-4 0.d0 0.d0

    #=========================== runtime ==========================================
    #CHECKPOINT 100
    #WALLCLOCK_STOP 11.75

    # == chemistry ================================================================
    CHEMISTRY
    #OPERATOR_SPLIT
    PRIMARY_SPECIES
    Na+
    #K+
    Ca++
    Mg++
    H+
    HCO3-
    Cl-
    Tracer
    /
    SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
    CaOH+
    CaCO3(aq)
    CaHCO3+
    CaCl+
    MgCO3(aq)
    MgHCO3+
    MgCl+
    HCl(aq)
    #KCl(aq)
    NaCl(aq)
    NaOH(aq)
    /
    GAS_SPECIES
    CO2(g)
    /
    MINERALS
    Halite
    /

    MINERAL_KINETICS
    Halite
    RATE_CONSTANT 1.e-30
    /
    /
    SORPTION
      ION_EXCHANGE_RXN
    # MINERAL Halite
      CEC 71.004  ! mol/m^3
      CATIONS
        Ca++   1.0 REFERENCE
        Na+    0.125893
        Mg++   0.691831
      /
    /
    /
    DATABASE /Users/lichtner/projects/parallel/repository/pflotran/database/hanford.dat
    LOG_FORMULATION
    ACTIVITY_COEFFICIENTS ! NEWTON_ITERATION
    MOLAL
    OUTPUT
    All
    TOTAL
    FREE_ION
    /
    /

    # == reference variables ======================================================
    REFERENCE_POROSITY 0.61d0

    # == time stepping ============================================================
    TIMESTEPPER
    TS_ACCELERATION 8
    MAX_STEPS 100000
    #MAX_STEPS 1
    /

    # == discretization ===========================================================
    GRID
    TYPE structured
    NXYZ 250 1 1
    BOUNDS
    0.d0 0.d0 0.d0
    0.481d0 1.d0 1.d0
    /
    /

    # == flow solvers =============================================================
    NEWTON_SOLVER FLOW
    PRECONDITIONER_MATRIX_TYPE AIJ
    RTOL 1.d-8
    ATOL 1.d-8
    STOL 1.d-30
    ITOL_UPDATE 1.d0
    #NO_INFINITY_NORM
    #NO_PRINT_CONVERGENCE
    #PRINT_DETAILED_CONVERGENCE
    /

    LINEAR_SOLVER FLOW
    #KSP_TYPE PREONLY
    #PC_TYPE LU
    #KSP_TYPE FGMRES !samrai
    #PC_TYPE SHELL !samrai
    /

    # == transport solvers ========================================================
    NEWTON_SOLVER TRANSPORT
    PRECONDITIONER_MATRIX_TYPE AIJ
    RTOL 1.d-8
    ATOL 1.d-8
    STOL 1.d-30
    #NO_INFINITY_NORM
    #NO_PRINT_CONVERGENCE
    #PRINT_DETAILED_CONVERGENCE
    /

    LINEAR_SOLVER TRANSPORT
    #PC_TYPE LU
    #KSP_TYPE PREONLY
    #KSP_TYPE FGMRES ! samrai
    #PC_TYPE SHELL !samrai
    /

    # == fluid properties =========================================================
    FLUID_PROPERTY
    DIFFUSION_COEFFICIENT 1.d-9
    #DIFFUSION_COEFFICIENT 9.33333e-7
    /

    # == material properties ======================================================
    MATERIAL_PROPERTY HD
    ID 1
    SATURATION_FUNCTION default
    POROSITY 0.61
    TORTUOSITY 1.0
    #LONGITUDINAL_DISPERSIVITY 0.001
    PERMEABILITY
    PERM_ISO 5.43d-13
    /
    /

    # == saturation / permeability functions ======================================
    SATURATION_FUNCTION HD
    SATURATION_FUNCTION_TYPE VAN_GENUCHTEN
    RESIDUAL_SATURATION 0.115
    LAMBDA 0.286
    ALPHA 1.9401d-4
    /

    #=========================== saturation functions =============================
    SATURATION_FUNCTION default
    /

    # == output ===================================================================
    OUTPUT
    TIMES s 10307.1 33498.2 41228.6
    PERIODIC_OBSERVATION TIMESTEP 1
    #PERIODIC TIMESTEP 1
    #PERIODIC TIME 0.04 y
    SCREEN PERIODIC 10
    #FORMAT HDF5
    FORMAT TECPLOT POINT
    VELOCITIES
    /

    # == times ====================================================================
    TIME
    FINAL_TIME 41228.6 s
    INITIAL_TIMESTEP_SIZE 1. s
    MAXIMUM_TIMESTEP_SIZE 20. s
    MAXIMUM_TIMESTEP_SIZE 1. s at 10200. s
    MAXIMUM_TIMESTEP_SIZE 20. s at 10350 s
    MAXIMUM_TIMESTEP_SIZE 1. s at 33300 s
    MAXIMUM_TIMESTEP_SIZE 20. s at 33600 s
    /

    # == regions ==================================================================
    REGION all
    COORDINATES
    0.d0 0.d0 0.d0
    0.481d0 1.d0 1.d0
    /
    /

    REGION west
    FACE WEST
    COORDINATES
    0. 0. 0.
    0. 1. 1.
    /
    /

    REGION east
    FACE EAST
    COORDINATES
    0.481 0. 0.
    0.481 1. 1.
    /
    /

    OBSERVATION
    REGION east
    /

    # == flow conditions ==========================================================
    skip
    FLOW_CONDITION west
    TYPE
    FLUX neumann
    /
    FLUX 0.317098d-7 ! 1 m/y
    #FLUX 1.5855d-9 ! 5 cm/y
    #FLUX file 200w_recharge_1951-2000_daily.dat
    /
    noskip

    FLOW_CONDITION Initial
    TYPE
    PRESSURE hydrostatic
    /
    DATUM 0.d0 0.d0 0.d0
    PRESSURE 101325.d0
    /

    FLOW_CONDITION west
    TYPE
    PRESSURE hydrostatic
    /
    DATUM 0.d0 0.d0 0.d0
    PRESSURE 101425.d0
    END

    FLOW_CONDITION east
    TYPE
    PRESSURE hydrostatic
    /
    DATUM 0.d0 0.d0 0.d0
    PRESSURE 101325.d0
    END

    # == transport conditions =====================================================
    TRANSPORT_CONDITION Initial
    TYPE dirichlet
    CONSTRAINT_LIST
    0.d0 Initial
    /
    /

    TRANSPORT_CONDITION east
    TYPE dirichlet
    CONSTRAINT_LIST
    0.d0 Initial
    /
    /

    TRANSPORT_CONDITION west
    TYPE dirichlet
    CONSTRAINT_LIST
    0.d0    Inlet1
    10307.1 Inlet2
    33498.2 Inlet3
    /
    /

    # == couplers =================================================================
    INITIAL_CONDITION Initial
    #FLOW_CONDITION Initial
    TRANSPORT_CONDITION Initial
    REGION all
    /

    BOUNDARY_CONDITION
    #FLOW_CONDITION west
    TRANSPORT_CONDITION west
    REGION west
    END

    BOUNDARY_CONDITION
    #FLOW_CONDITION east
    TRANSPORT_CONDITION east
    REGION east
    END

    # == stratigraphy =============================================================
    STRATA
    MATERIAL HD
    REGION all
    /

    # == transport constraints ====================================================
    CONSTRAINT Initial
    CONCENTRATIONS
    Na+           4.65d-3  T
    #K+            2.d-4    T
    Ca++          5.2d-3   T
    Mg++          4.55e-3  T
    H+            4.6     pH
    HCO3-        -3.5      G   CO2(g)
    Cl-           1.d-3    Z
    Tracer        4.65d-3  T
    /
    MINERALS
    Halite        0.5 1.
    /
    /

    CONSTRAINT Inlet1
    CONCENTRATIONS
    Na+           1.d-16  T
    #K+            1.d-10  T
    Ca++          5.3d-3  T
    Mg++          1.e-16  T
    H+            4.6    pH
    HCO3-        -3.5     G   CO2(g)
    Cl-           3.d-4   Z
    Tracer        9.4d-3  T
    /
    /

    CONSTRAINT Inlet2
    CONCENTRATIONS
    Na+           4.6d-3  T
    #K+            1.d-10  T
    Ca++          1.d-16  T
    Mg++          2.4e-3  T
    H+            4.6    pH
    HCO3-        -3.5     G   CO2(g)
    Cl-           3.d-4   Z
    Tracer        9.4d-3  T
    /
    /

    CONSTRAINT Inlet3
    CONCENTRATIONS
    Na+           4.65d-3 T
    #K+            1.d-10  T
    Ca++          5.2d-3  T
    Mg++          4.55e-3 T
    H+            4.6    pH
    HCO3-        -3.5     G   CO2(g)
    Cl-           3.d-4   Z
    Tracer        9.4d-3  T
    /
    /

    END_SUBSURFACE

GENERAL\_REACTION Example
-------------------------

### Problem Description

A single irreversible reaction is considered of the form

\begin{align}
A + 2 B \rightarrow C,
\end{align}

for flow in a fully saturated 1D column of
length 100 m with a Darcy velocity of 1 m/y, diffusion coefficient of
$10^{-9}$ m$^2$/s and porosity equal to 0.25. The conservation equation
for advection, diffusion and reaction is given by

\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi C_l + {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_l {\ =\ }- \varphi \nu_l {{{\mathcal R}}}, \ \ \ \ (l=A,\,B,\,C),
\end{align}

with stoichiometric coefficients $\nu_A = 1$, $\nu_B = 2$, and
$\nu_C=-1$. The flux ${\boldsymbol{F}}_l$ consists of contributions from
advection and diffusion

\begin{align}
{\boldsymbol{F}}_l {\ =\ }{\boldsymbol{q}}C_l - \varphi D {\boldsymbol{\nabla}}C_l.
\end{align}

The forward reaction rate is based on a elementary aqueous reaction

\begin{align}
{{{\mathcal R}}}{\ =\ }k_f C_A^{\nu_A} C_B^{\nu_B}.
\end{align}

Dividing through by porosity (assuming $\varphi$ = constant), the transport equation
becomes

\begin{align}
\frac{{{\partial}}C_l}{{{\partial}}t} + {\boldsymbol{\nabla}}\cdot{\boldsymbol{v}}C_l - D {\boldsymbol{\nabla}}\cdot{\boldsymbol{\nabla}}C_l + \nu_l^{} k_{f}^{} C_A^{\nu_A} C_B^{\nu_B} {\ =\ }0,
\end{align}

with average pore velocity

\begin{align}
{\boldsymbol{v}}{\ =\ }\frac{{\boldsymbol{q}}}{\varphi}.
\end{align}
Initial and boundary conditions imposed on the solution are given by


\begin{align}
C(x,t=0) {\ =\ }C_\infty,
C(x=0,\,t) {\ =\ }C_0,
\left.\frac{{{\partial}}C}{{{\partial}}x} \right|_{x=l_x} {\ =\ }0.
\end{align}

### Simulation Results

Results are shown in [Figure 3](#fig:fabc) for the concentrations of species
A, B, C at 5 years obtained from PFLOTRAN and a prototype code written
in C++ based on the PETSc TS time stepping class. The code uses a
backward Euler (TSBEULER) time integrator with nodes placed at the grid
cell corners. The slight discrepancy between the results of the two
codes may be due to the use of a finite volume cell-centered grid in
PFLOTRAN, versus the corner-node grid used in the prototype code.

![Comparison of concentrations for species A, B, C plotted as a function
of distance for an elapsed time of 5 years for PFLOTRAN and a prototype
code based on PETSc’s TS class.](./figs/abc.png){#fig:fabc}
*Figure 3. Comparison of concentrations for species A, B, C plotted as a function
of distance for an elapsed time of 5 years for PFLOTRAN and a prototype
code based on PETSc’s TS class.*

### PFLOTRAN Input File

    #Description: 1D general reaction with the aqueous reaction A + 2 B -> C.

    SIMULATION
      SIMULATION_TYPE SUBSURFACE
      PROCESS_MODELS
        SUBSURFACE_TRANSPORT transport
          GLOBAL_IMPLICIT
        /
      /
    END

    SUBSURFACE

    #=========================== useful tranport parameters ==================
    UNIFORM_VELOCITY 1.d0 0.d0 0.d0 m/yr

    REFERENCE_DENSITY 1000.d0

    #=========================== chemistry ========================================
    CHEMISTRY
      PRIMARY_SPECIES
        A(aq)
        B(aq)
        C(aq)
      /
      GENERAL_REACTION
        REACTION A(aq) + 2 * B(aq) <-> C(aq)
        FORWARD_RATE 5.d-8
        BACKWARD_RATE 0.d0
      /
      DATABASE /Users/lichtner/pflotran/pflotran-dev/database/hanford.dat
      OUTPUT
        all
        TOTAL
      /
    END

    #=========================== solver options ===================================
    LINEAR_SOLVER TRANSPORT
      SOLVER DIRECT
    END

    #=========================== discretization ===================================
    GRID
      TYPE structured
      NXYZ 100 1 1
      BOUNDS
        0.d0 0.d0 0.d0
        100.d0 100.d0 1.d0
      /
    END

    #=========================== fluid properties =================================
    FLUID_PROPERTY
      DIFFUSION_COEFFICIENT 1.d-9
    END

    #=========================== material properties ==============================
    MATERIAL_PROPERTY soil1
      ID 1
      POROSITY 0.25d0
      TORTUOSITY 1.d0
      ROCK_DENSITY 1650.d0
    END

    #=========================== output options ===================================
    OUTPUT
      TIMES y 5.
      FORMAT TECPLOT POINT
    END

    #=========================== times ============================================
    TIME
      FINAL_TIME 5.d0 y
      INITIAL_TIMESTEP_SIZE 1.d0 h
      MAXIMUM_TIMESTEP_SIZE 1.d-2 y
    END

    #=========================== regions ==========================================
    REGION all
      COORDINATES
        0.d0 0.d0 0.d0
        100.d0 1.d0 1.d0
      /
    END

    REGION west
      FACE west
      COORDINATES
        0.d0 0.d0 0.d0
        0.d0 1.d0 1.d0
      /
    END

    REGION east
      FACE east
      COORDINATES
        100.d0 0.d0 0.d0
        100.d0 1.d0 1.d0
      /
    END

    #=========================== transport conditions =============================
    TRANSPORT_CONDITION initial
      TYPE dirichlet
      CONSTRAINT_LIST
        0.d0 initial
      /
    END

    TRANSPORT_CONDITION inlet
      TYPE dirichlet
      CONSTRAINT_LIST
        0.d0 inlet
      /
    END

    TRANSPORT_CONDITION outlet
      TYPE zero_gradient
      CONSTRAINT_LIST
        0.d0 inlet
      /
    END

    #=========================== constraints ======================================
    CONSTRAINT initial
      CONCENTRATIONS
        A(aq) 1.d-16 T
        B(aq) 1.d-16 T
        C(aq) 1.d-16 T
      /
    END

    CONSTRAINT inlet
      CONCENTRATIONS
        A(aq) 1.d0   T
        B(aq) 1.d0   T
        C(aq) 1.d-16 T
      /
    END

    #=========================== condition couplers ===============================
    # initial condition
    INITIAL_CONDITION
      TRANSPORT_CONDITION initial
      REGION all
    END

    BOUNDARY_CONDITION outlet
      TRANSPORT_CONDITION outlet
      REGION east
    END

    BOUNDARY_CONDITION inlet
      TRANSPORT_CONDITION inlet
      REGION west
    END

    #=========================== stratigraphy couplers ============================
    STRATA
      REGION all
      MATERIAL soil1
    END

    END_SUBSURFACE

RICHARDS Mode with Tracer: SX-115 Hanford Tank Farm
---------------------------------------------------

### Problem Description

The saturation profile is computed for both steady-state and transient
conditions in a 1D vertical column consisting of a layered porous medium
representing the Hanford sediment in the vicinity of the S/SX tank farm.
The transient case simulates a leak from the base of the SX-115 tank.
This problem description is taken from Lichtner et al. (2004).

### Governing Equations

The moisture profile is calculated using parameters related to the
Hanford sediment at the S/SX tank farm based on the Richards equation
for variably saturated porous media. The Hanford sediment is composed of
five layers with the properties listed in Tables [t1] and [t2]. The
governing equations consist of Richards equation for variably saturated
fluid flow given by

\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi s\rho + {\boldsymbol{\nabla}}\cdot{\boldsymbol{q}}\rho {\ =\ }Q,
\end{align}

and solute transport of a tracer

\begin{align}
\frac{{{\partial}}}{{{\partial}}t}\varphi C + {\boldsymbol{\nabla}}\cdot\big({\boldsymbol{q}}C - \varphi s \tau D {\boldsymbol{\nabla}}C\big) {\ =\ }Q_C.
\end{align}

In these equations $\varphi$ denotes the spatially variable porosity of
the porous medium assumed to constant within each stratigraphic layer,
$s$ gives the saturation state of the porous medium, $\rho$ represents
the fluid density in general a function of pressure and temperature, $C$
denotes the solute concentration, $D$ denotes the diffusion/dispersion
coefficient, $\tau$ represents tortuosity, $Q$ and $Q_C$ denote
source/sink terms, and ${\boldsymbol{q}}$ denotes the Darcy velocity
defined by

\begin{align}
{\boldsymbol{q}}{\ =\ }- \frac{k_{\rm sat}k_r}{\mu} {\boldsymbol{\nabla}}(p-\rho g z),
\end{align}

with saturated permeability $k_{\rm sat}$, relative permeability $k_r$,
fluid viscosity $\mu$, pressure $p$, formula weight of water $W$,
acceleration of gravity $g$, and height $z$. Van Genuchten capillary
properties are used for relative relative permeability according to the
relation
\begin{align}
\label{kr}
k_{r} {\ =\ }\sqrt{s_{\rm eff}} \left\{1 - \left[1- \left( s_l^{\rm
eff} \right)^{1/m} \right]^m \right\}^2,
\end{align}
 where $s_{\rm eff}$ is
related to capillary pressure $P_c$ by the equation
\begin{align}
\label{sat}
s_{\rm eff} {\ =\ }\left[1+\left( \alpha |P_c| \right)^n
\right]^{-m},
\end{align}
 where $s_{\rm
eff}$ is defined by
\begin{align}
\label{seff1}
s_{\rm eff} {\ =\ }\frac{s - s_r}{1 - s_r},
\end{align}
and where $s_r$ denotes
the residual saturation. The quantity $n$ is related to $m$ by the
expression
\begin{align}
\label{lambda}
m {\ =\ }1-\frac{1}{n}, \ \ \ \ \ n {\ =\ }\frac{1}{1-m}.
\end{align}
The capillary pressure $P_c$ and fluid pressure $p$ are related by the
(constant) gas pressure $p_g^0$
\begin{align}
P_c {\ =\ }p_g^0-p,
\end{align}
 where
$p_g^0 = 101,325$ Pa is set to atmospheric pressure.

#### Semi-Analytical Solution for Steady-State Conditions

For steady-state conditions the saturation profile satisfies the
equation
\begin{align}
\frac{d}{dz} \rho q_z {\ =\ }0,
\end{align}
 or assuming an
incompressible fluid
\begin{align}
q_z {\ =\ }q_z^0,
\end{align}
where $q_z^0$ denotes
infiltration at the surface. Thus the pressure is obtained as a function
of $z$ by solving the ODE
\begin{align}
\label{dpdz}
\frac{dp}{dz} {\ =\ }-\frac{\mu q_z^0}{k_{\rm sat} k_r} - \rho g,
\end{align}

using Eqns.\ref{kr} and \ref{sat} to express the relative permeability $k_r$ as a function
of pressure. For the special case of zero infiltration it follows that

\begin{align}
p(z) {\ =\ }p_0 - \rho g (z-z_0),
\end{align}
 with $p(z_0) = p_0$. The
saturation profile is obtained from Eqns.\ref{sat} and \ref{seff}.

#### Watertable

The position of the watertable is defined by vanishing of the capillary
pressure
\begin{align}
P_c(z_{\rm wt}) {\ =\ }0,
\end{align}
 where $z_{\rm wt}$ denotes the
height of the watertable. For the case with no infiltration at the
surface it follows that

\begin{align}
z_{\rm wt} {\ =\ }z_0 + \frac{p_0-p_g}{\rho g},
\end{align}
 with the boundary
condition $p(z_0) = p_0$ and $z_0$ denotes the datum. If $p_0$ is set
equal to $p_g$, then $z_{\rm wt} = z_0$, or the height of the watertable
is equal to the datum. The same holds true also with constant nonzero
infiltration.

### Model Parameters

Model parameters used in the simulations are listed in Tables [t1] and
[t2]. Although not needed here, thermal properties are also listed.
Diffusivity was set to $10^{-9}$ m$^2$ s$^{-1}$ and tortuosity was set
to one.

| Formation               |  Abbrev.  |   Thickness [m]
| ----------------------- | --------- | -----------------
|  Backfill               |    BF     |             16.0
|  Hanford Fine Sand      |    HF     |             23.0
|  Plio-Pleistocene       |    PP     |              6.0
|  Upper Ringold Gravel   |    URG    |              3.0
|  Middle Ringold Gravel  |    MRG    |             20.0

  : Stratigraphic sequence used in the calculations, after Ward et al.
  (1996).<span data-label="t1"></span>



| Form. | $\rho_s$  |   $c$    |  $\kappa_{\rm dry}$ | $\kappa_{\rm wet}$ |  $\varphi$  |   $s_r$  |  $\alpha$  |  $m$  |  $k_{\rm sat}$  |
|------ |------ | ----- | ------- | ------ | ------ | ------ | ------------ | ------- | --------------- |
|            |g $cm^{-3}$| J kg$^{-1}$ K$^{-1}$||      | —       |     —    |  Pa$^{-1}$ |   —    |    m$^2$        |
|BF          |     2.8   |    800   |    0.5   |   2   | 0.2585  | 0.0774   |  1.008e-3  | 0.6585 |     1.240e-12   |
|HF          |     2.8   |    800   |    0.5   |   2   | 0.3586  | 0.0837   |  9.408e-5  | 0.4694 |     3.370e-13   |
|PP          |     2.8   |    800   |    0.5   |   2   | 0.4223  | 0.2595   |  6.851e-5  | 0.4559 |     3.735e-14   |
|URG         |     2.8   |    800   |    0.5   |   2   | 0.2625  | 0.2130   |  2.966e-5  | 0.3859 |     1.439e-13   |
|MRG         |     2.8   |    800   |    0.5   |   2   | 0.1643  | 0.0609   |  6.340e-5  | 0.3922 |     2.004e-13   |


: Parameters for material and thermal properties for intrinsic rock
  density $\rho_s$, heat capacity $c$, thermal conductivity $\kappa$,
  porosity $\varphi$, residual water saturation $s_r$, van Genuchten
  parameters $\alpha$ and $\lambda$, and vertical water saturated
  permeability $k_{\rm sat}$. Data taken from Khaleel and Freeman
  (1995), Khaleel et al. (2001), and Pruess et al. (2002).<span
  data-label="t2"></span>

### Simulation Results

The calculations are carried out for an isothermal system using Richards
equation. First, the steady-state saturation profile is obtained without
the tank leak present. Then using the steady-state profile as the
initial condition the tank leak is turned on. This can be easily
accomplished using CHECKPOINTING and RESTART keywords. The results for
the steady-state saturation and pressure profiles are shown in
[Figure 4](#fig:f1) for infiltration rates at the surface of 0, 8 and 80 mm/y.
The mean infiltration rate at the Hanford site is approximately 8 mm/y.
A 1D column 68 m heigh with the water table located at a height of 6 m
from the bottom is used in the simulation. A uniform grid spacing of 0.5
m is used to discretize Richards equation.

Shown in [Figure 5](#fig:f2) is the saturation at different times following a
two week leak releasing 60,000 gallons from the SX-115 tank at a depth
of 16 m. In the simulation a release rate of $1.87 \times  10^{-3}$
kg/s is used.

![Steady-state saturation and pressure profiles for infiltration rates of 0, 8 and 80 mm/y.
The water table is located at 6 m from the bottom of the computational domain.](./figs/ps.png){#fig:f1}
*Figure 4. Steady-state saturation and pressure profiles for infiltration rates of 0, 8 and 80 mm/y.
The water table is located at 6 m from the bottom of the computational domain.*

![Simulation of a tank leak with a duration of two weeks showing the saturation profile
for different times indicated in the figure for an infiltration rate of 8 mm/y.](./figs/sat_leak.png){#fig:f2}
*Figure 5. Simulation of a tank leak with a duration of two weeks showing the saturation profile
for different times indicated in the figure for an infiltration rate of 8 mm/y.*

![The solute concentration profile corresponding to the above figure for different times indicated
in the figure for an infiltration rate of 8 mm/y.](./figs/conc.png){#fig:f3}
*Figure 6. The solute concentration profile corresponding to the above figure for different times indicated
in the figure for an infiltration rate of 8 mm/y.*

### PFLOTRAN Input File

Listing for the PFLOTRAN input file coupling Richards mode to a tracer
is given below. Note that the stratigraphic zone specification in
`REGION` is grid independent as is the grid size
specification in keyword `GRID`. Therefore to change the grid
spacing only the line:`NXYZ 1 1 136`, needs to be changed.
Also note that lines beginning with `#` are read as a comment as is input
following `!`.

Note that the input file looks for the RESTART file for the transient
run in the subdirectory: `./ss/sx115-restart.chk`.

PFLOTRAN input file `sx115.in`:

    #Description: 1D test problem for tracer transport for Hanford SX-115 waste tank.

    SIMULATION
      SIMULATION_TYPE SUBSURFACE
      PROCESS_MODELS
        SUBSURFACE_FLOW flow
          MODE RICHARDS
        /
        SUBSURFACE_TRANSPORT transport
          GLOBAL_IMPLICIT
        /
      /
    END

    SUBSURFACE

    #=========================== chemistry ========================================
    CHEMISTRY
      PRIMARY_SPECIES
        Tracer
      /
      OUTPUT
        all
        FREE_ION
      /
    END

    #=========================== runtime ==========================================
    #CHECKPOINT 100000
    RESTART ./ss/sx115-restart.chk 0.d0
    #OVERWRITE_RESTART_TRANSPORT
    #WALLCLOCK_STOP 11.75

    #=========================== solver options ===================================
    TIMESTEPPER FLOW
      #MAX_STEPS -1
      TS_ACCELERATION 8
      INITIALIZE_TO_STEADY_STATE 1.d0
    END

    NEWTON_SOLVER FLOW
      #RTOL 1.d-12
      RTOL 1.d-20
      #ATOL 1.d-12
      #STOL 1.e-60
      #DTOL 1.e4
      ITOL_UPDATE 1.d0
      #NO_INFINITY_NORM
      #NO_PRINT_CONVERGENCE
      #PRINT_DETAILED_CONVERGENCE
    END

    LINEAR_SOLVER FLOW
      #KSP_TYPE GMRES
      #PC_TYPE NONE
      #KSP_TYPE PREONLY
      #PC_TYPE LU
      #SOLVER GMRES
    END

    NEWTON_SOLVER TRANSPORT
      RTOL 1.d-12
      ATOL 1.d-12
      STOL 1.e-60
      DTOL 1.e4
      #ITOL_UPDATE 1.d-4
      #NO_INFINITY_NORM
      #NO_PRINT_CONVERGENCE
      #PRINT_DETAILED_CONVERGENCE
    END

    LINEAR_SOLVER TRANSPORT
      #KSP_TYPE GMRES
      #PC_TYPE NONE
      #KSP_TYPE PREONLY
      #PC_TYPE LU
      #SOLVER GMRES
    END

    #=========================== discretization ===================================
    GRID
      TYPE structured
      ORIGIN 0.d0 0.d0 0.d0
      NXYZ 1 1 136
      BOUNDS
        0.d0 0.d0 0.d0
        1.d0 1.d0 68.d0
      /
    END

    #=========================== fluid properties =================================
    FLUID_PROPERTY
      DIFFUSION_COEFFICIENT 1.d-9
    END

    #=========================== material properties ==============================
    MATERIAL_PROPERTY Backfill
      ID 1
      POROSITY 0.2585d0
      TORTUOSITY 0.5d0
      SATURATION_FUNCTION BF
      PERMEABILITY
        PERM_X 1.24e-12
        PERM_Y 1.24e-12
        PERM_Z 1.24e-12
      /
    END

    MATERIAL_PROPERTY Hanford-Fine-Sand
      ID 2
      POROSITY 0.3586
      TORTUOSITY 0.5d0
      SATURATION_FUNCTION HF
      PERMEABILITY
        PERM_X 3.37028e-13
        PERM_Y 3.37028e-13
        PERM_Z 3.37028e-13
      /
    END

    MATERIAL_PROPERTY Plio-Pleistocene
      ID 3
      POROSITY 0.4223d0
      TORTUOSITY 0.5d0
      SATURATION_FUNCTION PP
      PERMEABILITY
        PERM_X 3.73463e-14
        PERM_Y 3.73463e-14
        PERM_Z 3.73463e-14
      /
    END

    MATERIAL_PROPERTY Upper-Ringold-Gravel
      ID 4
      POROSITY 0.2625d0
      TORTUOSITY 0.5d0
      SATURATION_FUNCTION URG
      PERMEABILITY
        PERM_X 1.4392e-13
        PERM_Y 1.4392e-13
        PERM_Z 1.4392e-13
      /
    END

    MATERIAL_PROPERTY Middle-Ringold-Gravel
      ID 5
      POROSITY 0.1643
      TORTUOSITY 0.5d0
      SATURATION_FUNCTION MRG
      PERMEABILITY
        PERM_X 2.00395e-13
        PERM_Y 2.00395e-13
        PERM_Z 2.00395e-13
      /
    END

    #=========================== saturation functions =============================

    CHARACTERISTIC_CURVES BF
      SATURATION_FUNCTION VAN_GENUCHTEN
        M 0.6585d0
        ALPHA  1.008d-3
        LIQUID_RESIDUAL_SATURATION 0.0774
      /
      PERMEABILITY_FUNCTION MUALEM_VG_LIQ
        M 0.6585d0
        LIQUID_RESIDUAL_SATURATION 0.0774
      /
    END

    CHARACTERISTIC_CURVES HF
      SATURATION_FUNCTION VAN_GENUCHTEN
        M 0.46944d0
        ALPHA  9.40796d-5
        LIQUID_RESIDUAL_SATURATION 0.08366d0
      /
      PERMEABILITY_FUNCTION MUALEM_VG_LIQ
        M 0.46944d0
        LIQUID_RESIDUAL_SATURATION 0.08366d0
      /
    END

    CHARACTERISTIC_CURVES PP
      SATURATION_FUNCTION VAN_GENUCHTEN
        M 0.45587d0
        ALPHA  6.85145d-5
        LIQUID_RESIDUAL_SATURATION 0.25953d0
      /
      PERMEABILITY_FUNCTION MUALEM_VG_LIQ
        M 0.45587d0
        LIQUID_RESIDUAL_SATURATION 0.25953d0
      /
    END

    CHARACTERISTIC_CURVES URG
      SATURATION_FUNCTION VAN_GENUCHTEN
        M 0.38594d0
        ALPHA  2.96555d-5
        LIQUID_RESIDUAL_SATURATION 0.21295d0
      /
      PERMEABILITY_FUNCTION MUALEM_VG_LIQ
        M 0.38594d0
        LIQUID_RESIDUAL_SATURATION 0.21295d0
      /
    END

    CHARACTERISTIC_CURVES MRG
      SATURATION_FUNCTION VAN_GENUCHTEN
        M 0.39217d0
        ALPHA  6.34015e-5
        LIQUID_RESIDUAL_SATURATION 0.06086d0
      /
      PERMEABILITY_FUNCTION MUALEM_VG_LIQ
        M 0.39217d0
        LIQUID_RESIDUAL_SATURATION 0.06086d0
      /
    END


    #=========================== output options ===================================
    OUTPUT
      #SCREEN PERIODIC 10
      #MASS_BALANCE
      TIMES y 0.0383562 0.5 1.0 1.5 2.0 5.0 10.0 25. 50. 75. 100.
      FORMAT TECPLOT POINT
    # VELOCITIES
      PRINT_COLUMN_IDS
      PERIODIC_OBSERVATION TIMESTEP 1
    END

    #=========================== times ============================================
    TIME
      FINAL_TIME 100.d0 y
      INITIAL_TIMESTEP_SIZE 1.d-6 y
      MAXIMUM_TIMESTEP_SIZE 1.d-2 y
      MAXIMUM_TIMESTEP_SIZE 1.d0 y at 10 y
      MAXIMUM_TIMESTEP_SIZE 10.d0 y at 100 y
    END

    #=========================== regions ==========================================
    REGION all
      COORDINATES
        0.d0 0.d0 0.d0
        1.d0 1.d0 136.d0
      /
    END

    REGION MRG
      COORDINATES
        0.d0 0.d0 0.d0
        1.d0 1.d0 20.d0
      /
    END

    REGION URG
      COORDINATES
        0.d0 0.d0 20.d0
        1.d0 1.d0 23.d0
      /
    END

    REGION PP
      COORDINATES
        0.d0 0.d0 23.d0
        1.d0 1.d0 29.d0
      /
    END

    REGION HF
      COORDINATES
        0.d0 0.d0 29.d0
        1.d0 1.d0 52.d0
      /
    END

    REGION BF
      COORDINATES
        0.d0 0.d0 52.d0
        1.d0 1.d0 68.d0
      /
    END

    #=============boundaries=================

    REGION west
      FACE WEST
      COORDINATES
        0.d0 0.d0 0.d0
        0.d0 1.d0 68.d0
      /
    END

    REGION east
      FACE EAST
      COORDINATES
        1.d0 0.d0 0.d0
        1.d0 1.d0 68.d0
      /
    END

    REGION north
      FACE NORTH
      COORDINATES
        0.d0 1.d0 0.d0
        1.d0 1.d0 68.d0
      /
    END

    REGION south
      FACE SOUTH
      COORDINATES
        0.d0 0.d0 0.d0
        1.d0 0.d0 68.d0
      /
    END

    REGION top
      FACE TOP
      COORDINATES
        0.d0 0.d0 68.d0
        1.d0 1.d0 68.d0
      /
    END

    REGION bottom
      FACE BOTTOM
      COORDINATES
        0.d0 0.d0 0.d0
        1.d0 1.d0 0.d0
      /
    END

    REGION well
      COORDINATES
        1.d0 1.d0 52.d0
        1.d0 1.d0 52.d0
      /
    END

    #=========================== flow conditions ==================================
    FLOW_CONDITION initial
      TYPE
        PRESSURE hydrostatic
      /
      DATUM 0.d0 0.d0 6.d0
      PRESSURE 101325.d0
    END

    FLOW_CONDITION infiltration
      TYPE
        FLUX neumann
      /
    # FLUX 2.53678e-8 ! 0.08 m/yr
    # FLUX 2.53678e-9 ! 0.08 m/yr
      FLUX 2.53678e-10 ! 8 mm/yr
    # FLUX 0.d0
    END

    FLOW_CONDITION water_table
      TYPE
        PRESSURE hydrostatic
      /
      DATUM 0.d0 0.d0 6.d0
      PRESSURE 101325.d0
      #PRESSURE 1.4e5 ! 200 meter piezometric head (200*997.32*9.81)
    END

    FLOW_CONDITION source
      TYPE
        RATE mass_rate
      /
      RATE LIST
      TIME_UNITS s
      DATA_UNITS kg/s
      0.  0.187e-4
      1.21293e6 0.
      /
    END

    #=========================== transport conditions =============================
    TRANSPORT_CONDITION initial
      TYPE zero_gradient
      CONSTRAINT_LIST
        0.d0 initial
      /
    END

    TRANSPORT_CONDITION boundary
      TYPE zero_gradient
      CONSTRAINT_LIST
        0.d0 initial
      /
    END

    TRANSPORT_CONDITION infiltration
      TYPE dirichlet
      CONSTRAINT_LIST
        0.d0 infiltration
      /
    END

    TRANSPORT_CONDITION source
      TYPE dirichlet
      CONSTRAINT_LIST
        0.d0 well
      /
    /

    #=========================== condition couplers ===============================
    # initial condition
    INITIAL_CONDITION
      FLOW_CONDITION initial
      TRANSPORT_CONDITION initial
      REGION all
    END

    # top boundary condition
    BOUNDARY_CONDITION top
      #FLOW_CONDITION initial
      FLOW_CONDITION infiltration
      TRANSPORT_CONDITION initial
      REGION top
    END

    # bottom boundary condition
    BOUNDARY_CONDITION bottom
      FLOW_CONDITION water_table
      TRANSPORT_CONDITION initial
      REGION bottom
    END

    # well source/sink
    #skip
    SOURCE_SINK well
      FLOW_CONDITION source
      TRANSPORT_CONDITION source
      REGION well
    END
    #noskip

    # infiltration source/sink
    skip
    SOURCE_SINK infil
      FLOW_CONDITION infiltration
      TRANSPORT_CONDITION infiltration
      REGION top
    END
    noskip

    #=========================== stratigraphy couplers ============================
    STRATA
      REGION MRG
      MATERIAL Middle-Ringold-Gravel
    END

    STRATA
      REGION URG
      MATERIAL Upper-Ringold-Gravel
    END

    STRATA
      REGION PP
      MATERIAL Plio-Pleistocene
    END

    STRATA
      REGION HF
      MATERIAL Hanford-Fine-Sand
    END

    STRATA
      REGION BF
      MATERIAL Backfill
    END

    skip
    STRATA
      REGION all
      MATERIAL Middle-Ringold-Gravel
    END
    noskip

    #=========================== constraints ======================================

    CONSTRAINT well
      CONCENTRATIONS
        Tracer 1.d0 T
      /
    END

    CONSTRAINT infiltration
      CONCENTRATIONS
        Tracer 1.d0 T
      /
    END

    CONSTRAINT initial
      CONCENTRATIONS
        Tracer 1.d-16 T
      /
    END

    END_SUBSURFACE

MPHASE {#subsection_mphase}
------

### $\mathrm{CO_2}$ Sequestration: 1D Example Problem and Comparison with\
TOUGHREACT

In this example problem involves sequentially coupling of
`MPHASE`and `CHEMISTRY`. The chemical system
consists of four primary species and 5 secondary species. Supercritical
$\mathrm{CO_2}$ is injected into a well located at the west boundary. Dirichlet
pressure boundary conditions are imposed at the east boundary. The
problem definition with associated parameters is given in
Table [tco2].

| Description                 |          Symbol            |                Value
| --------------------------- | -------------------------- | ----------------------------------------
|Domain                       |          $l$               |               100 m
|Permeability                 |          $k$               |          $10^{-15}$ m$^2$
|Porosity                     |       $\varphi$            |                0.12
|Tortuosity                   |         $\tau$             |                 1
|Injection Rate               |    $Q_{{\rm CO_2}}$        |$5\times 10^{-5}$ kg/s, duration 0.4 y
|Characteristic Curves        | modified van Genuchten     |           [see Eqns.\ref{pc}-\ref{sg}]
|                             |     $\lambda$              |              0.6
|                             |    ${{\alpha}}$            | $1.9 \times 10^{-5}$ Pa$^{-1}$
|                             |      $s_{rl}$              |               0
|                             |      $s_{rg}$              |               0
|                             |  $P_c^{\rm max}$           |           $10^7$ Pa
|Rock Density                 |        $\rho_r$            |           2650 kg/m$^3$
|Rock Specific Heat           |         $c_r$              |            1000 J/kg/K
|Rock Thermal Conductivity    |$\kappa_{\rm wet,\,dry}$    |             0.5 W/m/K

  : Problem definition and parameters used in the 1D $\mathrm{CO_2}$
  sequestration example.<span data-label="tco2"></span>

The PFLOTRAN initial aqueous solution corresponds to a brine with NaCl
concentration of 0.5 m. Mineral reactions are not considered. The
initial fluid composition taken from <span>pflotran.out</span> is listed
in Table [tinitial_co2].

|  Transport Condition: Initial                                                |
| ---------------------------------------------------------------------------- |
|    iterations:    20|
|            pH:   5.0273|
|ionic strength:   4.7915E-01 [mol/L]|
|charge balance:   1.1102E-16|
|      pressure:   1.6450E+07 [Pa]|
|   temperature:    54.50 [C]|
|   density H2O:   992.99 [kg/m^3]|
|ln / activity H2O:   0.0000E+00  1.0000E+00    [---]|
|mole fraction H2O:   9.8093E-01    [---]|
|mass fraction H2O:   9.7160E-01    [---]|

|primary  species | free molal     |   total molal  |    act coef     |  constraint  |
| --------------- | -------------- | -------------- | --------------- | ------------ |
|H+               |     1.1727E-05 | 2.5844E-17     | 8.0079E-01      |  chrg |
|Na+              |     4.7913E-01 | 5.0000E-01     | 6.8288E-01      | total aq|
|Cl-              |     4.7913E-01 | 5.0000E-01     | 6.4459E-01      | total aq|
|CO2(aq)          |     1.1380E-04 | 1.2551E-04     | 1.1053E+00      | CO2(g)|

| complex         |      molality   |  act coef   | logK         |
| --------------- | --------------- | ----------- | ------------ |
|NaCl(aq)         |     2.0866E-02  | 1.0000E+00  |  6.8511E-01  |
|HCO3-            |     1.1713E-05  | 6.8288E-01  |  6.2239E+00  |
|OH-              |     1.2056E-08  | 6.6467E-01  |  1.3123E+01  |
|NaOH(aq)         |     1.6487E-09  | 1.0000E+00  |  1.3325E+01  |
|CO3--            |     3.2433E-10  | 2.0899E-01  |  1.6323E+01  |



The defining equations for the saturation and relative permeability
functions for the aqueous solution and supercritical $\mathrm{CO_2}$ are given by
the van Genuchten -Corey relations. For the aqueous solution van
Genuchten curves are used for capillary pressure $P_c$

\begin{align}
\label{pc}
P_c(s_e) {\ =\ }\frac{1}{{{\alpha}}}\Big[\big(s_e\big)^{-1/\lambda} -1\big]^{1-\lambda},
\end{align}

and relative permeability $k_{rl}$

\begin{align}
k_{rl} {\ =\ }\sqrt{s_e}\left\{1-\left[1-\big( s_e \big)^{1/\lambda}\right]^\lambda\right\}^2,
\end{align}

with effective saturation $s_e$ defined by

\begin{align}
s_e {\ =\ }\frac{s_l - s_{lr}}{1-s_{lr}}.
\end{align}

For the supercritical $\mathrm{CO_2}$ phase the Corey curve is used defined by

\begin{align}
k_{rg} {\ =\ }\big(1-s'\big)^2 \big[1-(s')^2\big],
\end{align}
 with
\begin{align}
\label{sg}
s' {\ =\ }\frac{s_l-s_{lr}}{1-s_{lr}-s_{gr}}.
\end{align}


Shown in [Figure 6](#fig:fco2) is a comparison of PFLOTRAN with TOUGHREACT
(TOUGHREACT results provided by Alt-Epping and Wanner, private communication).
The same thermodynamic database is used for both codes. Only slight
differences can be seen.
The $\mathrm{CO_2}$ aqueous and total concentrations are
essentially identical for PFLOTRAN in the low pH region where
supercritical $\mathrm{CO_2}$ is present, with slight differences for TOUGHREACT.

![Comparison with TOUGHREACT (dashed curves) and PFLOTRAN
(solid curves) after an elapsed time of 0.4 y corresponding to the end
of injection. Reasonable agreement is obtained between the two codes.](./figs/tr-pflotran-brine.png){#fig:fco2}
*Figure 7. Comparison with TOUGHREACT (dashed curves) and PFLOTRAN
(solid curves) after an elapsed time of 0.4 y corresponding to the end
of injection. Reasonable agreement is obtained between the two codes.*

![Liquid (blue curve) and supercritical $\mathrm{CO_2}$ (red curve) pressures
predicted by PFLOTRAN after an elapsed time of 0.4 y corresponding to the
end of injection. Also shown is the $\mathrm{CO_2}$ saturation (green curve).](./figs/p.png){#fig:fp}
*Figure 8. Liquid (blue curve) and supercritical $\mathrm{CO_2}$ (red curve) pressures
predicted by PFLOTRAN after an elapsed time of 0.4 y corresponding to the
end of injection. Also shown is the $\mathrm{CO_2}$ saturation (green curve).*

Note that the $\mathrm{CO_2}$ aqueous concentration
(and mole fraction $X_{\rm CO_2}$ although not visible in the figure) obtained from
PFLOTRAN is not exactly constant. This is caused, presumably, by a
change in pressure as shown in [Figure 7](#fig:fp) for the liquid and $\mathrm{CO_2}$
pressures in addition to the $\mathrm{CO_2}$ saturation $s_{\rm CO_2}$.


### $\mathrm{CO_2}$ Sequestration in the Presence of a Leaky Well

The simulation domain has a lateral extent of $1,000\times 1,000$ m and
vertical height of 160 m. The leaky well is at the center of the domain
and the injection well is 100 m east. There are two aquifers at the top
and bottom of the domain, each 30 m thick, and an aquitard with
thickness of 100 m sandwiched between the two aquifers. The leaky well
is modeled as a porous medium with a higher permeability compared to the
formation. Parameter values used in the simulation are listed in
Table [tleaky_params]. Other parameters used for characteristic
curves, heat conduction, etc. may be found in the input file listing
(see Table [tleaky-co2in]).

The initial conditions consist of hydrostatic pressure, and isothermal
temperature of 34$^\circ$C. The initial pressure at the
bottom of the domain is $3.086\times 10^7$ Pa (at 3,000 m depth). At the
lateral boundaries, hydrostatic boundary conditions are imposed on the
system. The boundaries at the top and bottom of the domain are no flow
boundary conditions. $\mathrm{CO_2}$ is injected at a constant rate of 8.87 kg/s
for the duration of the simulation of 1000 days and at a constant
temperature of 33.6$^\circ$C.

The computational domain was discretized into
$200 \times 200 \times 32$ grid blocks with spacing
$\Delta x = \Delta y = 5$ m, and $\Delta z = 5$ m. The total
number of degrees of freedom are 3,840,000. The problem was run on 512
processes on the supercomputer Yellowstone at the NCAR-Wyoming
Supercomputing Center.

| Unit         | Permeability [m$^2$]   | Porosity [—]   | Depth [m]     |
| ------------ | ------------------------ | ---------------- | --------------- |
| Aquifer      |  $2 \times 10^{-14}$     |     0.15         | 0–30, 130–160   |
| Aquitard     |  $1 \times 10^{-18}$     |     0.15         |  30–130         |
| Leaky well   |  $1 \times 10^{-12}$     |     0.15         |   0–160         |

: Model parameters.<span data-label="tleaky_params"></span>

Results of the simulation for an elapsed time of 250 days are shown in
[Figure 8](#fig:f250d) for liquid pressure and saturation of supercritical
$\mathrm{CO_2}$. Supercritical $\mathrm{CO_2}$ proceeds up the leaky well until it ponds
at the top of the domain where a closed boundary is imposed.

The leakage of $\mathrm{CO_2}$ through the leaky well as a function of time is
shown in [Figure 9](#fig:fleaky_flx). This is defined as the $\mathrm{CO_2}$ mass flow
midway between the top and bottom domain divided by the injection rate.
The maximum value in the leak occurs at approximately 800 d. The leak
begins at approximately 50 d. The results can be compared to Ebigo et
al. (2007), Figure 8. It should be noted that the leakage rate is highly
sensitive to the lateral grid spacing.

![Pressure supercritical $\mathrm{CO_2}$ saturation; for an elapsed
time of 250 days.](./figs/liq_p_250d.png){#fig:f250d}
*Figure 9. Pressure supercritical $\mathrm{CO_2}$ saturation; for an elapsed
time of 250 days.*

![Supercritical $\mathrm{CO_2}$ saturation; for an elapsed
time of 250 days.](./figs/sg-250d.png)
*Figure 10. Supercritical $\mathrm{CO_2}$ saturation; for an elapsed
time of 250 days.*

![Leakage rate relative to injection rate.](./figs/leakage_flx.png){#fig:fleaky_flx}
*Figure 11. Leakage rate relative to injection rate.*

References
==========

Balay S., V. Eijkhout V, W.D. Gropp, L.C. McInnes and B.F. Smith (1997)
Modern Software Tools in Scientific Computing, Eds. Arge E, Bruaset AM
and Langtangen HP (Birkhaüser Press), pp. 163–202.

Coats, K.H. and A.B. Ramesh (1982) Effects of Grid Type and Difference
Scheme on Pattern Steamflood Simulation Results, paper SPE-11079,
presented at the 57th Annual Fall Technical Conference and Exhibition of
the Society of Petroleum Engineers, New Orleans, LA, September 1982.

Ebigbo, A., Holger Class, H., Helmig, R. (2007) $\mathrm{CO_2}$ leakage through
an abandoned well: problem-oriented benchmarks, Comput Geosciences
11:103?115 DOI 10.1007/s10596-006-9033-7.

Fenghour, A., W.A. Wakeham, and V. Vesovic (1998) The viscosity of
carbon dioxide, J. Phys. Chem. Ref. Data, 27(1), 31–44.

Goode, D.J. (1996) Direct simulation of groundwater age, Water Resources
Research, 32, 289–296.

Hammond, G.E., P.C. Lichtner, C. Lu, and R.T. Mills (2011) PFLOTRAN:
Reactive Flow & Transport Code for Use on Laptops to Leadership-Class
Supercomputers, Editors: Zhang, F., G. T. Yeh, and J. C. Parker,
<span>*Ground Water Reactive Transport Models*</span>, Bentham Science
Publishers. ISBN 978-1-60805-029-1.

Khaleel, R., E.J. Freeman (1995) Variability and scaling of hydraulic
properties for 200 area soils, Hanford Site. Report WHC-EP-0883.
Westinghouse Hanford Company, Richland, WA.

Khaleel, R., T.E. Jones, A.J. Knepp, F.M. Mann, D.A. Myers, P.M. Rogers,
R.J. Serne, and M.I. Wood (2000) Modeling data package for S-SX Field
Investigation Report (FIR). Report RPP-6296, Rev. 0. CH2M Hill Hanford
Group, Richland, WA.

Lichtner, P.C., Yabusaki, S.B., Pruess K., and Steefel, C.I. (2004) Role
of Competitive Cation Exchange on Chromatographic Displacement of Cesium
in the Vadose Zone Beneath the Hanford S/SX Tank Farm,
<span>*VJZ*</span>, <span>**3**</span>, 203–219.

Lichtner, P.C. (1996a) Continuum Formulation of
Multicomponent-Multiphase Reactive Transport, In: <span>*Reactive
Transport in Porous Media*</span> (eds. P. C. Lichtner, C. I. Steefel,
and E. H. Oelkers), <span>*Reviews in Mineralogy*</span>,
<span>**34**</span>, 1–81.

Lichtner P.C. (1996b) Modeling Reactive Flow and Transport in Natural
Systems, Proceedings of the Rome Seminar on Environmental Geochemistry,
Eds. G. Ottonello and L. Marini, Castelnuovo di Porto, May 22–26, Pacini
Editore, Pisa, Italy, 5–72.

Lichtner P.C. (2000) Critique of Dual Continuum Formulations of
Multicomponent Reactive Transport in Fractured Porous Media, Ed. Boris
Faybishenko, <span>*Dynamics of Fluids in Fractured Rock*</span>,
Geophysical Monograph <span>**122**</span>, 281–298.

Painter, S.L. (2011) Three-phase numerical model of water migration in
partially frozen geological media: model formulation, validation, and
applications, Computational Geosciences <span>**15**</span>, 69-85.

Peaceman, D.W. (1977) Interpretation of Well-Block Pressures in
Numerical Reservoir Simulation with Nonsquare Grid Blocks and
Anisotropic Permeability, paper SPE-10528, presented at the Sixth SPE
Symposium on Reservoir Simulation of the Society of Petroleum Engineers,
New Orleans, LA, January 1982.

Pruess, K., S. Yabusaki, C. Steefel, and P. Lichtner (2002) Fluid flow,
heat transfer, and solute transport at nuclear waste storage tanks in
the Hanford vadose zone. Available at www.vadosezonejournal.org. Vadose
Zone J. 1:68–88.

Pruess, K., and Narasimhan (1985) A practical method for modeling fluid
and heat flow in fractured porous media, SPE 10509, 14–26.

Somerton, W.H., A.H. El-Shaarani, and S.M. Mobarak (1974) High
temperature behavior of rocks associated with geothermal-type
reservoirs. Paper SPE-4897. Proceedings of the 44th Annual California
Regional Meeting of the Society of Petroleum Engineers. Richardson, TX:
Society of Petroleum Engineers.

Andreas Voegelin, Vijay M. Vulava, Florian Kuhnen, Ruben Kretzschmar
(2000) Multicomponent transport of major cations predicted from binary
adsorption experiments, Journal of Contaminant Hydrology, 46, 319–338.
Appendix A: Governing Equations {#appendix-a-governing-equations}
==============================================================================

Mode: <span>RICHARDS</span>
---------------------------

<span>RICHARDS</span> Mode applies to single phase, variably saturated,
isothermal systems. The governing mass conservation equation is given by
\begin{align}
\frac{{{\partial}}}{{{\partial}}t}\left(\varphi s\rho\right) + {\boldsymbol{\nabla}}\cdot\left(\rho{\boldsymbol{q}}\right) = Q_w,
\end{align}
with Darcy flux ${\boldsymbol{q}}$ defined as
\begin{align}
{\boldsymbol{q}}= -\frac{kk_r(s)}{\mu}{\boldsymbol{\nabla}}\left(P-W_w\rho g z\right).
\end{align}

Here, $\varphi$ denotes porosity [-], $s$ saturation
[m$^3$m$^{-3}$], $\rho$ water density [kmol m$^{-3}$],
${\boldsymbol{q}}$ Darcy flux [m s$^{-1}$], $k$ intrinsic permeability
[m$^2$], $k_r$ relative permeability [-], $\mu$ viscosity [Pa s],
$P$ pressure [Pa], $W_w$ formula weight of water [kg kmol$^{-1}$],
$g$ gravity [m s$^{-2}$], and $z$ the vertical component of the
position vector [m]. Supported relative permeability functions $k_r$
for Richards’ equation include van Genuchten, Books-Corey and
Thomeer-Corey, while the saturation functions include Burdine and
Mualem. Water density and viscosity are computed as a function of
temperature and pressure through an equation of state for water. The
source/sink term $Q_w$ [kmol m$^{-3}$ s$^{-1}$] has the form
\begin{align}
Q_w {\ =\ }\frac{q_M}{W_w} \delta({\boldsymbol{r}}-{\boldsymbol{r}}_{ss}),
\end{align}

where $q_M$ denotes a mass rate in kg/m$^{3}$/s, and
${\boldsymbol{r}}_{ss}$ denotes the location of the source/sink.

Capillary Pressure Relations
----------------------------

Capillary pressure is related to saturation by various phenomenological
relations, one of which is the van Genuchten (1980) relation
\begin{align}
\label{seff}
s_e {\ =\ }\left[1+\left( \frac{p_c}{p_c^0} \right)^n
\right]^{-m},\end{align}
 where $p_c$ represents the capillary pressure [Pa],
and the effective saturation $s_e$ is defined by
\begin{align}
s_e {\ =\ }\frac{s - s_r}{s_0 - s_r},\end{align}
 where $s_r$ denotes the
residual saturation, and $s_0$ denotes the maximum saturation. The
inverse relation is given by
\begin{align}
p_c {\ =\ }p_c^0 \left(s_e^{-1/m}-1\right)^{1/n}.
\end{align}
 The quantities
$m$, $n$ and $p_c^0$ are impirical constants determined by fitting to
experimental data.

### Brooks-Corey Saturation Function

The Brooks-Corey saturation function is a limiting form of the van
Genuchten relation for $p_c/p_c^0 \gg 1$, with the form
\begin{align}
s_e {\ =\ }\left(\frac{p_c}{p_c^0}\right)^{-\lambda},
\end{align}
 with
$\lambda=mn$ and inverse relation \begin{align}
p_c {\ =\ }p_c^0 s_e^{-1/\lambda}.\end{align}


### Relative Permeability

Two forms of the relative permeability function are implemented based on
the Mualem and Burdine formulations. The quantity $n$ is related to $m$
by the expression \begin{align}
\label{lambda_mualem}
m {\ =\ }1-\frac{1}{n}, \ \ \ \ \ n {\ =\ }\frac{1}{1-m},\end{align}
 for the
Mualem formulation and by \begin{align}
\label{lambda_burdine}
m {\ =\ }1-\frac{2}{n}, \ \ \ \ \ n {\ =\ }\frac{2}{1-m},\end{align}
 for the
Burdine formulation.

For the Mualem relative permeability function based on the van Genuchten
saturation function is given by the expression \begin{align}
\label{krl_mualem}
k_{r} {\ =\ }\sqrt{s_e} \left\{1 - \left[1- \left( s_e \right)^{1/m} \right]^m \right\}^2.
\end{align}

The Mualem relative permeability function based on the Brooks-Corey
saturation function is defined by
\begin{align}
k_r &{\ =\ }\big(s_e\big)^{5/2+2/\lambda},\\
&{\ =\ }\big(p_c/p_c^0\big)^{-(5\lambda/2+2)}.
\end{align}


For the Burdine relative permeability function based on the van
Genuchten saturation function is given by the expression
\begin{align}
\label{krl_burdine}
k_{r} {\ =\ } s_e^2 \left\{1 - \left[1- \left( s_e \right)^{1/m} \right]^m \right\}.
\end{align}

The Burdine relative permeability function based on the Brooks-Corey
saturation function has the form
\begin{align}
k_r &{\ =\ }\big(s_e\big)^{2+3/\lambda} \\
 &{\ =\ }\left(\frac{p_c}{p_c^0}\right)^{-(2+3\lambda)}.
\end{align}


### Smoothing

At the end points of the saturation and relative permeability functions
it is sometimes necessary to smooth the functions in order for the
Newton-Raphson equations to converge. This is accomplished using a third
order polynomial interpolation by matching the values of the function to
be fit (capillary pressure or relative permeability), and imposing zero
slope at the fully saturated end point and matching the derivative at a
chosen variably saturated point that is close to fully saturated. The
resulting equations for coefficients $a_i$, $i=0-3$, are given by

\begin{align}
a_0 + a_1 x_1 + a_2 x_1^2 + a_3 x_1^3 &{\ =\ }f_1,\\
a_0 + a_1 x_2 + a_2 x_2^2 + a_3 x_2^3 &{\ =\ }f_2,\\
a_1 x_1 + 2a_2 x_1 + 3a_3 x_1^2 &{\ =\ }f_1',\\
a_1 x_2 + 2a_2 x_2 + 3a_3 x_2^2 &{\ =\ }f_2',
\end{align}


for chosen points $x_1$ and $x_2$. In matrix form these equations become
\begin{align}
\begin{bmatrix}
1 & x_1 & x_1^2 & x_1^3\\
1 & x_2 & x_2^2 & x_2^3\\
0 & 1 & 2x_1 & 3x_1^2\\
0 & 1 & 2x_2 & 3x_2^2
\end{bmatrix}
\begin{bmatrix}
a_0\\
a_1\\
a_2\\
a_3
\end{bmatrix}
{\ =\ }\begin{bmatrix}
f_1\\
f_2\\
f_1'\\
f_2'
\end{bmatrix}.
\end{align}
The conditions imposed on the smoothing equations for
capillary pressure $f=s_e(p_c)$ are $x_1=2 p_c^0$, $x_2=p_c^0/2$,
$f_1 = (s_e)_1$, $f_2 = 1$, $f_1' = (s_e')_1$, $f_2' = 0$. For relative
permeability $f=k_r(s_e)$, $x_1 = 1$, $x_2 = 0.99$, $f_1 = 1$,
$f_2 = (k_r)_2$, $f_1' = 0$, $f_2' = (k_r')_2$.


Mode: `MPHASE` {#sec_mphase}
-------------------------

The mode `MPHASE` solves the two-phase system of water and
supercritical $\mathrm{CO_2}$. It may also be coupled to chemistry using the
`CHEMISTRY` keyword and its various associated optional and
required keywords for selecting the primary and secondary aqueous
species and setting up initial and boundary conditions and source/sinks.
`MPHASE` requires that the species CO2(aq) be used as primary
species. In addition, for pure aqueous and supercritical $\mathrm{CO_2}$ phases,
the input to `MPHASE` requires specifying the mole fraction
of $\mathrm{CO_2}$. When coupled to chemistry, the $\mathrm{CO_2}$ mole fraction is
calculated internally directly from the aqueous concentrations specified
in the `CONSTRAINT` keyword.

Local equilibrium is assumed between phases for modeling multiphase
systems with PFLOTRAN. The multiphase partial differential equations for
mass and energy conservation solved by PFLOTRAN have the general form:

\begin{align}
\label{mass_conservation_equation}
\frac{{{\partial}}}{{{\partial}}t} \bigg(\varphi \sum_{{\alpha}}s_{{\alpha}}^{}\eta_{{\alpha}}^{} x_i^{{\alpha}}\bigg)
+ {\boldsymbol{\nabla}}\cdot\sum_{{\alpha}}{\boldsymbol{F}}_i^{{\alpha}}{\ =\ }Q_{i},\end{align}


for the $i$th component where the flux ${\boldsymbol{F}}_i^{{\alpha}}$
is given by
\begin{align}
{\boldsymbol{F}}_i^{{\alpha}}{\ =\ }{\boldsymbol{q}}_{{\alpha}}^{}\eta_{{\alpha}}^{} x_i^{{\alpha}}- \varphi s_{{\alpha}}^{} D_{{\alpha}}^{} \eta_{{\alpha}}^{} {\boldsymbol{\nabla}}x_i^{{\alpha}},\end{align}

and \begin{align}
\label{energy_equation}
\frac{{{\partial}}}{{{\partial}}t} \bigg(\varphi \sum_{{\alpha}}s_{{\alpha}}\eta_{{\alpha}}U_{{\alpha}}+ (1-\varphi) \rho_r c_r T\bigg)
+ {\boldsymbol{\nabla}}\cdot\sum_{{\alpha}}\bigg[{\boldsymbol{q}}_{{\alpha}}\eta_{{\alpha}}H_{{\alpha}}- \kappa{\boldsymbol{\nabla}}T\bigg] {\ =\ }Q_{e},\end{align}


for energy. In these equations ${{\alpha}}$ designates a fluid phase
(${{\alpha}}=l$, sc) at temperature $T$ and pressure $P_{{\alpha}}$ with
the sums over all fluid phases present in the system, and source/sink
terms $Q_i$ and $Q_e$ described in more detail below. Species are
designated by the subscript $i$
($i=\mathrm{H_2O}$, $\mathrm{CO_2}$);
$\varphi$ denotes the porosity of the porous medium; $s_{\alpha}$
denotes the phase saturation state; $x_i^{\alpha}$ denotes the mole
fraction of species $i$ satisfying

\begin{align}
\sum_i x_i^\alpha=1,\end{align}

the quantities $\eta_{{\alpha}}$, $H_{{\alpha}}$, $U_{{\alpha}}$ refer to
the molar density, enthalpy, and internal energy of each fluid phase,
respectively; and ${\boldsymbol{q}}_{{\alpha}}$ denotes the Darcy flow
rate for phase ${{\alpha}}$ defined by
\begin{align}
{\boldsymbol{q}}_{{\alpha}}{\ =\ }-\frac{kk_{{\alpha}}}{\mu_{{\alpha}}} {\boldsymbol{\nabla}}\big(P_{{\alpha}}-\rho_{{\alpha}}g {\boldsymbol{z}}\big),\end{align}

where $k$ refers to the intrinsic permeability, $k_{{\alpha}}$ denotes
the relative permeability, $\mu_{{\alpha}}$ denotes the fluid viscosity,
$W_{{\alpha}}^{}$ denotes the formula weight, $g$ denotes the
acceleration of gravity, and $z$ designates the vertical of the position
vector. The mass density $\rho_{{\alpha}}$ is related to the molar
density by the expression
\begin{align}
\rho_{{\alpha}}= W_{{\alpha}}\eta_{{\alpha}},\end{align}
 where the formula
weight $W_{{\alpha}}$ is a function of composition according to the
relation
\begin{align}
W_{{\alpha}}{\ =\ }\frac{\rho_{{\alpha}}}{\eta_{{\alpha}}} {\ =\ }\sum_i W_i^{} x_i^{{\alpha}}.\end{align}

The quantities $\rho_r$, $c_r$, and $\kappa$ refer to the mass density,
heat capacity, and thermal conductivity of the porous rock.

### Source/Sink Terms

The source/sink terms, $Q_i$ and $Q_e$, describe injection and
extraction of mass and heat, respectively, for various well models.
Several different well models are available. The simplest is a volume or
mass rate injection/production well given by

\begin{align}
Q_i &{\ =\ }\sum_n\sum_{{\alpha}}q_{{\alpha}}^V \eta_{{\alpha}}x_i^{{\alpha}}\delta({\boldsymbol{r}}-{\boldsymbol{r}}_{n}),\\
&{\ =\ }\sum_n\sum_{{\alpha}}\frac{\eta_{{\alpha}}}{\rho_{{\alpha}}} q_{{\alpha}}^M x_i^{{\alpha}}\delta({\boldsymbol{r}}-{\boldsymbol{r}}_{n}),\\
&{\ =\ }\sum_n\sum_{{\alpha}}W_{{\alpha}}^{-1} q_{{\alpha}}^M x_i^{{\alpha}}\delta({\boldsymbol{r}}-{\boldsymbol{r}}_{n}),\end{align}


where $q_{{\alpha}}^V$, $q_{{\alpha}}^M$ refer to volume and mass rates
with units m$^3$/s, kg/s, respectively, related by the density
\begin{align}
q_{{\alpha}}^M {\ =\ }\rho_{{\alpha}}q_{{\alpha}}^V.\end{align}
 The position
vector ${\boldsymbol{r}}_{n}$ refers to the location of the $n$th
source/sink.

A less simplistic approach is to specify the bottom well pressure to
regulate the flow rate in the well. In this approach the mass flow rate
is determined from the expression
\begin{align}
q_{{\alpha}}^M {\ =\ }\Gamma \rho_{{\alpha}}\frac{k_{{\alpha}}}{\mu_{{\alpha}}} \big(p_{{\alpha}}-p_{{\alpha}}^{\rm bw}\big),\end{align}

with bottom well pressure $p_{{\alpha}}^{\rm bw}$, and where $\Gamma$
denotes the well factor (production index) given by
\begin{align}
\Gamma {\ =\ }\frac{2\pi k \Delta z}{\ln\big(r_e/r_w\big) +  \sigma -1/2}.\end{align}

In this expression $k$ denotes the permeability of the porous medium,
$\Delta z$ refers to the layer thickness, $r_e$ denotes the grid block
radius, $r_w$ denotes the well radius, and $\sigma$ refers to the skin
thickness factor. For a rectangular grid block of area
$A=\Delta x \Delta y$, $r_e$ can be obtained from the relation
\begin{align}
r_e {\ =\ }\sqrt{A/\pi}.\end{align}
 See Peaceman (1977) and Coats and Ramesh
(1982) for more details.

### Variable Switching

In PFLOTRAN a variable switching approach is used to account for phase
changes enforcing local equilibrium. According to the Gibbs phase rule
there are a total of $N_C+1$ degrees of freedom where $N_C$ denotes
the number of independent components. This can be seen by noting that
the intensive degrees of freedom are equal to
$N_{\rm int}=N_C - N_P +2$, where $N_P$ denotes the number
of phases. The extensive degrees of freedom equals
$N_{\rm ext}=N_P-1.$ This gives a total number of degrees of
freedom $N_{\rm dof}=N_{\rm int}+N_{\rm ext}=N_C+1$,
independent of the number of phases $N_P$ in the system. Primary
variables for liquid, gas and two-phase systems are listed in
Table [tvar]. The conditions for phase changes to occur are considered
in detail below.

| State       |  $X_1$ |  $X_2$  |       $X_3$
| ----------- |------- | ------- | --------------------
| Liquid      | $p_l$  |  $T$    | $x_{{\rm CO_2}}^l$
| Gas         | $p_g$  |  $T$    | $x_{{\rm CO_2}}^g$
| Two-Phase   | $p_g$  |  $T$    |      $s_g$

  : Choice of primary variables.<span data-label="tvar"></span>

#### Gas: $(p_g,\,T,\,x_{{\rm CO_2}}^g)$ $\rightarrow$ Two-Phase: $(p_g,\,T,\,s_g^{})$

 

$\bullet$ gas $\rightarrow$ 2-ph:
$x_{{\rm CO_2}}^g \leq 1-\dfrac{P_{\rm sat}(T)}{p_g}$,  or equivalently:
$x_{{\rm H_2O}}^g \geq \dfrac{P_{\rm sat}(T)}{p_g}$

#### Liquid: $(p_l,\,T,\,x_{{\rm CO_2}}^l)$ $\rightarrow$ Two-phase: $(p_g,\,T,\,s_g^{})$

 

$\bullet$ liq $\rightarrow$ 2-ph:
$x_{{\rm CO_2}}^l \geq x_{{\rm CO_2}}^{eq}$

The equilibrium mole fraction $x_{{\rm CO_2}}^{eq}$ is given by
\begin{align}
x_{{\rm CO_2}}^{eq} {\ =\ }\frac{m_{{\rm CO_2}}}{W_{{\rm H_2O}}^{-1} + m_{{\rm CO_2}}+  \sum_{l\ne {{\rm H_2O}},\,{{\rm CO_2}}} m_l},\end{align}

where the molality at equilibrium is given by
\begin{align}
m_{{\rm CO_2}}^{eq} {\ =\ }\left(1-\dfrac{P_{\rm sat}(T)}{p}\right)\frac{\phi_{{\rm CO_2}}p}{K_{{\rm CO_2}}\gamma_{{\rm CO_2}}},\end{align}

where it is assumed that
\begin{align}
y_{{\rm CO_2}}^{} {\ =\ }1-\dfrac{P_{\rm sat}(T)}{p}.\end{align}


#### Two-Phase: $(p_g,\,T,\,s_g)$ $\rightarrow$ Liquid $(p_l,\,T,\,x_{{\rm CO_2}}^l)$ or Gas $(p_g,\,T,\,x_{{\rm CO_2}}^g)$

 

Equilibrium in a two-phase ${{\rm H_2O}}$–${{\rm CO_2}}$ system is
defined as the equality of chemical potentials between the two phases as
expressed by the relation
\begin{align}
f_{{\rm CO_2}}^{} {\ =\ }y_{{\rm CO_2}}^{}\phi_{{\rm CO_2}}^{} p_g^{} {\ =\ }K_{{\rm CO_2}}^{} \big(\gamma_{{\rm CO_2}}^{} m_{{\rm CO_2}}^{}\big),\end{align}

where \begin{align}
y_{{\rm CO_2}}^{} {\ =\ }x_{{\rm CO_2}}^g,\end{align}

\begin{align}
x_{{\rm H_2O}}^g {\ =\ }\frac{P_{\rm sat}(T)}{p_g},\end{align}
 and
\begin{align}
y_{{\rm CO_2}}^{} {\ =\ }1-x_{{\rm H_2O}}^g {\ =\ }1-\frac{P_{\rm sat}(T)}{p_g}.\end{align}

From these equations a Henry coefficient-like relation can be written as
\begin{align}
y_{{\rm CO_2}}^{} {\ =\ }\widetilde K_{{\rm CO_2}}^{} x_{{\rm CO_2}}^{},\end{align}

where \begin{align}
x_{{\rm CO_2}}^{} {\ =\ }x_{{\rm CO_2}}^l,\end{align}

\begin{align}
\widetilde K_{{\rm CO_2}}^{} {\ =\ }\frac{\gamma_{{\rm CO_2}}^{} K_{{\rm CO_2}}^{}}{\phi_{{\rm CO_2}}^{} p_g}\frac{m_{{\rm CO_2}}}{x_{{\rm CO_2}}}.\end{align}


$\bullet$ A phase change to single liquid or gas phase occurs if
$s_g \leq 0$ or $s_g\geq 1$, respectively.

Conversion relations between mole fraction $(x_i)$, mass fraction
$(w_i)$ and molality $(m_i)$ are as follows:

Molality–mole fraction:
\begin{align}
m_i {\ =\ }\frac{n_i}{M_{{\rm H_2O}}} {\ =\ }\frac{n_i}{W_{{\rm H_2O}}n_{{\rm H_2O}}} {\ =\ }\frac{x_i}{W_{{\rm H_2O}}x_{{\rm H_2O}}} {\ =\ }\frac{x_i}{W_{{\rm H_2O}}\big(1-\sum_{l\ne{{\rm H_2O}}} x_l\big)}\end{align}

Mole fraction–molality:
\begin{align}
x_i {\ =\ }\frac{n_i}{N} {\ =\ }\frac{n_i}{M_{{\rm H_2O}}}\frac{M_{{\rm H_2O}}}{N} {\ =\ }\frac{m_i}{\sum m_l} {\ =\ }\frac{W_{{\rm H_2O}}m_i}{1+W_{{\rm H_2O}}\sum_{l\ne{{\rm H_2O}}} m_l}\end{align}

Mole fraction–mass fraction:
\begin{align}
x_i {\ =\ }\frac{n_i}{N} {\ =\ }\frac{W_i^{-1} W_i n_i}{\sum W_l^{-1} W_l n_l} {\ =\ }\frac{W_i^{-1} w_i}{\sum W_l^{-1} w_l}\end{align}

Mass fraction–mole fraction:
\begin{align}
w_i {\ =\ }\frac{M_i}{M} {\ =\ }\frac{W_i n_i}{\sum W_l n_l} {\ =\ }\frac{W_i x_i}{\sum W_l x_l}\end{align}


### Sequentially Coupling `MPHASE` and `CHEMISTRY`

MPHASE and CHEMISTRY may be sequentially coupled to one another by
including the CHEMISTRY keyword in the MPHASE input file and adding the
requisite associated keywords. At the end of an MPHASE time step the
quantities $p$, $T$, $s_g$, $q_l$ and $q_g$ are passed to the reactive
transport equations. These quantities are interpolated between the
current time $t_{\rm MPH}$ and the new time
$t_{\rm MPH}+\Delta t_{\rm MPH}$. The reactive transport equations may
need to sub-step the MPHASE time step, i.e.
$\Delta t_{\rm RT} \leq \Delta t_{\rm MPH}$. Coupling also occurs from
the reactive transport equations back to MPHASE. This is through changes
in material properties such as porosity, tortuosity and permeability
caused by mineral precipitation and dissolution reactions (see
§[sec_mat_prop]). In addition, coupling occurs through consumption
and production of $\mathrm{H_2O}$ and $\mathrm{CO_2}$ by mineral precipitation/dissolution
reactions occurring in the reactive transport equations. This effect is
accounted for by passing the reaction rates $R_{\mathrm{H_2O}}$ and
$R_{\mathrm{CO_2}}$ given by
\begin{align}
R_j {\ =\ }-\sum_m\nu_{jm}I_m,
\end{align}
back to the MPHASE conservation equations.

A further constraint on the reactive transport equations for aqueous
$\mathrm{CO_2}$ is that it must be in equilibrium with supercritical $\mathrm{CO_2}$ in
regions where $0< s_g <1$. This is accomplished by replacing the $\mathrm{CO_2}$
mass conservation equations in those regions with the constraint
$m_{\rm CO_{\rm 2(aq)}} = m_{\rm CO_2}^{\rm eq}$.


Mode: `IMMIS`
------------------------

The `IMMIS` mode applies to multiple completely immiscible
phases. The code PIMS, parallel immiscible multiphase flow simulator, is
a simplified version of the MPHASE mode in which the dependency on
thermodynamic relations have been removed, since for immiscible systems
the solubility is identically zero for each component. In this case the
number of components is equal to the number of phases, or degrees of
freedom associated with each node for an isothermal system. The
immiscible property removes the variable switching strategy used in
MPHASE, which may be the most numerically difficult part of PFLOTRAN,
and may cause problems for multi-level solvers. The governing equations
solved by PIMS are given by \begin{align}
\label{mass}
\frac{{{\partial}}}{{{\partial}}t}\big(\varphi\rho_{{\alpha}}^{} s_{{\alpha}}^{}\big) + {\boldsymbol{\nabla}}\cdot \big(\rho_{{\alpha}}^{} {\boldsymbol{q}}_{{\alpha}}\big) {\ =\ }Q_{{\alpha}},\end{align}

where the subscript ${{\alpha}}$ denotes an immiscible phase.

In this equation $\varphi$ is porosity, $s_{{\alpha}}$,
$\rho_{{\alpha}}$ refer to the ${{\alpha}}$th phase saturation and
density, respectively, ${\boldsymbol{q}}_{{\alpha}}$ is the Darcy
velocity of the ${{\alpha}}$th phase given by
\begin{align}
{\boldsymbol{q}}_{{\alpha}}{\ =\ }-\frac{kk_{{\alpha}}}{\mu_{{\alpha}}} \big({\boldsymbol{\nabla}}p-\rho_{{\alpha}}g \hat{\boldsymbol{z}}\big),\end{align}

with permeability $k$, relative permeability $k_{{\alpha}}$, fluid
viscosity $\mu_{{\alpha}}$, and $Q_{{\alpha}}$ is the source/sink term.
The selection of primary variables are pressure $p$ and $n-1$
independent phase saturation variables
$s_{{\alpha}}, {{\alpha}}=1,...,n-1$ with
\begin{align}
\sum_{{{\alpha}}=1}^n s_{{\alpha}}= 1.\end{align}
 The mass conservation
equations are coupled to the energy balance equation given by
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \Big(\varphi\sum_{{\alpha}}s_{{\alpha}}\rho_{{\alpha}}U_{{\alpha}}+ (1-\varphi) \rho_r C_r T\Big) + {\boldsymbol{\nabla}}\cdot\Big(\sum_{{\alpha}}\rho_{{\alpha}}{\boldsymbol{q}}_{{\alpha}}H_{{\alpha}}- \kappa{\boldsymbol{\nabla}}T\Big) {\ =\ }Q_e,\end{align}

where $U_{{\alpha}}$, $H_{{\alpha}}$ denote the internal energy and
enthalpy of the ${{\alpha}}$th fluid phase, $\kappa$ denotes the thermal
conductivity of the bulk porous medium, $\rho_r$, $C_r$ denote the rock
density and heat capacity, and $T$ refers to the temperature. Thus the
number of equations is equal to number of phases plus one, which is
equal to the number of unknowns: ($p$, $T$, $s_1$, …, $s_{n-1}$).

Mode: `MISCIBLE`
---------------------------

The miscible mode applies to a mixture of water and proplyene glycol
(PPG). In terms of molar density for the mixture $\eta$ and mole
fractions $x_i$, $i$=1 (water), $i$=2 (PPG), the mass conservation
equations have the form
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi \eta x_i + {\boldsymbol{\nabla}}\cdot\left[{\boldsymbol{q}}\eta x_i - \varphi D \eta {\boldsymbol{\nabla}}x_i\right] {\ =\ }Q_i,\end{align}

with source/sink term $Q_i$. It should be noted that the mass- and
mole-fraction formulations of the conservation equations are not exactly
equivalent. This is due to the diffusion term which gives an extra term
when transformed from the mole-fraction to mass-fraction gradient.

The molar density $\eta$ is related to the mass density by
\begin{align}
\eta {\ =\ }W^{-1} \rho,\end{align}
 and \begin{align}
W_i\eta x_i {\ =\ }\rho y_i.\end{align}
 It
follows that
\begin{align}
W_i \eta {\boldsymbol{\nabla}}x_i {\ =\ }\rho {\boldsymbol{\nabla}}y_i + \rho y_i {\boldsymbol{\nabla}}\ln W.\end{align}

The second term on the right-hand side is ignored.

Simple equations of state are provided for density [g/cm$^3$],
viscosity [Pa s], and diffusivity [m$^2$/s]. The density is a
function of both composition are pressrue with the form
\begin{align}
\rho(y_1,\,p) &{\ =\ }\rho(y_1,\,p_0) + \left.\frac{{{\partial}}\rho}{{{\partial}}p}\right|_{p=p_0} (p-p_0),\\
&{\ =\ }\rho(y_1,\,p_0) \big(1+\beta (p-p_0)\big),\end{align}
 with the
compressibility $\beta(y_1)$ given by
\begin{align}
\beta &{\ =\ }\left.\frac{1}{\rho}\frac{{{\partial}}\rho}{{{\partial}}p}\right|_{p=p_0},\\
&{\ =\ }4.49758\times 10^{-10} y_1 + 5\times 10^{-10}(1-y_1),\end{align}
 and the
mixture density at the reference pressure $p_0$ taken as atmospheric
pressure is given by
\begin{align}
\rho(y_1,\,p_0) {\ =\ }\Big(\big((0.0806 y_1 - 0.203) y_1 + 0.0873\big) y_1 + 1.0341\Big)10^3,\end{align}

with mass fraction of water $y_1$. The viscosity and diffusivity have
the forms
\begin{align}
\mu(y_1) {\ =\ }10^{\big(1.6743 (1-y_1) - 0.0758\big)} 10^{-3},\end{align}
 and
\begin{align}
D(y_1) {\ =\ }\Big(\big(((-4.021 y_1 + 9.1181) y_1 - 5.9703) y_1
     + 0.4043\big) y_1 + 0.5687\Big) 10^{-9},\end{align}
 The mass fraction is
related to mole fraction according to
\begin{align}
y_1 = \frac{x_1 W_{\rm H_2O}}{W},\end{align}
 where the mean formula weight $W$
is given by \begin{align}
W = x_1 W_{\rm H_2O} + x_2 W_{\rm PPG},\end{align}
 with formula
weights for water and proplyene glycol equal to $W_{\rm H_2O}$ =
18.01534 and $W_{\rm PPG}$ = 76.09 [kg/kmol].

Global mass conservation satisfies the relation
\begin{align}
\frac{d}{dt}M_i {\ =\ }-\int{\boldsymbol{F}}_i\cdot{\boldsymbol{dS}}+ \int Q_i dV,\end{align}

with \begin{align}
M_i {\ =\ }\int \varphi \eta x_i dV.\end{align}
 In terms of mass fractions
and mass density
\begin{align}
M_i^m {\ =\ }W_i M_i {\ =\ }\int \varphi \rho y_i dV.\end{align}


Mode: `Air-Water`
----------------------------

The `Air-Water` mode involves two phase liquid water-gas flow
coupled to the reactive transport mode. Mass conservation equations have
the form
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi \Big(s_l^{} \rho_l^{} x_i^l + s_g^{} \rho_g^{} x_i^g \Big) + {\boldsymbol{\nabla}}\cdot\Big({\boldsymbol{q}}_l^{} \rho_l^{} x_i^l + {\boldsymbol{q}}_g \rho_g^{} x_i^g -\varphi s_l^{} D_l^{} \rho_l^{} {\boldsymbol{\nabla}}x_i^l -\varphi s_g^{} D_g^{} \rho_g^{} {\boldsymbol{\nabla}}x_i^g \Big) {\ =\ }Q_i^{},\end{align}

for liquid and gas saturation $s_{l,\,g}^{}$, density $\rho_{l,\,g}^{}$,
diffusivity $D_{l,\,g}^{}$, Darcy velocity ${\boldsymbol{q}}_{l,\,g}^{}$
and mole fraction $x_i^{l,\,g}$. The energy conservation equation can be
written in the form
\begin{align}
\sum_{{{\alpha}}=l,\,g}\left\{\frac{{{\partial}}}{{{\partial}}t} \big(\varphi s_{{\alpha}}\rho_{{\alpha}}U_{{\alpha}}\big) + {\boldsymbol{\nabla}}\cdot\big({\boldsymbol{q}}_{{\alpha}}\rho_{{\alpha}}H_{{\alpha}}\big) \right\} + \frac{{{\partial}}}{{{\partial}}t} \Big((1-\varphi)\rho_r C_p T \big) - {\boldsymbol{\nabla}}\cdot (\kappa{\boldsymbol{\nabla}}T)\Big) {\ =\ }Q,\end{align}

as the sum of contributions from liquid and gas fluid phases and rock,
with internal energy $U_{{\alpha}}$ and enthalpy $H_{{\alpha}}$ of fluid
phase ${{\alpha}}$, rock heat capacity $C_p$ and thermal conductivity
$\kappa$. Note that
\begin{align}
U_{{\alpha}}{\ =\ }H_{{\alpha}}-\frac{P_{{\alpha}}}{\rho_{{\alpha}}}.\end{align}


Thermal conductivity $\kappa$ is determined from the equation (Somerton
et al., 1974) \begin{align}
\label{cond}
\kappa {\ =\ }\kappa_{\rm dry} + \sqrt{s_l^{}} (\kappa_{\rm sat} - \kappa_{\rm dry}),\end{align}

where $\kappa_{\rm dry}$ and $\kappa_{\rm sat}$ are dry and fully
saturated rock thermal conductivities.

Mode: `TH` (Thermal-Hydrologic)
------------------------------------------

The current implementation of the `TH` mode applies to mass
and energy conservation equations which are solved fully coupled. The
fluid density only a function of $T$ and $P$. Future generalizations of
the `TH` mode will include multicomponent variable density
fluids. The TH equations may be coupled to the reactive transport mode
(see Section [sec:chem]).

`TH` mode applies to single phase, variably saturated,
nonisothermal systems with incorporation of density variations coupled
to fluid flow. The governing equations for mass and energy are given by
\begin{align}
\label{masseqn}
\frac{{{\partial}}}{{{\partial}}t}\left(\varphi s\rho\right) + {\boldsymbol{\nabla}}\cdot\left(\rho{\boldsymbol{q}}\right) = Q_w,\end{align}

and
\begin{align}
\frac{{{\partial}}}{{{\partial}}t}\big(\varphi s\rho U + (1-\varphi) \rho_p c_p T\big) + {\boldsymbol{\nabla}}\cdot\big(\rho{\boldsymbol{q}}H -\kappa {\boldsymbol{\nabla}}T\big) = Q_e,\end{align}

The Darcy flow velocity ${\boldsymbol{q}}$ is given by
\begin{align}
{\boldsymbol{q}}= -\frac{kk_r}{\mu}{\boldsymbol{\nabla}}\left(P-W\rho g z\right).\end{align}

Here, $\varphi$ denotes porosity, $s$ saturation, $\rho$ mixture density
of the brine, ${\boldsymbol{q}}$ Darcy flux, $k$ intrinsic permeability,
$k_r$ relative permeability, $\mu$ viscosity, $P$ pressure, $g$ gravity,
and $z$ the vertical component of the position vector. Supported
relative permeability functions $k_r$ for Richards’ equation include van
Genuchten, Books-Corey and Thomeer-Corey, while the saturation functions
include Burdine and Mualem. Water density and viscosity are computed as
a function of temperature and pressure through an equation of state for
water. The quantities $\rho_p$, $c_p$, and $\kappa$ denotes the density,
heat capacity, and thermal conductivity of the porous medium-fluid
system. The internal energy and enthalpy of the fluid, $U$ and $H$, are
obtained from an equation of state for pure water. These two quantities
are related by the thermodynamic expression
\begin{align}
U {\ =\ }H -\frac{P}{\rho}.\end{align}


Thermal conductivity is determined from the equation (Somerton et al.,
1974) \begin{align}
\label{cond1}
\kappa {\ =\ }\kappa_{\rm dry} + \sqrt{s_l^{}} (\kappa_{\rm sat} - \kappa_{\rm dry}),\end{align}

where $\kappa_{\rm dry}$ and $\kappa_{\rm sat}$ are dry and fully
saturated rock thermal conductivities.

### Ice Model

In PFLOTRAN, the formulation used to model ice and water vapor involves
solving a modified Richards equation coupled with an energy balance
equation. This formulation is different from Painter (2011), where a
multiphase approach was used and mass balance for air was also solved
for. In this formulation, the movement of air is not tracked, and hence
the mass balance for air is not considered. The balance equations for
mass and energy involving three phases (liquid, gas, ice) for the water
component are given by
\begin{align}
{\dfrac{\partial{}}{\partial{t}}} \left[ \phi \left( s_l \eta_l X_w^l + s_g \eta_g X_w^g + s_i \eta_i X_w^i \right) \right] & + \boldsymbol{\nabla} \cdot \left[X_w^l \boldsymbol{v}_l \eta_l + X_w^g \eta_g \boldsymbol{v}_g \right] \nonumber\\
&- \boldsymbol{\nabla} \cdot \left[\phi s_g \tau_g  \eta_g D_g \boldsymbol{\nabla} X^g_w \right] = Q_w, \\
{\dfrac{\partial{}}{\partial{t}}} \left[ \phi \left( s_l \eta_l U_l + s_g \eta_g U_g + s_i \eta_i U_i \right) + (1- \phi) \rho_r c_r T \right] & + \boldsymbol{\nabla} \cdot \left[ \boldsymbol{v}_l \eta_l  H_l + \boldsymbol{v}_g \eta_gH_g \right] \nonumber\\
&- \boldsymbol{\nabla} \cdot \left[ \kappa \boldsymbol{\nabla} T\right] = Q_e,\end{align}

[eq:balance\_eqns] where the subscripts $l$, $i$, $g$ denote the
liquid, ice and gas phases, respectively; $\phi$ is the porosity;
$s_{\alpha} (\alpha = i, l, g)$ is the saturation of the $\alpha$-th
phase; $\eta_{\alpha} (\alpha = i, l, g)$ is the molar density of the
$\alpha$-th phase; $\rho_g$, $\rho_l$ are the mass densities of the gas
and liquid phases; $Q_w$ is the mass source of $\mathrm{H_2O}$;
$X_w^{\alpha} (\alpha = i, l, g)$ is the mole fraction of $\mathrm{H_2O}$ in the
$\alpha$-th phase; $\tau_g$ is the tortuosity of the gas phase; $D_g$ is
the diffusion coefficient in the gas phase; $T$ is the temperature
(assuming all the phases and the rock are in thermal equilibrium); $c_r$
is the specific heat of the rock; $\rho_r$ is the density of the rock;
$U_{\alpha} (\alpha = i, l, g)$ is the molar internal energy of the
$\alpha$-th phase; $H_{\alpha} (\alpha = l, g)$ is the molar enthalpy of
the $\alpha$-the phase; $Q_e$ is the heat source;
$\boldsymbol{\nabla}\, (\, )$ is the gradient operator;
$\boldsymbol{\nabla}\cdot (\,)$ is the divergence operator.

The Darcy velocity for the gas and liquid phases are given as follows:
\begin{align}
& \boldsymbol{v}_g = - \frac{k_{rg}k}{\mu_g} \boldsymbol{\nabla}\left[p_g - \rho_g g  z \right], \\
& \boldsymbol{v}_l = - \frac{k_{rl}k}{\mu_l} \boldsymbol{\nabla}\left[p_l - \rho_l g z \right],\end{align}

[eq:darcy] where $k$ is the absolute permeability;
$k_{r \alpha} (\alpha =  l, g)$ is the relative permeability of the
$\alpha$-th phase; $\mu_{\alpha} (\alpha =  l, g)$ is the viscosity of
the $\alpha$-th phase; $p_{\alpha} (\alpha =  l, g)$ is the partial
pressure of the $\alpha$-th phase; $g$ is acceleration due to gravity,
and $z$ is the vertical distance from a reference datum.

The constraint on the saturations of the various phases of water is
given by \begin{align}
& s_l + s_g + s_i = 1.\end{align}
 Furthermore, neglecting the amount
of air in liquid and ice phases, it follows that
\begin{align}
X_a^l = 0, X_a^i = 0 \Rightarrow X_w^l = 1, X_w^i =1,\end{align}
 and so
Eqns.\ref{eq:balance_eqns},\ref{eq:darcy} based on the assumption that $p_g$ is hydrostatic i.e.,
${p}_g = {({p}_g)}_0 + \rho_g gz$, reduce to [eq:gov]
\begin{align}
&{\dfrac{\partial{}}{\partial{t}}}\left[ \phi \left( s_g \eta_g X_w^g +s_l \eta_l + s_i \eta_i \right) \right] + \boldsymbol{\nabla} \cdot \left[\boldsymbol{v}_l \eta_l \right] - \boldsymbol{\nabla} \cdot \left[\phi s_g \tau_g  \eta_g D_g \boldsymbol{\nabla} X^g_w \right] = Q_w, \label{eq:gov1}\\
& {\dfrac{\partial{}}{\partial{t}}}\left[ \phi \left( s_l \eta_l U_l + s_g \eta_g U_g + s_i \eta_i U_i \right) + (1- \phi) \rho_r c_r T \right] + \boldsymbol{\nabla} \cdot \left[ \boldsymbol{v}_l \eta_l  H_l \right] - \boldsymbol{\nabla} \cdot \left[ \kappa \boldsymbol{\nabla} T\right] = Q_e, \\
& \boldsymbol{v}_l = - \frac{k_{rl}k}{\mu_l} \boldsymbol{\nabla}\left[p_l - \rho_l g z \right]. \label{eq:gov2}\end{align}

In the above formulation, temperature and liquid pressure are chosen to
be primary variables. It is ensured that complete dry-out does not
occur, and that liquid is present at all times. With this approach, it
is not necessary to change the primary variables based on the phases
present.

In addition to the previously described mass and energy balance
equations, additional constitutive relations are required to model
non-isothermal, multiphase flow of water. Assuming thermal equilibrium
among the ice, liquid and vapor phases, the mole fraction of water in
vapor phase is given by the relation, \begin{align}
& X_w^g = \frac{p_v}{p_g},\end{align}

where $p_v$ is the vapor pressure, and $p_g$ is the gas pressure (It is
assumed that $p_g$ = 1 atm throughout the domain). Vapor pressure is
calculated using Kelvin’s relation which includes vapor pressure
lowering due to capillary effects as follows
\begin{align}
p_v = P_{\text{sat}}(T) \text{exp}\left[\frac{P_{cgl}}{\eta_l R (T + 273.15)} \right],\end{align}

where $P_{\text{sat}}$ is the saturated vapor pressure, $P_{cgl}$ is the
liquid-gas capillary pressure, and $R$ is the gas constant. Empirical
relations for saturated vapor pressure are used for both above and below
freezing conditions. To calculate the partition of ice, liquid and vapor
phases, at a known temperature and liquid pressure, the following two
relations are used (see Painter, 2011): [sats]
\begin{align}
\frac{s_l}{s_l + s_g} &= S_{*}\left(P_{cgl}\right), \label{sats1}\\
\frac{s_l}{s_l + s_i} &= S_{*}\left[\frac{\sigma_{gl}}{\sigma_{il}} P_{cil} \right], \label{sats2}\end{align}

$S_{*}$ is the retention curve for unfrozen liquid-gas phases, $P_{cgl}$
is the gas-liquid capillary pressure, $P_{cil}$ is the ice-liquid
capillary pressure, $\sigma_{il}$ and $\sigma_{gl}$ are the ice-liquid
and gas-liquid interfacial tensions. Also,
\begin{align}P_{cil} = - {\rho}_i h_{iw} \vartheta,\end{align}
where $h_{iw}^0$ is the heat of
fusion of ice at 273.15 K, ${\rho}_i$ is the mass density of ice,
$\vartheta = \frac{T - T_0}{T_0}$ with $T_0 = 273.15$ K.

For $S_{*}$ the van Genuchten model is used: \begin{align}
S_{*} = \begin{cases}
           \left[ 1 + \left(\alpha {P_c}\right)^\gamma\right]^{-\lambda} , &\quad P_c > 0\\
           1, &\quad P_c \leq 0
            \end{cases}\end{align}
 with the Mualem model implemented for the
relative permeability of liquid water,
\begin{align}
k_{rl} = (s_l)^{\frac{1}{2}} \left[1 - \left( 1 - (s_l)^{\frac{1}{\lambda}}\right)^{\lambda} \right]^2,\end{align}

where $\lambda$, $\alpha$ are parameters, with
$\gamma =  \frac{1}{1-\lambda}$.

The thermal conductivity for the frozen soil is chosen to be
\begin{align}
\kappa = Ke_{f} \kappa_{\text{wet},f} + Ke_{u} \kappa_{\text{wet},u} + (1 - Ke_u - Ke_f) \kappa_{\text{dry}},\end{align}

where $\kappa_{\text{wet},f}$, $\kappa_{\text{wet},u}$ are the liquid-
and ice-saturated thermal conductivities, $\kappa_{\text{dry}}$ is the
dry thermal conducitivity, $Ke_f$, $Ke_u$ are the Kersten numbers in
frozen and unfrozen conditions and are assumed to be related to the ice
and liquid saturations by power law relations as follows
\begin{align}
Ke_f = \left(s_i  \right)^{\alpha_f}, \\
Ke_u = \left(s_l  \right)^{\alpha_u},\end{align}
 with $\alpha_f$, $\alpha_u$
being the power law coefficients. Care is also taken to ensure that the
derivatives of the Kersten numbers do not blow up when $s_i$, $s_l$ go
to zero when $\alpha_f$, $\alpha_u$ are less than one.

The gas diffusion coefficient $D_g$ is assumed to dependend on
temperature and pressure as follows:
\begin{align}
D_g = D_g^0 \left( \frac{P_{\text{ref}}}{P}\right) \left( \frac{T}{T_{\text{ref}}}\right)^{1.8},\end{align}

where $D_g^0$ is the reference diffusion coefficient at some reference
temperature, $T_{\text{ref}}$, and pressure $P_{\text{ref}}$.

Thermal Conduction Multiple Continuum Model
-------------------------------------------

A thermal conduction model employing a multiple continuum model has been
added to modes `MPHASE` and `TH`. The
formulation is based on Pruess and Narasimhan (1985) using a integrated
finite volume approach to develop equations for fracture (primary
continuum) and matrix (secondary continua) temperatures $T_{{\alpha}}$
and $T_{{\beta}}$, respectively, with fracture volume fraction denoted
by $\epsilon_{{\alpha}}$. The DCDM (dual continuum discrete matrix)
model following the classification given in Lichtner (2000) is
implemented. In what follows the matrix porosity is assumed to be zero.

In terms of partial differential equations the heat conservation
equations may be written as

\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \epsilon_{{\alpha}}\Big[\varphi_{{\alpha}}\rho_{{\alpha}}U_{{\alpha}}+ (1-\varphi_{{\alpha}}) \rho_r C_r T_{{\alpha}}\Big] &+ {\boldsymbol{\nabla}}\cdot \Big({\boldsymbol{q}}_{{\alpha}}\rho_{{\alpha}}H_{{\alpha}}-
\kappa_a{\boldsymbol{\nabla}}T_{{\alpha}}\Big) \nonumber\\
&{\ =\ }-A_{{{\alpha}}{{\beta}}} \kappa_{{{\alpha}}{{\beta}}}\frac{{{\partial}}T_{{\beta}}}{{{\partial}}n},\end{align}


and
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \rho_r C_r T_{{\beta}}+ \frac{{{\partial}}}{{{\partial}}\xi} \Big(-\kappa_{{\beta}}\frac{{{\partial}}T_{{\beta}}}{{{\partial}}\xi}\Big) {\ =\ }0,\end{align}

for fracture and matrix temperatures $T_{{\alpha}}$ and $T_{{\beta}}$,
respectively, where $\xi$ represents the matrix coordinate assumed to be
an effective 1D domain. The boundary condition
\begin{align}
T_{{\beta}}(\xi_M,\,t\,|\,{\boldsymbol{r}}) {\ =\ }T_{{\alpha}}({\boldsymbol{r}},\,t),\end{align}


is imposed at the fracture-matrix interface, where $\xi_M$ denotes the
outer boundary of the matrix.

Using the control volume configuration shown in [Figure 10](#fig:fminc), with
fracture nodes designated by the subscript $n$ and matrix nodes by $m$,
the integrated finite volume form of the heat transport equation for the
$n$th fracture control volume is given by

\begin{align}
\label{pricont}
&\Big[\varphi_{{\alpha}}\Big(\big(\rho_{{\alpha}}U_{{\alpha}}\big)_n^{k+1} - \big(\rho_{{\alpha}}U_{{\alpha}}\big)_n^{k}\Big) + (1-\varphi_{{\alpha}})\Big(\big(\rho_r C_r T_{{\alpha}}\big)_n^{k+1} - \big(\rho_r C_r T_{{\alpha}}\big)_n^{k}\Big)\Big] \frac{V_n^{{\alpha}}}{\Delta t} \nonumber\\
&\qquad + \sum_{n'} \Big[\big(q_{{\alpha}}\rho_{{\alpha}}H_{{\alpha}}\big)_{nn'} + \frac{\kappa_{nn'}^{{\alpha}}}{d_n+d_{n'}}\big(T_{{{\alpha}}n} - T_{{{\alpha}}n'}\big) \Big] A_{nn'}^{{\alpha}}\nonumber\\
&\qquad + \sum_{l = 1}^{N_{{\beta}}}\frac{\kappa_{nM}^{{{\alpha}}{{\beta}}_l}}{d_n+d_{M}}\big(T_{{{\alpha}}n}-T_{{{\beta}}_l M}\big) A_{nM}^{{{\beta}}_l} {\ =\ }0,\end{align}


where $V_n^{{\alpha}}$ denotes the fracture volume, and
\begin{align}
\Big(\big(\rho_r C_r T_{{\beta}}\big)_m^{k+1} - \big(\rho_r C_r T_{{\beta}}\big)_m^{k}\Big) \frac{V_m^{{\beta}}}{\Delta t} &+ \sum_{m'} \frac{\kappa_{mm'}^{{\beta}}}{d_m+d_{m'}}\big(T_{{{\beta}}m} - T_{{{\beta}}m'}\big) A_{mm'}^{{\beta}}\nonumber\\
&+ \delta_{mM}^{}\frac{\kappa_{nM}^{{{\alpha}}{{\beta}}}}{d_n+d_{M}}\big(T_{{{\alpha}}n} - T_{{{\beta}}M}\big) A_{nM}^{{\beta}}{\ =\ }0,\end{align}


for the $m$th matrix node with volume $V_m^{{\beta}}$. The matrix node
designated by $M$ refers to the outer most node in contact with the
fracture (see Figure [fnodestruct]). More than one type of matrix
geometry is included in the above equations as indicated by the sum over
$l$ in Eqn. \ref{pricont}, where $N_{{\beta}}$ denotes the number of different
secondary continua. However, it should be noted that the current
implementation in PFLOTRAN only allows coupling to a single secondary
continuum $(N_{{\beta}}=1)$. The fracture volume $V_n^{{\alpha}}$ is
related to the REV volume $V_n$ by
\begin{align}
\epsilon_{{\alpha}}{\ =\ }\frac{V_n^{{\alpha}}}{V_n}.\end{align}
 Thermal
conductivity at the interface between two control volumes is calculated
using the harmonic average
\begin{align}
\kappa_{ll'} {\ =\ }\frac{\kappa_l \kappa_{l'}(d_l+d_{l'})}{d_l \kappa_{l'}+d_{l'}\kappa_l}.\end{align}


\begin{align}
\qquad
\bigg|\quad\mathop{\bullet}_{\ \ \ \, \displaystyle 1 \ {{\beta}}}\quad\bigg| \qquad \cdots \qquad
\bigg|\quad\mathop{\bullet}_{\ \ \ \, \displaystyle l \ {{\beta}}}\quad\bigg| \qquad \cdots \qquad
\bigg|\quad\mathop{\bullet}_{\ \ \ \, \displaystyle M \ {{\beta}}}\quad
\bigg|\quad\mathop{\bullet}_{\ \ \ \, \displaystyle n \ {{\alpha}}}\quad\bigg|\nonumber\end{align}


For better convergence uniform logarithmic spacing is used for the
matrix nodes

[eqnloggrid] \begin{align}
\Delta \xi_m {\ =\ }\rho \,\Delta \xi_{m-1},\end{align}

specifying $\Delta\xi_M$ and $l_M$ for the outer most matrix node and
matrix block size, respectively. The factor $\rho$ is determined from
the constraint \begin{align}
l_M {\ =\ }2\sum_{m=1}^{M} \Delta \xi_m,\end{align}
 which
requires that $\rho$ satisfy the equation
\begin{align}
\frac{l_M}{2\Delta \xi_1} {\ =\ }\frac{\rho^M-1}{\rho-1},\end{align}
 with the
inner and outer grid spacing related by
\begin{align}
\Delta\xi_M {\ =\ }\rho^{M-1} \Delta \xi_1.\end{align}


![Control volumes in DCDM multiple continuum
model with fracture aperture $2δ$ and matrix block size $d$.](./figs/mincl.png){#fig:fminc}
*Figure 12. Control volumes in DCDM multiple continuum
model with fracture aperture $2δ$ and matrix block size $d$.*

According to the geometry in [Figure 10](#fig:fminc) assuming a 3D orthogonal
set of fractures, \begin{align}
V_n {\ =\ }(d+2\delta)^3,\end{align}
 and
\begin{align}
V_n^{{\alpha}}{\ =\ }(d+2\delta)^3 - d^3,\end{align}
 giving

\begin{align}
\epsilon_{{\alpha}}&{\ =\ }1-\frac{d^3}{(d+2\delta)^3} {\ =\ }1-\left(\dfrac{1}{1+\dfrac{2\delta}{d}}\right)^3,\\
& ~\simeq~ \frac{6\delta}{d}.\end{align}


The fracture aperture $2\delta$ is found to be in terms of
$\epsilon_{{\alpha}}$ and $d$
\begin{align}
2\delta {\ =\ }d \left(\frac{1}{(1-\epsilon_{{\alpha}})^{1/3}} -1\right).\end{align}

A list of different sub-continua geometries and parameters implemented
in PFLOTRAN is given in Table [tdcdmgeom]. Different independent and
dependent parameters for the nested cube geometry are listed in
Table [tnestedcube]. The interfacial area $A_{nn'}^{{\alpha}}$ between
fracture control volumes is equal to $\Delta y \Delta z$,
$\Delta z \Delta x$, $\Delta x \Delta y$ for $x$, $y$, and $z$
directions, respectively.

In the case of nested cubes there are four possible parameters
$(\epsilon_{{\alpha}}, \, 2\delta, \, l_m,\, l_f)$, where $l_m$ denotes
the matrix block size and $l_f$ refers to the fracture spacing, two of
which are independent.

The fracture-matrix interfacial area $A_{nM}$ per unit volume is equal
to
\begin{align}
A_{nM}^{{\beta}}{\ =\ }\frac{{{\mathcal N}}_{{\beta}}}{V} A_{{\beta}}^0,\end{align}

where the number density ${{\mathcal N}}_{{\beta}}/V$ of secondary
continua of type ${{\beta}}$ is equal to
\begin{align}
\frac{{{\mathcal N}}_{{\beta}}}{V} {\ =\ }\frac{1}{V} \frac{V_{{\beta}}}{V_{{\beta}}^0} {\ =\ }\frac{\epsilon_{{\beta}}}{V_{{\beta}}^0},\end{align}

and $A_{{\beta}}^0$ and $V_{{\beta}}^0$ refer to the area and volume of
each geometric type as listed in Table [tdcdmgeom].

| Geometry         | Area $A_{{\beta}}^0$   | Volume $V_{{\beta}}^0$       |
| ---------------- | ---------------------- | ---------------------------- |
| Slab             |         $A$            |        $A l$                 |
| Nested Cubes     |        $6d^2$          |        $d^3$                 |
| Nested Spheres   |     $4 \pi R^2$        |  $4/3 \pi R^3$               |

  : DCDM geometric parameters.<span data-label="tdcdmgeom"></span>

The primary-secondary coupling term can then be written in the form
\begin{align}
\sum_{{\beta}}\frac{\kappa_{nM}^{{{\alpha}}{{\beta}}}}{d_n+d_{M}}\big(T_n^{{\alpha}}-T_{M}^{{\beta}}\big) A_{nM}^{{\beta}}{\ =\ }V_n
\sum_{{\beta}}\frac{\epsilon_{{\beta}}\kappa_{nM}^{{{\alpha}}{{\beta}}}}{d_n+d_{M}}\big(T_n^{{\alpha}}-T_{M}^{{\beta}}\big) \frac{A_{{\beta}}^0}{V_{{\beta}}^0}.\end{align}

|  Independent            |                         |   Dependent                                                                 |                                                |
| ----------------------- | ----------------------- | --------------------------------------------------------------------------- | ---------------------------------------------- |
| $\epsilon_{{\alpha}}$   |         $l_f$           |                          $2\delta = l_f - l_m$                              | $l_m = l_f(1-\epsilon_{{\alpha}})^{1/3}$       |
| $\epsilon_{{\alpha}}$   |         $l_m$           |                          $2\delta = l_f - l_m$                              | $l_f = l_m(1-\epsilon_{{\alpha}})^{-1/3}$      |
| $2\delta$               |         $l_f$           |                  $\epsilon_{{\alpha}}= 1-(l_m/l_f)^3$                       |          $l_m = l_f - 2\delta$                 |
| $2\delta$               |         $l_m$           |                   $\epsilon_{{\alpha}}= 1-(l_m/_f)^3$                       |          $l_f = l_m + 2\delta$                 |
| $2\delta$               | ${\epsilon}_{\alpha}$   | $l_m = 2\delta \Big(\dfrac{1}{(1-\epsilon_{{\alpha}})^{1/3}}-1\Big)^{-1}$   |             $l_m = l-2\delta$                  |

  : Independent and dependent nested cube parameters.<span
  data-label="tnestedcube"></span>


Mode: Reactive Transport (Keyword `CHEMISTRY`) {#sec:chem}
---------------------------------------------------------

The governing mass conservation equations for the geochemical transport
mode for a multiphase system written in terms of a set of independent
aqueous primary or basis species with the form \begin{align}
\label{rteqn}
\frac{{{\partial}}}{{{\partial}}t}\big(\varphi \sum_{{\alpha}}s_{{\alpha}}\Psi_j^{{\alpha}}\big) +
\nabla\cdot\sum_{{\alpha}}{\boldsymbol{\Omega}}_j^{{\alpha}}{\ =\ }Q_j - \sum_m\nu_{jm} I_m -\frac{{{\partial}}S_j}{{{\partial}}t},\end{align}

and
\begin{align}
\frac{{{\partial}}\varphi_m}{{{\partial}}t} {\ =\ }\overline{V}_m I_m,\end{align}

for minerals with molar volume $\overline{V}_m$, mineral reaction rate
$I_m$ and mineral volume fraction $\varphi_m$ referenced to an REV. Sums
over ${{\alpha}}$ in Eqn.\ref{rteqn} are over all fluid phases in the system. The
quantity $\Psi_j^{{\alpha}}$ denotes the total concentration of the
$j$th primary species ${{\mathcal A}}_j^{\rm pri}$ in the ${{\alpha}}$th
fluid phase defined by
\begin{align}
\Psi_j^{{\alpha}}= \delta_{l{{\alpha}}}^{} C_j^l + \sum_{i=1}^{N_{\rm sec}}\nu_{ji}^{{{\alpha}}} C_i^{{\alpha}},\end{align}

In this equation the subscript $l$ represents the aqueous electrolyte
phase from which the primary species are chosen. The secondary species
concentrations $C_i^{{\alpha}}$ are obtained from mass action equations
corresponding to equilibrium conditions of the reactions
\begin{align}
\sum_j\nu_{ji}^{{\alpha}}{{\mathcal A}}_j^l {~\rightleftharpoons~}{{\mathcal A}}_i^{{\alpha}},\end{align}

yielding the mass action equations
\begin{align}
C_i^{{\alpha}}{\ =\ }\frac{K_i^{{\alpha}}}{\gamma_i^{{\alpha}}} \prod_j \Big(\gamma_j^l C_j^l\Big)^{\nu_{ji}^{{\alpha}}},\end{align}

with equilibrium constant $K_i^{{\alpha}}$, and activity coefficients
$\gamma_k^{{\alpha}}$. For the molality of the $k$th aqueous species,
the Debye-Hückel activity coefficient algorithm is given by
\begin{align}
\log\,\gamma_k {\ =\ }-\frac{z_k^2 A \sqrt{I}}{1+B \stackrel{\circ}{a}_k \sqrt{I}}+\dot b I,\end{align}

and the Davies algorithm by the expression
\begin{align}
\log\,\gamma_k {\ =\ }-\frac{z_k^2}{2}\left[\frac{\sqrt{I}}{1+ \sqrt{I}}-0.3 I\right].\end{align}

with valence $z_k$, Debye-Hückel parameters $A$, $B$, and ionic radius
$\stackrel{\circ}{a}_k$, and ionic strength $I$ defined as
\begin{align}
I {\ =\ }\frac{1}{2}\sum_{j=1}^{N_c} m_j^2 + \frac{1}{2}\sum_{i=1}^{N_{\rm sec}} m_i^2,\end{align}

for molality $m_j$ and $m_i$ of primary and secondary species,
respectively (note: $C_i^l = \rho_l y_w^l m_i \simeq \rho_l m_i$,
$\rho_l$ = fluid density, $y_w^l$ = mass fraction of $\mathrm{H_2O}$). For
high-ionic strength solutions (approximately above 0.1 M) the Pitzer
model should be used. Currently, however, only the Debye-Hückel
algorithm is implemented in PFLOTRAN.



The total flux ${\boldsymbol{\Omega}}_j^{{\alpha}}$ for
species-independent diffusion is given by
\begin{align}
{\boldsymbol{\Omega}}_j^{{\alpha}}{\ =\ }\big({\boldsymbol{q}}_{{\alpha}}- \varphi s_{{\alpha}}{\boldsymbol{D}}_{{\alpha}}{\boldsymbol{\nabla}}\big)\Psi_j^{{\alpha}}.\end{align}

The diffusion/dispersion coefficient ${\boldsymbol{D}}_{\alpha}$ may
be different for different phases, e.g. an aqueous electrolyte solution
or gas phase, but is assumed to be species independent. Dispersivity
currently must be described through a diagonal dispersion tensor.

The
Darcy velocity ${\boldsymbol{q}}_{{\alpha}}$ for phase ${{\alpha}}$ is
given by
\begin{align}
{\boldsymbol{q}}_a {\ =\ }-\frac{kk_{{\alpha}}}{\mu_{{\alpha}}} {\boldsymbol{\nabla}}\big(p_{{\alpha}}-\rho_{{\alpha}}g z\big),\end{align}

with bulk permeability of the porous medium $k$ and relative
permeability $k_{{\alpha}}$, fluid viscosity $\mu_{{\alpha}}$, pressure
$p_{{\alpha}}$, density $\rho_{{\alpha}}$, and acceleration of gravity
$g$. The diffusivity/dispersivity tensor ${\boldsymbol{D}}_{{\alpha}}$
is the sum of contributions from molecular diffusion and dispersion
which for an isotropic medium has the form
\begin{align}
{\boldsymbol{D}}_{{\alpha}}{\ =\ }\tau D_m {\boldsymbol{I}}+ a_T v{\boldsymbol{I}}+ \big(a_L-a_T\big)\frac{{\boldsymbol{v}}{\boldsymbol{v}}}{v},
\end{align}
 with longitudinal and transverse dispersivity coefficients $a_L$,
$a_T$, respectively, $\tau$ refers to tortuosity, and $D_m$ to the
molecular diffusion coefficient. Currently, only longitudinal dispersion
is implemented in PFLOTRAN.


The porosity may be calculated from the mineral volume fractions
according to the relation \begin{align}
\varphi {\ =\ }1 - \sum_m \varphi_m.\end{align}


The temperature dependence of the diffusion coefficient is defined
through the relation
\begin{align}
D_m(T) {\ =\ }D_m^\circ\exp\left[\frac{A_D}{R}\left(\frac{1}{T_0}-\frac{1}{T}\right)\right],\end{align}

with diffusion activation energy $A_D$ in kJ/mol. The quantity
$D_m^\circ$ denotes the diffusion coefficient at the reference
temperature $T_0$ taken as 25$^\circ$C and the quantity $R$
denotes the gas constant ($8.317\times 10^{-3}$ kJ/mol/K). The
temperature $T$ is in Kelvin.

The quantity $Q_j$ denotes a source/sink term
\begin{align}
Q_j {\ =\ }\sum_n\frac{q_M}{\rho}\Psi_j \delta({\boldsymbol{r}}-{\boldsymbol{r}}_{n}),\end{align}

where $q_M$ denotes a mass rate in units of kg/s, $\rho$ denotes the
fluid density in kg/m$^3$, and ${\boldsymbol{r}}_{n}$ refers to the
location of the $n$th source/sink. The quantity $S_j$ represents the
sorbed concentration of the $j$th primary species considered in more
detail in the next section.

Molality $m_i$ and molarity $C_i$ are related by the density of water
$\rho_w$ according to \begin{align}
C_i {\ =\ }\rho_w m_i.\end{align}
 The activity of water
is calculated from the approximate relation
\begin{align}
a_{\rm H_2O}^{} {\ =\ }1 - 0.017 \sum_i m_i.\end{align}


### Mineral Precipitation and Dissolution

The reaction rate $I_m$ is based on transition state theory taken as
positive for precipitation and negative for dissolution, with the form
\begin{align}
\label{Im}
I_m {\ =\ }-a_m\left(\sum_l k_{ml}(T) {{{\mathcal P}}}_{ml}\right) \Big|1-\big(K_m Q_m\big)^{1/\sigma_m}\Big|^{\beta_m} {\rm sign}(1-K_mQ_m),\end{align}

where the sum over $l$ represents contributions from parallel reaction
mechanisms such as pH dependence etc., and where $K_m$ denotes the
equilibrium constant, $\sigma_m$ refers to Temkin’s constant which is
defined as the average stoichiometric coefficient of the overall
reaction (Lichtner, 1996b; see also Section [thermo:database]),
$\beta_m$ denotes the affinity power, $a_m$ refers to the specific
mineral surface area, and the ion activity product $Q_m$ is defined as
\begin{align}
Q_m {\ =\ }\prod_j \big(\gamma_j m_j\big)^{\nu_{jm}},\end{align}
 with molality
$m_j$ of the $j$th primary species. The rate constant $k_{ml}$ is a
function of temperature given by the Arrhenius relation
\begin{align}
k_{ml} (T) {\ =\ }k_{ml}^0 \exp\left[\frac{E_{ml}}{R}\Big(\frac{1}{T_0}-\frac{1}{T}\Big)\right],\end{align}

where $k_{ml}^0$ refers to the rate constant at the reference
temperature $T_0$ taken as 25$^\circ$C, with $T$ in units
of Kelvin, $E_{ml}$ denotes the activation energy (kJ/mol), and the
quantity ${{{\mathcal P}}}_{ml}$ denotes the prefactor for the $l$th
parallel reaction with the form \begin{align}
\label{prefactor}
{{{\mathcal P}}}_{ml} {\ =\ }\prod_i\dfrac{\big(\gamma_i m_i\big)^{{{\alpha}}_{il}^m}}{1+K_{ml}\big(\gamma_i m_i\big)^{{{\beta}}_{il}^m} },\end{align}

where the product index $i$ generally runs over both primary and
secondary species, the quantities $\alpha_{il}^m$ and
$\beta_{il}^m$ refer to prefactor coefficients, and $K_{ml}$ is an
attenuation factor. The quantity $R$ denotes the gas constant
($8.317 \times 10^{-3}$ kJ/mol/K).


#### Rate Limiter

A rate-limited form of the mineral kinetic rate law can be devised
according to the expression \begin{align}
\label{ratemintran}
\widehat I_m {\ =\ }-a_m^{} \left(\sum_l {{{\mathcal P}}}_{ml}^{} k_{ml}^{}\right) \left[ \dfrac{1-\left(K_m Q_m\right)^{1/\sigma_m}}{1+\dfrac{k_{ml}^{}}{k_{ml}^{\rm lim}} \left(K_m Q_m\right)^{1/\sigma_m}} \right],\end{align}

with rate-limiter $r_{\rm lim}$. In the limit $K_mQ_m\rightarrow\infty$,
the rate becomes
\begin{align}
\lim_{K_m Q_m\rightarrow\infty}\widehat I_m {\ =\ }k_{ml}^{\rm lim} a_m^{}\sum_l {{{\mathcal P}}}_{ml}^{}.\end{align}

Defining the affinity factor
\begin{align}
\Omega_m {\ =\ }1-\left(K_m Q_m\right)^{1/\sigma_m},\end{align}
 or
\begin{align}
K_mQ_m {\ =\ }\Big(1-\Omega_m\Big)^{\sigma_m},\end{align}
 the rate may be
expressed alternatively as
\begin{align}
\widehat I_m {\ =\ }-a_m^{} \sum_l {{{\mathcal P}}}_{ml}^{} k_{ml}^{}
\dfrac{\Omega_m}{1+\dfrac{k_{ml}^{}}{k_{ml}^{\rm lim}} \big(1-\Omega_m\big)}.\end{align}

#### Changes in Material Properties {#sec_mat_prop}

Porosity, permeability, tortuosity and mineral surface area may be
updated optionally due to mineral precipitation and dissolution
reactions according to the relations \begin{align}
\label{porosity}
\varphi {\ =\ }1-\sum_m\varphi_m,\end{align}
 \begin{align}
\label{permeability}
k {\ =\ }k_0 f(\varphi,\,\varphi_0,\,\varphi_c,\,a),\end{align}
 with
\begin{align}
f &{\ =\ }\left(\frac{\varphi-\varphi_c}{\varphi_0-\varphi_c}\right)^a,\label{permf}\\
&{\ =\ }f_{\rm min} \ \ \ \text{if} \ \ \ \varphi \leq \varphi_c,\label{fmin}\end{align}

\begin{align}
\label{tortuosity}
\tau {\ =\ }\tau_0 \left(\frac{\varphi}{\varphi_0}\right)^b,\end{align}
 and
\begin{align}
\label{surface_area_vf}
a_m {\ =\ }a_m^0 \left(\frac{\varphi_m}{\varphi_m^0}\right)^n  \left(\frac{1-\varphi}{1-\varphi_0}\right)^{n'},\end{align}

where the super/subscript 0 denotes initial values, with a typical value
for $n$ of $2/3$ reflecting the surface to volume ratio. Note that this
relation only applies to primary minerals $(\varphi_m^0\ne 0)$. The
quantity $\varphi_c$ refers to a critical porosity below which the
permeability is assumed to be constant with scale factor $f_{\rm min}$.

In PFLOTRAN the solid is represented as an aggregate of minerals
described quantitatively by specifying its porosity $\varphi$ and the
volume fraction $\varphi_m$ of each primary mineral. It is not necessary
that Eqn.\ref{porosity}  relating porosity and mineral volume fractions holds.
Typically, however, the solid composition is specified by giving the
mass fraction $y_m$ of each of the primary minerals making up the solid
phase. The volume fraction is related to mole $x_m$ and mass $y_m$
fractions by the expressions

\begin{align}
\varphi_m &{\ =\ }(1-\varphi) \frac{x_m \overline V_m}{\sum_{m'} x_{m'} \overline V_{m'}},\\
&{\ =\ }(1-\varphi) \frac{y_m^{} \rho_m^{-1}}{\sum_{m'} y_{m'}^{} \rho_{m'}^{-1}},\end{align}


with inverse relation
\begin{align}
x_m {\ =\ }\frac{\varphi_m}{\overline V_m \eta_s(1-\varphi)},\end{align}
 and
similarly for the mass fraction, where
\begin{align}
\rho_m^{} {\ =\ }W_m^{} \overline V_m^{-1},\end{align}
 and the solid molar
density $\eta_s$ is given by
\begin{align}
\eta_s {\ =\ }\frac{1}{\sum_m x_m \overline V_m}.\end{align}
 In these relations
$W_m$ refers to the formula weight and $\overline{V_m}$ the molar volume
of the $m$th mineral. The solid molar density is related to the mass
density $\rho_s$ by \begin{align}
\rho_s {\ =\ }W_s \eta_s,\end{align}
 with the mean
molecular weight $W_s$ of the solid phase equal to
\begin{align}
W_s {\ =\ }\sum_m x_m W_m {\ =\ }\frac{1}{\sum_m W_m^{-1} y_m^{}}.\end{align}

Mass and mole fractions are related by the expression
\begin{align}
W_m x_m {\ =\ }W_s y_m.\end{align}


#### Affinity Threshold

An affinity threshold $f$ for precipitation may be introduced which only
allows precipitation to occur if $K_m Q_m > f > 1$.

#### Surface Armoring

Surface armoring occurs when one mineral precipitates on top of another
mineral, blocking that mineral from reacting. Thus suppose mineral
${{\mathcal M}}_m$ is being replaced by the secondary mineral
${{\mathcal M}}_{m'}$. Blocking may be described phenomenologically by
the surface area relation \begin{align}
\label{surface_armoring}
a_m(t) {\ =\ }a_m^0  \left(\frac{\varphi_m}{\varphi_m^0}\right)^n  \left(\frac{1-\varphi}{1-\varphi_0}\right)^{n'} \left(\frac{\varphi_{m'}^c - \varphi_{m'}}{\varphi_{m'}^c}\right)^{n''},\end{align}

for $\varphi_{m'} < \varphi_{m'}^c$, and \begin{align}
a_m {\ =\ }0,\end{align}
 if
$\varphi_{m'}(t) \geq \varphi_{m'}^c$, where $\varphi_{m'}^c$ represents
the critical volume fraction necessary for complete blocking of the
reaction of mineral ${{\mathcal M}}_m$.

### Sorption

Sorption reactions incorporated into PFLOTRAN consist of ion exchange
and surface complexation reactions for both equilibrium and multirate
formulations.

#### Ion Exchange

Ion exchange reactions may be represented either in terms of bulk- or
mineral-specific rock properties. Changes in bulk sorption properties
can be expected as a result of mineral reactions. However, only the
mineral-based formulation enables these effects to be captured in the
model. The bulk rock sorption site concentration $\omega_{{\alpha}}$, in
units of moles of sites per bulk sediment volume (mol/dm$^3$), is
related to the bulk cation exchange capacity $Q_{{\alpha}}$ (mol/kg) by
the expression
\begin{align}
\omega_{{\alpha}}{\ =\ }\frac{N_{\rm site}}{V} {\ =\ }\frac{N_{\rm site}}{M_s} \frac{M_s}{V_s} \frac{V_s}{V} {\ =\ }(1-\phi) \rho_s Q_{{\alpha}},\end{align}

with solid density $\rho_s$ and porosity $\varphi$. The cation exchange
capacity associated with the $m$th mineral is defined on a molar basis
as
\begin{align}
\omega_m^{\rm CEC} {\ =\ }\frac{N_m}{V} {\ =\ }\frac{N_m}{M_m} \frac{M_m}{V_m} \frac{V_m}{V} {\ =\ }Q_m^{\rm CEC} \rho_m \phi_m.\end{align}

The site concentration $\omega_{{\alpha}}$ is related to the sorbed
concentrations $S_k^{{\alpha}}$ by the expression
\begin{align}
\omega_{{\alpha}}^{} = \sum_k z_k^{} S_k^{{\alpha}}.\end{align}

In PFLOTRAN ion exchange reactions are expressed in the form with the
reference cation denoted by ${{\mathcal A}}_j^{z_j+}$ on the right-hand
side of the reaction \begin{align}
\label{ex1}
z_j^{} {{\mathcal A}}_i^{z_i+} + z_i^{} (\chi_{{\alpha}})_{z_j} {{\mathcal A}}_j {~\rightleftharpoons~}z_i^{} {{\mathcal A}}_j^{z_j+} + z_j^{} (\chi_{{\alpha}})_{z_i} {{\mathcal A}}_i,\end{align}

with valencies $z_j$, $z_i$ of cations ${\mathcal A}_j^{z_j+}$ and
${\mathcal A}_i^{z_i+}$, respectively, and exchange site
$\chi_{{\alpha}}^-$ on the solid surface. The cations
${{\mathcal A}}_i^{z_i+}, \,i\ne j$ represents all other cations
besides the reference cation. The corresponding mass action equation is
given by \begin{align}
\label{ionexmassact}
K_{ij}^{{\alpha}}{\ =\ }\left(\frac{\lambda_i^{{\alpha}}X_i^{{\alpha}}}{a_i}\right)^{z_j}
\left(\frac{a_j}{\lambda_j^{{\alpha}}X_j^{{\alpha}}}\right)^{z_i},\end{align}

with selectivity coefficient $K_{ij}^{{\alpha}}$, solid phase activity
coefficients $\lambda_l^{{\alpha}}$ (taken as unity in what follows),
and mole fraction $X_l^{{\alpha}}$ of the $l$th cation of site
${{\alpha}}$. For $N_c$ cations participating in exchange reactions,
there are $N_c-1$ independent reactions and thus $N_c-1$
independent selectivity coefficients.

The exchange reactions may also be expressed as half reactions in the
form
\begin{align}
z_j^{} \chi_{{\alpha}}^- + {{\mathcal A}}_j^{z_j+} {~\rightleftharpoons~}(\chi_{{\alpha}})_{z_j} {{\mathcal A}}_j^{},\end{align}

with corresponding selectivity coefficient $k_j^{{\alpha}}$. The
half-reaction selectivity coefficients are related to the
$K_{ij}^{{\alpha}}$ by
\begin{align}
\log K_{ij}^{{\alpha}}{\ =\ }z_j^{} \log k_i^{{\alpha}}- z_i^{} \log k_j^{{\alpha}},\end{align}

or \begin{align}
\label{eqkij}
K_{ij}^{{\alpha}}{\ =\ }\frac{(k_i^{{\alpha}})^{z_j}}{(k_j^{{\alpha}})^{z_i}}.\end{align}

This relation is obtained by multiplying the half reaction for cation
${{\mathcal A}}_j$ by the valence $z_i$ and subtracting from the half
reaction for ${{\mathcal A}}_i$ multiplied by $z_j$, resulting in
cancelation of the empty site $X^{{\alpha}}$, to obtain the complete
exchange reaction \ref{ex1}. It should be noted that the coefficients
$k_l^{{\alpha}}$ are not unique since, although there are $N_c$
coefficients in number, only $N_c-1$ are independent and one may be
chosen arbitrarily, usually taken as unity. Thus for
$k_j^{{\alpha}}=1$, Eqn.\ref{eqkij} yields
\begin{align}
k_i^{{\alpha}}{\ =\ }\big(K_{ij}^{{\alpha}}\big)^{1/z_j}.\end{align}

An alternative form of reactions \ref{ex1} often found in the literature
is \begin{align}
\label{rxn2}
\frac{1}{z_i} \,{{\mathcal A}}_i + \frac{1}{z_j}\, (\chi_{{\alpha}})_{z_j} {{\mathcal A}}_j {~\rightleftharpoons~}\frac{1}{z_j} \,{{\mathcal A}}_j + \frac{1}{z_i}\, (\chi_{{\alpha}})_{z_i} {{\mathcal A}}_i,\end{align}

obtained by dividing reaction \ref{ex1} through by the product $z_i z_j$.
The mass action equations corresponding to reactions \ref{rxn2} have the
form
\begin{align}
{\widetilde K}_{ij}^{{\alpha}}{\ =\ }\frac{({\widetilde k}_i^{{\alpha}})^{1/z_i}}{({\widetilde k}_j^{{\alpha}})^{1/z_j}} {\ =\ }\left(\frac{a_j}{X_j^{{\alpha}}}\right)^{1/z_j} \left(\frac{X_i^{{\alpha}}}{a_i}\right)^{1/z_i}.\end{align}

The selectivity coefficients corresponding to the two forms are related
by the expression
\begin{align}
{\widetilde K}_{ij}^{{\alpha}}{\ =\ }\left(K_{ij}^{{\alpha}}\right)^{1/(z_i z_j)},\end{align}

and similarly for $k_i^{{\alpha}}$, $k_j^{{\alpha}}$. When comparing
with other formulations it is important that the user determine which
form of the ion exchange reactions are being used and make the
appropriate transformations.

The selectivity coefficients satisfy the relations
\begin{align}
K_{ji}^{{\alpha}}{\ =\ }\big(K_{ij}^{{\alpha}}\big)^{-1},\end{align}
 and from
the identity
\begin{align}
\left(\frac{X_i^{{\alpha}}}{a_i}\right)^{z_j}\left(\frac{a_j}{X_j^{{\alpha}}}\right)^{z_i}
{\ =\ }\left[
\left(\frac{X_i^{{\alpha}}}{a_i}\right)^{z_l} \left(\frac{a_l}{X_l^{{\alpha}}}\right)^{z_i}
\right]^{z_j/z_l}
\left[
\left(\frac{X_l^{{\alpha}}}{a_l}\right)^{z_j}\left(\frac{a_j}{X_j^{{\alpha}}}\right)^{z_l}
\right]^{z_i/z_l},\end{align}
 the following relation is obtained
\begin{align}
K_{ij}^{{\alpha}}{\ =\ }\big(K_{il}^{{\alpha}}\big)^{z_j/z_l}\big(K_{lj}^{{\alpha}}\big)^{z_i/z_l}.\end{align}


To see how the selectivity coefficients change when changing the
reference cation from ${{\mathcal A}}_j^{z_j+}$ to
${{\mathcal A}}_k^{z_k+}$ note that
\begin{align}
\widetilde K_{jk}^{{\alpha}}{\ =\ }\big(\widetilde K_{kj}^{{\alpha}}\big)^{-1},\end{align}

and
\begin{align}
\widetilde K_{ik}^{{\alpha}}{\ =\ }\widetilde K_{ij}^{{\alpha}}\, \widetilde K_{jk}^{{\alpha}}.\end{align}

This latter relation follows from adding the two reactions

\begin{align}
\frac{1}{z_i} \,{{\mathcal A}}_i + \frac{1}{z_j}\, (\chi_{{\alpha}})_{z_j} {{\mathcal A}}_j &{~\rightleftharpoons~}\frac{1}{z_j} \,{{\mathcal A}}_j + \frac{1}{z_i}\, (\chi_{{\alpha}})_{z_i} {{\mathcal A}}_i,\\
\frac{1}{z_j} \,{{\mathcal A}}_j + \frac{1}{z_k}\, (\chi_{{\alpha}})_{z_k} {{\mathcal A}}_k &{~\rightleftharpoons~}\frac{1}{z_k} \,{{\mathcal A}}_k + \frac{1}{z_j}\, (\chi_{{\alpha}})_{z_j} {{\mathcal A}}_j,\end{align}
to give
\begin{align}
\frac{1}{z_i} \,{{\mathcal A}}_i + \frac{1}{z_k}\, (\chi_{{\alpha}})_{z_k} {{\mathcal A}}_k {~\rightleftharpoons~}\frac{1}{z_k} \,{{\mathcal A}}_k + \frac{1}{z_i}\, (\chi_{{\alpha}})_{z_i} {{\mathcal A}}_i,\end{align}
with ${{\mathcal A}}_k^{z_k+}$ as reference species.

In terms of the
selectivity coefficients $K_{ij}^{{\alpha}}$ it follows that
\begin{align}
\big(K_{ik}^{{\alpha}}\big)^{1/(z_i z_k)} {\ =\ }\big(K_{ij}^{{\alpha}}\big)^{1/(z_i z_j)} \big(K_{jk}^{{\alpha}}\big)^{1/(z_j z_k)},\end{align}

or
\begin{align}
K_{ik}^{{\alpha}}{\ =\ }\big(K_{ij}^{{\alpha}}\big)^{z_k /z_j} \big(K_{jk}^{{\alpha}}\big)^{z_i/ z_j}.\end{align}

In terms of the coefficients $k_i^{{\alpha}}$ and
$\overline k_i^{{\alpha}}$ corresponding to reference cation
${{\mathcal A}}_k$ the transformation becomes
\begin{align}
\frac{\big(\overline k_i^{{\alpha}}\big)^{z_k}}{\big(\overline k_i^{{\alpha}}\big)^{z_i}} {\ =\ }\left(\frac{\big(k_i^{{\alpha}}\big)^{z_j}}{\big(k_i^{{\alpha}}\big)^{z_j}}\right)^{z_k/z_j}
\left(\frac{\big(k_j^{{\alpha}}\big)^{z_k}}{\big(k_k^{{\alpha}}\big)^{z_j}}\right)^{z_i/z_j}.\end{align}

In terms of the coefficients $k_l^{{\alpha}}$ the sorbed concentration
for the $i$th cation can be expressed as a function of the reference
cation from the mass action equations according to
\begin{align}
X_i^{{\alpha}}{\ =\ }k_i^{{\alpha}}a_i^{} \left(\frac{X_j^{{\alpha}}}{k_j^{{\alpha}}a_j^{}}\right)^{z_i/z_j}.\end{align}

For a given reference species ${{\mathcal A}}_{J_0}$ the coefficients
$K_{iJ_0}$ are uniquely determined. For some other choice of reference
species, say ${{\mathcal A}}_{I_0}$, the coefficients $K_{iI_0}$ are
related to the original coefficients by the expressions
\begin{align}
\log K_{J_0I_0} &{\ =\ }-\log K_{I_0J_0},\\\end{align}


Taking the reference cation as ${{\mathcal A}}_j$ then $k_i^{{\alpha}}$
is given by

\begin{align}
k_i^{{\alpha}}&{\ =\ }\big(K_{ij}^{{\alpha}}(k_j^{{\alpha}})^{z_i}\big)^{1/z_j},\\
&{\ =\ }(K_{ij}^{{\alpha}})^{1/z_j}, \ \ \ \ \ \ \ \ \ \ \ \ (k_j^{{\alpha}}=1),\\
&{\ =\ }K_{ij}^{{\alpha}}, \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (z_j=1).\end{align}

As an example consider the ion-exchange reactions with Ca$^{2+}$ as
reference species

\begin{align}
\rm 2 \, Na^+ + \chi_2 Ca &{~\rightleftharpoons~}\rm Ca^{2+} + 2 \, \chi Na,\\
\rm 2\,Mg^{2+} + 2\,\chi_2 Ca &{~\rightleftharpoons~}\rm 2\,Ca^{2+} + 2\,\chi_2 Mg,\end{align}


with selectivity coefficients $K_{\rm NaCa}$ and $K_{\rm MgCa}$.
Alternatively, using Na$^+$ as reference cation gives

\begin{align}
\rm Ca^{2+} + 2 \, \chi Na &{~\rightleftharpoons~}\rm 2 \, Na^+ + \chi_2 Ca,\\
\rm Mg^{2+} + 2 \, \chi Na &{~\rightleftharpoons~}\rm 2 \, Na^{+} + \chi_2 Mg,\end{align}


with selectivity coefficients $K_{\rm CaNa}$ and $K_{\rm MgNa}$. The
selectivity coefficients are related by the equations

\begin{align}
\log K_{\rm CaNa} & {\ =\ }-\log K_{\rm NaCa},\\
\log K_{\rm MgNa} &{\ =\ }\frac{1}{2} \, \log K_{\rm MgCa} - \log K_{\rm NaCa}.\end{align}


Using the Gaines-Thomas convention, the equivalent fractions
$X_k^{{\alpha}}$ are defined by
\begin{align}
X_k^{{\alpha}}= \frac{z_k S_k^{{\alpha}}}{\displaystyle\sum_l z_l S_l^{{\alpha}}} = \frac{z_k}{\omega_{{\alpha}}}S_k^{{\alpha}},\end{align}

with \begin{align}
\sum_k X_k^{{\alpha}}= 1.\end{align}


For equivalent exchange $(z_j=z_i=z)$, an explicit expression
exists for the sorbed concentrations given by
\begin{align}
S_j^{{\alpha}}{\ =\ }\frac{\omega_{{\alpha}}}{z} \frac{k_j^{{\alpha}}\gamma_j m_j^{}}{\displaystyle\sum_l k_l^{{\alpha}}\gamma_l m_l^{}},\end{align}

where $m_k$ denotes the $k$th cation molality. This expression follows
directly from the mass action equations for the sorbed cations and
conservation of exchange sites.

In the more general case $(z_i\ne z_j)$ it is necessary to solve the
nonlinear equation
\begin{align}
X_j^{{\alpha}}+ \sum_{i\ne j} X_i^{{\alpha}}{\ =\ }1,\end{align}
 for the
reference cation mole fraction $X_j$. From the mass action equation Eqn.\ref{ionexmassact}
it follows that
\begin{align}
X_i^{{\alpha}}{\ =\ }k_i^{{\alpha}}a_i^{} \left(\frac{X_j^{{\alpha}}}{k_j^{{\alpha}}a_j^{}}\right)^{z_i/z_j}.\end{align}

Defining the function
\begin{align}
f(X_j^{{\alpha}}) {\ =\ }X_j^{{\alpha}}+ \sum_{i\ne j}X_i^{{\alpha}}(X_j^{{\alpha}})-1,\end{align}

its derivative is given by
\begin{align}
\frac{df}{dX_j^{{\alpha}}} {\ =\ }1 - \frac{1}{z_j^{} X_j^{{\alpha}}}\sum_{i\ne j} z_i^{} k_i^{{\alpha}}a_i^{} \left(\frac{X_j^{{\alpha}}}{k_j^{{\alpha}}a_j^{}}\right)^{z_i/z_j}.\end{align}

The reference mole fraction is then obtained by Newton-Raphson iteration
\begin{align}
(X_j^{{\alpha}})^{k+1} {\ =\ }(X_j^{{\alpha}})^k -\dfrac{f[(X_j^{{\alpha}})^k]}{\dfrac{df[(X_j^{{\alpha}})^k]}{dX_j^{{\alpha}}}}.\end{align}


The sorbed concentration for the $j$th cation appearing in the
accumulation term is given by
\begin{align}
S_j^{{\alpha}}{\ =\ }\frac{\omega_{{\alpha}}}{z_j} X_j^{{\alpha}},\end{align}

with the derivatives for $j\ne l$

\begin{align}
\dfrac{{{\partial}}S_j^{{\alpha}}}{{{\partial}}m_l} &{\ =\ }-\frac{\omega_{{\alpha}}}{m_l} \dfrac{X_j^{{\alpha}}X_l^{{\alpha}}}{\displaystyle\sum_l z_l X_l^{{\alpha}}},\\
&{\ =\ }-\frac{1}{m_l} \dfrac{z_jz_lS_j^{{\alpha}}S_l^{{\alpha}}}{\displaystyle\sum_l z_l^2 S_l^{{\alpha}}},\end{align}


and for $j=l$

\begin{align}
\dfrac{{{\partial}}S_j^{{\alpha}}}{{{\partial}}m_j} &{\ =\ }\frac{\omega_{{\alpha}}X_j^{{\alpha}}}{z_j m_j} \left(1-\dfrac{z_j X_j^{{\alpha}}}{\displaystyle\sum_{l} z_{l} X_{l}^{{\alpha}}}\right),\\
&{\ =\ }\frac{S_j^{{\alpha}}}{m_j} \left(1-\dfrac{z_j^2 S_j^{{\alpha}}}{\displaystyle\sum_{l} z_{l}^2 S_{l}^{{\alpha}}}\right).
\end{align}

#### Surface Complexation

Surface complexation reactions are assumed to have the form
\begin{align}
\label{srfrxn}
\nu_{{\alpha}}>\chi_{{\alpha}}+ \sum_j\nu_{ji} {{\mathcal A}}_j {~\rightleftharpoons~}> {{\mathcal S}}_{i{{\alpha}}},\end{align}

for the $i$th surface complex $>{{\mathcal S}}_{i{{\alpha}}}$ on
site ${{\alpha}}$ and empty site $>\chi_{{\alpha}}$. As follows from
the corresponding mass action equation the equilibrium sorption
concentration $S_{i{{\alpha}}}^{\rm eq}$ is given by
\begin{align}
S_{i{{\alpha}}}^{\rm eq}{\ =\ }\frac{\omega_{{\alpha}}K_i Q_i}{1+\sum_l K_lQ_l},\end{align}

and the empty site concentration by
\begin{align}
S_{{\alpha}}^{\rm eq}{\ =\ }\frac{\omega_{{\alpha}}}{1+\sum_l K_lQ_l},\end{align}

where the ion activity product $Q_i$ is defined by
\begin{align}
Q_i{\ =\ }\prod_j\big(\gamma_jC_j\big)^{\nu_{ji}}.\end{align}
 The site
concentration $\omega_{{\alpha}}$ satisfies the relation
\begin{align}
\label{totsite}
\omega_{{\alpha}}{\ =\ }S_{{\alpha}}+ \sum_i S_{i{{\alpha}}},\end{align}
 and is
constant. The equilibrium sorbed concentration
$S_{j{{\alpha}}}^{\rm eq}$ is defined as \begin{align}
\label{qeq}
S_{j{{\alpha}}}^{\rm eq} {\ =\ }\sum_i \nu_{ji}^{} S_{i{{\alpha}}}^{\rm eq}{\ =\ }\frac{\omega_{{\alpha}}}{1+\sum_l K_lQ_l} \sum_i \nu_{ji}K_i Q_i.\end{align}


#### Multirate Sorption

In the multirate model the rates of sorption reactions are described
through a kinetic relation given by \begin{align}
\label{sorbed}
\frac{{{\partial}}S_{i{{\alpha}}}}{{{\partial}}t} {\ =\ }k_{{\alpha}}^{} \big(S_{i{{\alpha}}}^{\rm eq}-S_{i{{\alpha}}}\big),\end{align}

for surface complexes, and \begin{align}
\label{fsite}
\frac{{{\partial}}S_{{{\alpha}}}}{{{\partial}}t} &{\ =\ }-\sum_i k_{{\alpha}}^{} \big(S_{i{{\alpha}}}^{\rm eq}-S_{i{{\alpha}}}\big),\\
&{\ =\ }k_{{\alpha}}\big(S_{{\alpha}}^{\rm eq}-S_{{{\alpha}}}\big),\end{align}

for empty sites, where $S_{{\alpha}}^{\rm eq}$ denotes the equilibrium
sorbed concentration. For simplicity, in what follows it is assumed that
$\nu_{{\alpha}}=1$. With each site ${{\alpha}}$ is associated a rate
constant $k_{{\alpha}}$ and site concentration $\omega_{{\alpha}}$.
These quantities are defined through a given distribution of sites
$\wp({{\alpha}})$, such that
\begin{align}
\int_0^\infty \wp(k_{{\alpha}})dk_{{\alpha}}{\ =\ }1.\end{align}
 The fraction
of sites $f_{{\alpha}}$ belonging to site ${{\alpha}}$ is determined
from the relation
\begin{align}
f_{{\alpha}}{\ =\ }\int_{k_{{\alpha}}-\Delta k_{{\alpha}}/2}^{k_{{\alpha}}+\Delta k_{{\alpha}}/2} \wp(k_{{\alpha}})dk_{{\alpha}}\simeq \wp(k_{{\alpha}})\Delta k_{{\alpha}},\end{align}

with the property that \begin{align}
\sum_{{\alpha}}f_{{\alpha}}=1.\end{align}
 Given that the
total site concentration is $\omega$, then the site concentration
$\omega_{{\alpha}}$ associated with site ${{\alpha}}$ is equal to
\begin{align}
\omega_{{\alpha}}{\ =\ }f_{{\alpha}}\omega.\end{align}


An alternative form of these equations is obtained by introducing the
total sorbed concentration for the $j$th primary species for each site
defined as \begin{align}
S_{j{{\alpha}}}{\ =\ }\sum_i \nu_{ji}S_{i{{\alpha}}}.\end{align}

Then the transport equations become \begin{align}
\label{totj}
\frac{{{\partial}}}{{{\partial}}t}\left(\varphi \Psi_j + \sum_{{{\alpha}}}S_{j{{\alpha}}}\right) + {\boldsymbol{\nabla}}\cdot{\boldsymbol{\Omega}}_j {\ =\ }- \sum_m\nu_{jm}I_m.\end{align}

The total sorbed concentrations are obtained from the equations
\begin{align}
\label{sja}
\frac{{{\partial}}S_{j{{\alpha}}}}{{{\partial}}t} {\ =\ }k_{{\alpha}}^{} \big(S_{j{{\alpha}}}^{\rm eq}-S_{j{{\alpha}}}\big).\end{align}


### Sorption Isotherm &lt;Under Revision&gt;

The distribution coefficient $\widetilde K_j^D$ [m$^3$ kg$^{-1}$] is
customarily defined as the ratio of sorbed to aqueous concentrations
with the sorbed concentration referenced to the mass of solid as given
by

\begin{align}
\widetilde K_j^D &{\ =\ }\frac{M_j^s/M_s}{M_j^{\rm aq}/V_l},\\
&{\ =\ }\frac{N_j^s/M_s}{N_j^{\rm aq}/V_l},\\
&{\ =\ }\frac{\widetilde S_j}{C_j} {\ =\ }\frac{1}{\rho_w}\frac{\widetilde S_j}{m_j},\end{align}


where $M_j^s = W_j N_j^s$, $M_j^{\rm aq}=W_j N_j^{\rm aq}$,
refers to the mass and number of moles of sorbed and aqueous solute
related by the formula weight $W_j$ of the $j$th species, $M_s$ refers
to the mass of the solid, $V_l$ denotes the aqueous volume,
$\widetilde S_j=N_j^s/M_s$ [mol kg$^{-1}$] represents the sorbed
concentration referenced to the mass of solid,
$C_j=N_j^{\rm aq}/V_l$ denotes molarity, and $m_j=C_j/\rho_w$
represents molality, where $\rho_w$ is the density of pure water.

The distribution coefficient $\widetilde K_j^D$ may be related to its
dimensionless counterpart $K_j^D$ [—] defined by
\begin{align}
K_j^D &{\ =\ }\frac{N_i^s}{N_i^{\rm aq}} {\ =\ }\frac{N_i^s/V}{N_i^{\rm aq}/V}{\ =\ }\frac{1}{\varphi s_l}\frac{S_j}{C_j},\label{kdj}\end{align}

by writing
\begin{align}
K_j^D &{\ =\ }\frac{N_i^s}{M_s} \frac{M_s}{V_s} \frac{V_s}{V_p} \frac{V_p}{V_l} \frac{V_l}{N_i^{\rm aq}},\\
&{\ =\ }\rho_s \frac{1-\varphi}{\varphi s_l} \widetilde K_j^D {\ =\ }\frac{\rho_b}{\varphi s_l} \widetilde K_j^D,\end{align}

with grain density $\rho_s=M_s/V_s$, bulk density
$\rho_b=(1-\varphi)\rho_s$, porosity $\varphi=V_p/V$, and
saturation $s_l=V_l/V_p$.

An alternative definition of the distribution coefficient denoted by
$\widehat K_j^D$ [kg m$^{-3}$] is obtained by using molality to define
the solute concentration and referencing the sorbed concentration to the
bulk volume $V$
\begin{align}
\widehat K_j^D {\ =\ }\frac{N_j^s/V}{N_j^{\rm aq}/M_w} {\ =\ }\frac{S_j}{m_j}.\end{align}


A sorption isotherm $S_j$ may be specified for any primary species
${{\mathcal A}}_j$ resulting in the transport equation
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi s_l C_j + {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_j {\ =\ }-\frac{{{\partial}}S_j}{{{\partial}}t},\end{align}

for a partially saturated medium. Substituting
$S_j=\varphi s_l K_j^D C_j$ from Eqn.\ref{kdj} and introducing the
retardation ${{{\mathcal R}}}_j$ gives
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} R_j \varphi s_l C_j + {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_j {\ =\ }0,\end{align}

with the retardation given by the alternative forms

\begin{align}
R_j &{\ =\ }1 + K_j^D, \ \ \ \ \ \ (\text{dimensionless)},\\
&{\ =\ }1+ \frac{\rho_b}{\varphi s_l} \widetilde K_j^D, \ \ \ \ \ \ (\text{conventional}),\\
&{\ =\ }1+ \frac{1}{\varphi s_l \rho_w} \widehat K_j^D, \ \ \ \ \ \ (\text{molality-based}).\end{align}


Three distinct models are available for the sorption isotherm $S_j$ in
PFLOTRAN:

-   linear $K_D$ model: \begin{align}
\label{linkd}
    S_j {\ =\ }\varphi s_l K_j^D C_j {\ =\ }\widehat K_j^D m_j,\end{align}
 with
    distribution coefficient $\widehat K_j^D$,

-   Langmuir isotherm: \begin{align}
\label{Langmuir}
    S_j{\ =\ }\frac{K_j^L b_j^L C_j/ \rho_w}{1+K_j^L C_j/ \rho_w} {\ =\ }\frac{K_j^L b_j^L m_j}{1+K_j^L m_j},\end{align}

    with Langmuir coefficients $K_j^L$ and $b_j^L$, and

-   Freundlich isotherm: \begin{align}
\label{Freundlich}
    S_j {\ =\ }K_j^F \left(\frac{C_j}{\rho_w}\right)^{(1/n_j^F)}  {\ =\ }K_j^F \big(m_j\big)^{(1/n_j^F)},\end{align}

    with coefficients $K_j^F$ and $n_j^F$.

### Colloid-Facilitated Transport

Colloid-facilitated transport is implemented into PFLOTRAN based on
surface complexation reactions. Competition between mobile and immobile
colloids and stationary mineral surfaces is taken into account. Colloid
filtration processes are not currently implemented into PFLOTRAN. A
colloid is treated as a solid particle suspended in solution or attached
to a mineral surface. Colloids may be generated through nucleation of
minerals in solution, although this effect is not included currently in
the code.

Three separate reactions may take place involving competition between
mobile and immobile colloids and mineral surfaces
\begin{align}
\mathrm{>} X_k^{{\rm m}}+ \sum_j\nu_{jk}{{\mathcal A}}_j &{~\rightleftharpoons~} \mathrm{>} S_k^{{\rm m}},\\
\mathrm{>} X_k^{{\rm im}}+ \sum_j\nu_{jk}{{\mathcal A}}_j &{~\rightleftharpoons~} \mathrm{>} S_k^{{\rm im}},\\
\mathrm{>} X_k^s + \sum_j\nu_{jk}{{\mathcal A}}_j &{~\rightleftharpoons~} \mathrm{>} S_k^s,
\end{align}

with corresponding reaction rates $I_k^{{\rm m}}$, $I_k^{{\rm im}}$, and
$I_k^s$, where the superscripts $s$, $m$, and $im$ denote mineral
surfaces, and mobile and immobile colloids, respectively. In addition,
reaction with minerals ${{\mathcal M}}_s$ may occur according to the
reaction
\begin{align}
\sum_j\nu_{js}{{\mathcal A}}_j {~\rightleftharpoons~}{{\mathcal M}}_s.\end{align}

The transport equations for primary species, mobile and immobile
colloids, read
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi s_l \Psi_j^l + {\boldsymbol{\nabla}}\cdot{\boldsymbol{\Omega}}_j^l &{\ =\ }-\sum_k\nu_{jk}\big(I_k^{{\rm m}}+ I_k^{{\rm im}}+ \sum_s I_k^s\big) - \sum_s \nu_{js} I_s,\label{rateform}\\
\frac{{{\partial}}}{{{\partial}}t} \varphi s_l S_k^{{\rm m}}+ {\boldsymbol{\nabla}}\cdot{\boldsymbol{q}}_c S_k^{{\rm m}}& {\ =\ }I_k^{{\rm m}},\label{mobile}\\
\frac{{{\partial}}}{{{\partial}}t} S_k^{{\rm im}}& {\ =\ }I_k^{{\rm im}},\label{immobile}\\
\frac{{{\partial}}}{{{\partial}}t} S_k^s & {\ =\ }I_k^s,\label{solid}\end{align}

where ${\boldsymbol{q}}_c$ denotes the colloid Darcy velocity which may
be greater than the fluid velocity ${\boldsymbol{q}}$. For conditions of
local equilibrium the sorption reaction rates may be eliminated and
replaced by algebraic sorption isotherms to yield \begin{align}
\label{eqform}
\frac{{{\partial}}}{{{\partial}}t}\Big[ \varphi s_l \Psi_j^l + \sum_k \nu_{jk} \big(\varphi s_l S_k^{{\rm m}}+ S_k^{{\rm im}}+ \sum_s S_k^s\big) \Big] + {\boldsymbol{\nabla}}\cdot\Big({\boldsymbol{\Omega}}_j^l + {\boldsymbol{q}}_c \sum_k \nu_{jk} S_k^{{\rm m}}\Big) {\ =\ }- \sum_s \nu_{js} I_s.\end{align}


In the kinetic case either form of the primary species transport
equations given by Eqn.\ref{rateform} or \ref{eqform}can be used provided it is coupled with the
appropriate kinetic equations Eqns.\ref{mobile}--\ref{solid}. The mobile case leads to
additional equations that must be solved simultaneously with the primary
species equations. A typical expression for $I_k^m$ might be
\begin{align}
I_k^m {\ =\ }k_k\big(S_k^m - S_{km}^{\rm eq}\big),\end{align}
 with rate
constant $k_k$ and where $S_{km}^{\rm eq}$ is a known function of the
solute concentrations. In this case, Eqn.\ref{mobile} must be added to the primary
species transport equations. Further reduction of the transport
equations for the case where a flux term is present in the kinetic
equation is not possible in general for complex flux terms.

Tracer Mean Age
---------------

PFLOTRAN implements the Eulerian formulation of solute age for a
nonreactive tracer following Goode (1996). PFLOTRAN solves the
advection-diffusion/dispersion equation for the mean age given by
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi s AC + {\boldsymbol{\nabla}}\cdot\Big({\boldsymbol{q}}AC - \varphi s D {\boldsymbol{\nabla}}(AC)\Big) {\ =\ }\varphi s C,\end{align}

where $A$ denotes the mean age of the tracer with concentration $C$.
Other quantities appearing in the age equation are identical to the
tracer transport equation for a partially saturated porous medium with
saturation state $s$. The age and tracer transport equations are solved
simultaneously for the age-concentration $\alpha = A C$ and tracer
concentration $C$. The age-concentration ${{\alpha}}$ satisfies the
usual advection-diffusion-dispersion equation with a source term on the
right-hand side.

The mean tracer age is calculated in PFLOTRAN by adding the species
`Tracer_Age` together with `Tracer` to the list
of primary species

      PRIMARY_SPECIES
        Tracer
        Tracer_Age
      /

including sorption through a constant $K_d$ model if desired

      SORPTION
        ISOTHERM_REACTIONS
          Tracer
            TYPE LINEAR
            DISTRIBUTION_COEFFICIENT 500. ! kg water/m^3 bulk
          /
          Tracer_Age
            TYPE LINEAR
            DISTRIBUTION_COEFFICIENT 500. ! kg water/m^3 bulk
          /
        /
      /

and specifying these species in the initial and boundary
`CONSTRAINT` condition as e.g.:

    CONSTRAINT initial
      CONCENTRATIONS
        Tracer     1.e-8        F
        Tracer_Age 1.e-16       F
      /
    /

Output is given in terms of $\alpha$ and $C$ from which the mean age $A$
can be obtained as $A{\ =\ }\alpha/C$.


Thermodynamic Database {#thermo:database}
----------------------

PFLOTRAN reads thermodynamic data from a database that may be customized
by the user. Reactions included in the database consist of aqueous
complexation, mineral precipitation and dissolution, gaseous reactions,
and surface complexation. Ion exchange reactions and their selectivity
coefficients are entered directly from the input file. A standard
database supplied with the code is referred to as
`hanford.dat` and is found in the `./database`
directory in the PFLOTRAN mercurial repository. This database is an
ascii text file that can be edited by any editor and is equivalent to
the EQ3/6 database:

    data0.com.V8.R6
    CII: GEMBOCHS.V2-EQ8-data0.com.V8.R6
    THERMODYNAMIC DATABASE
    generated by GEMBOCHS.V2-Jewel.src.R5 03-dec-1996 14:19:25

The database provides equilibrium constants in the form of log $K$
values at a specified set of temperatures listed in the top line of the
database. A least squares fit is used to interpolate the log $K$ values
between the database temperatures using a Maier-Kelly expansion of the
form \begin{align}
\label{mk}
\log K {\ =\ }c_{-1} \ln T + c_0 + c_1 T + \frac{c_2}{T} + \frac{c_3}{T^2},\end{align}

with fit coefficients $c_i$. The thermodynamic database stores all
chemical reaction properties (equilibrium constant $\log K_r$, reaction
stoichiometry $\nu_{ir}$, species valence $z_i$, Debye parameter $a_i$,
mineral molar volume $\overline V_m$, and formula weight $w_i$) used in
PFLOTRAN. The database is divided into 5 blocks as listed in
Table [tdatabase], consisting of database primary species, aqueous
complex reactions, gaseous reactions, mineral reactions, and surface
complexation reactions. Each block is terminated by a line beginning
with `’null’`. The quantity $N_{\rm temp}$ refers to the
number of temperatures at which log $K$ values are stored in the
database. In the `hanford.dat` database $N_{\rm temp}=8$ with
equilibrium constants stored at the temperatures: 0, 25, 60, 100, 150,
200, 250, and 300$^\circ$C. The pressure is assumed to lie
along the saturation curve of pure water for temperatures above
25$^\circ$C and is equal to 1 bar at lower temperatures.
Reactions in the database are assumed to be written in the canonical
form
\begin{align}
{{\mathcal A}}_r {~\rightleftharpoons~}\sum_{i=1}^{\rm nspec} \nu_{ir}{{\mathcal A}}_i,\end{align}

for species ${{\mathcal A}}_r$, where `nspec` refers to the
number of aqueous or gaseous species ${{\mathcal A}}_i$ on the
right-hand side of the reaction. Redox reactions in the standard
database `hanford.dat` are usually written in terms of
O$_{2(g)}$. Complexation reactions involving redox sensitive species are
written in such a manner as to preserve the redox state.

ll Primary Species: & name, $a_0$, $z$, $w$\
Secondary Species: & name, nspec, ($\nu$(n), name($n$), $n$=1, nspec),
log$K$(1:$N_{\rm temp}$), $a_0$, $z$, $w$\
Gaseous Species: & name, $\overline V$, nspec, ($\nu$(n), name($n$),
$n$=1, nspec), log$K$(1:$N_{\rm temp}$), $w$\
Minerals: & name, $\overline V$, nspec, ($\nu$(n), name($n$), $n$=1,
nspec), log$K$(1:$N_{\rm temp}$), $w$\
Surface Complexes: & $>$name, nspec, $\nu$, $>$site, ($\nu$(n),
name($n$), $n$=1, nspec-1),\
&

log$K$(1:$N_{\rm temp}$), $z$, $w$\

Note that chemical reactions are not unique. For example, given a
particular mineral reaction
\begin{align}
\sum_j \nu_{jm} {{\mathcal A}}_j {~\rightleftharpoons~}{{\mathcal M}}_m,\end{align}

and equally acceptable reaction is the scaled reaction
\begin{align}
\sum_j f_m\nu_{jm} {{\mathcal A}}_j {~\rightleftharpoons~}f_m{{\mathcal M}}_m,\end{align}

with scale factor $f_m$ corresponding to a different choice of formula
unit. A different scale factor may be used for each mineral. The scaled
reaction corresponds to
\begin{align}
\sum_j \nu_{jm}' {{\mathcal A}}_j {~\rightleftharpoons~}{{\mathcal M}}_m',\end{align}

with ${{\mathcal M}}_m' = f_m{{\mathcal M}}_m$,
$\nu_{jm}' = f_m\nu_{jm}$. In addition, mineral molar volume
$\overline V_m$, formula weight $W_m$, equilibrium constant $K_m$, and
the kinetic rate constant $k_m$ are scaled according to

\begin{align}
\overline V_m' &{\ =\ }f_m\overline V_m,\\
W_m' &{\ =\ }f_m W_m,\\
\log K_m' &{\ =\ }f_m \log K_m.\end{align}


The saturation index ${\rm SI}_m$ transforms according to
\begin{align}
{\rm SI}_m' {\ =\ }K_m' Q_m' {\ =\ }\big(K_m Q_m\big)^{f_m} {\ =\ }({\rm SI}_m)^{f_m}.\end{align}

Consequently, equilibrium is not affected as is to be expected. However,
a more general form for the reaction rate is needed involving Temkin’s
constant [see Eqn.\ref{Im}], in order to ensure that the identical solution to
the reactive transport equations is obtained using the scaled reaction.
Thus it is necessary that the following conditions hold
\begin{align}
{\overline V}_m' I_m' &{\ =\ }\overline V_m I_m,\\
\nu_{jm}' I_m' &{\ =\ }\nu_{jm} I_m.\end{align}
 This requires that the reaction
rate $I_m$ transform as \begin{align}
I_m' {\ =\ }\frac{1}{f_m} I_m,\end{align}
 which
guarantees that mineral volume fractions and solute concentrations are
identical as obtained from solving the reactive transport equations. For
this relation to hold it is necessary that the kinetic rate constant
transform as \begin{align}
k_m' {\ =\ }f_m^{-1}k_m.\end{align}
 From the above relations it is
found that the reaction rate transforms according to

\begin{align}
I_m' &{\ =\ }-k_m' A_m \big(1-(K_m'Q_m')^{1/\sigma_m'}\big),\\
&{\ =\ }-\frac{1}{f_m} k_m A_m \big(1-(K_m Q_m)^{f_m/\sigma_m'} \big),\\
&{\ =\ }\frac{1}{f_m} I_m,\end{align}


where the last result is obtained by scaling Temkin’s constant according
to \begin{align}
\sigma_m' {\ =\ }f_m\sigma_m.\end{align}
 It should be noted that the mineral
concentration
$(C_m' =({\overline V}_m^{-1})^{'} \phi_m = f_m^{-1} C_m)$,
differs in the two formulations; however, mass density
$(\rho_m = W_m \overline V_m^{-1})$ is an invariant, unlike molar
density $(\eta_m=\overline V_m^{-1})$.

Eh, pe
------

Output for Eh and pe is calculated from the half-cell reaction
\begin{align}
\label{redox}
\rm 2 \, H_2O - 4 \, H^+ - 4\,e^- {~\rightleftharpoons~}\rm O_2,\end{align}
 with
the corresponding equilibrium constant fit to the Maier-Kelly expansion
Eqn.\ref{mk}. The fit coefficients are listed in Table [tmkfit].

|  coefficient   |   value           |
| -------------- | ----------------- |
| $c_{-1}$       |     6.745529048   |
| $c_0$          | $-$48.295936593   |
| $c_1$          |  $-$0.000557816   |
| $c_2$          | 27780.749538022   |
| $c_3$          |  4027.337694858   |

  : Fit coefficients for log $K$ of reaction \ref{redox}.<span
  data-label="tmkfit"></span>

Geomechanics
------------

In PFLOTRAN, linear elasticity model is assumed as the constitutive
model for deformation of the rock. Biot’s model is used to incorporate
the effect of flow on the geomechanics. In addition, the effect of
temperature on geomechanics is considered via coefficient of thermal
expansion. The following governing equations are used: \begin{align}
& \nabla \cdot [{\boldsymbol{\sigma}}] + \rho {\boldsymbol{b}} = 0 \quad \mathrm{in} \; \Omega, \label{Eq:mom}\\
& {\boldsymbol{\sigma}} = \lambda \text{tr}\left({\boldsymbol{\varepsilon}}\right) + 2\mu {\boldsymbol{\varepsilon}} - \beta (p - p_0) {\boldsymbol{I}} - \alpha (T - T_0) {\boldsymbol{I}}, \\
& {\boldsymbol{\varepsilon}} = \frac{1}{2} \left(\nabla{\boldsymbol{u}}({\boldsymbol{x}}) + [\nabla {\boldsymbol{u}}({\boldsymbol{x}})]^{T}  \right), \\
&{\boldsymbol{u}}({\boldsymbol{x}}) = {\boldsymbol{u}}^p({\boldsymbol{x}}) \quad \mathrm{on} \; \Gamma^D,\label{eqn:diri}\\
& {\boldsymbol{\sigma}}{\boldsymbol{n}}({\boldsymbol{x}}) = t^p({\boldsymbol{x}}) \quad \mathrm{on} \; \Gamma^N, \label{eqn:neu}\end{align}

where ${\boldsymbol{u}}$ is the unknown displacement field,
${\boldsymbol{\sigma}}$ is the Cauchy stress tensor, $\lambda$, $\mu$
are Lamé parameters (Young’s modulus and Poisson’s ratio can be related
to these two parameters), ${\boldsymbol{b}}$ is the specific body force
(which is gravity in most cases), ${\boldsymbol{n}}$ is the outward
normal to the boundary $\Gamma^N$. Also, ${\boldsymbol{u}}^p$ is the
prescribed values of ${\boldsymbol{u}}$ on the Dirichlet part of the
boundary $\Gamma^D$, and ${\boldsymbol{t}}^p$ is the prescribed traction
on $\Gamma^N$. Additionally, $\beta$ is the Biot’s coefficient, $\alpha$
is the coefficient of thermal expansion, $p$, $T$ are the fluid pressure
and temperature, obtained by solving subsurface flow problem. Also,
$p_0$ and $T_0$ are set to initial pressure and temperature in the
domain, ${\boldsymbol{\varepsilon}}$ is the strain tensor and
$\text{tr}$ is the trace of a second order tensor, $\Omega$ is the
domain, and ${\boldsymbol{I}}$ is the identity tensor. Note that stress
is assumed *positive under tension*. The effect of deformation on the
pore structure is accounted for via
\begin{align}
\phi = \frac{\phi_0}{1 + (1-\phi_0) \text{tr}({\boldsymbol{\varepsilon}})}.\end{align}


Note that the above equations are solved using the finite element method
(Galerkin finite element) with the displacements solved for at the
vertices. Since, the flow equations are solved via the finite volume
method with unknowns such as pressure and temperature solved for at the
cell centers, in order to transfer data from the subsurface to
geomechanics grid without interpolation, the geomechanics grid is
constructed such that the vertices of the geomechanics grid coincide
with the cell centers of the subsurface mesh. That is, the dual mesh of
the subsurface mesh is used for the geomechanics solve.

Also, the geomechanics grid must be read in as an unstructured grid.
Even if one needs to work with a structured grid, the grid must be set
up in the unstructured grid format.

Appendix B: Method of Solution {#appendix-b-method-of-solution }
==============================

The flow and heat equations (Modes: RICHARDS, MPHASE, FLASH2, TH, …) are
solved using a fully implicit backward Euler approach based on
Newton-Krylov iteration. Both fully implicit backward Euler and operator
splitting solution methods are supported for reactive transport.

Integrated Finite Volume Discretization
---------------------------------------

The governing partial differential equations for mass conservation can
be written in the general form
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} A_j + {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_j {\ =\ }Q_j,\end{align}

with accumulation $A_j$, flux ${\boldsymbol{F}}_j$ and source/sink
$Q_j$. Integrating over a REV corresponding to the $n$th grid cell with
volume $V_n$ yields
\begin{align}
\frac{d}{dt}\int_{V_n} A_j \, dV + \int_{V_n} {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_j {\ =\ }\int_{V_n}Q_j\, dV.\end{align}

The accumulation term has the finite volume form
\begin{align}
\frac{d}{dt}\int_{V_n} A_j \, dV {\ =\ }\frac{A_{jn}^{t+\Delta t} - A_{jn}^t}{\Delta t} \, V_n,\end{align}

with time step $\Delta t$. The flux term can be expanded as a surface
integral using Gauss’ theorem
\begin{align}
\int_{V_n} {\boldsymbol{\nabla}}\cdot{\boldsymbol{F}}_j &{\ =\ }\int_{{{\partial}}V_n} {\boldsymbol{F}}_j \cdot d{\boldsymbol{S}},\\
&{\ =\ }\sum_{n'} F_{j,nn'} A_{nn'},\end{align}
 where the latter finite volume
form is based on the two-point flux approximation, where the sum over
$n'$ involves nearest neighbor grid cells connected to the $n$th node
with interfacial area $A_{nn'}$. The discretized flux has the form for
fluid phase ${{\alpha}}$
\begin{align}
F_{j,nn'}^{{\alpha}}{\ =\ }\big(q_{{\alpha}}X_{{\alpha}}\big)_{nn'} - \big(\varphi s_{{\alpha}}\tau_{{\alpha}}D_{{\alpha}}\big)_{nn'}
\frac{X_{n'}^{{\alpha}}- X_n^{{\alpha}}}{d_{n'}+d_n},\end{align}
 with
perpendicular distances to interface $nn'$ from nodes $n$ and $n'$
denoted by $d_{n'}$ and $d_n$, respectively. Upstream weighting is used
for the advective term
\begin{align}
\big(q_{{\alpha}}X_{{\alpha}})_{nn'} {\ =\ }\left\{
\begin{array}{ll}
q_{nn'}^{{\alpha}}X_{n'}, & q_{nn'} > 0\\
q_{nn'}^{{\alpha}}X_{n}, &  q_{nn'} < 0
\end{array}
\right..\end{align}
 Depending on the type of source/sink term, the finite volume
discretization has the form \begin{align}
\int_{V_n}Q_j\, dV {\ =\ }Q_{jn} V_n,\end{align}

for reaction rates that are distributed continuously over a control
volume, or for a well with point source
$Q_j {\ =\ }\widehat Q_j \delta({\boldsymbol{r}}-{\boldsymbol{r}}_0)$:
\begin{align}
\int_{V_n}Q_j\, dV {\ =\ }\widehat Q_{jn}.\end{align}

Fully Implicit Newton-Raphson Iteration with Linear and Logarithm Update
------------------------------------------------------------------------

In a fully implicit formulation the nonlinear equations for the residual
function ${\boldsymbol{R}}$ given by
\begin{align}
{\boldsymbol{R}}({\boldsymbol{x}}) {\ =\ }{\boldsymbol{0}},\end{align}
 are
solved using an iterative solver based on the Newton-Raphson equations
\begin{align}
{\boldsymbol{J}}^{(i)} \delta{\boldsymbol{x}}^{(i+1)} {\ =\ }-{\boldsymbol{R}}^{(i)},\end{align}

at the $i$th iteration. Iteration stops when
\begin{align}
\left|{\boldsymbol{R}}^{(i+1)}\right| < \epsilon,\end{align}
 or if
\begin{align}
\big|\delta{\boldsymbol{x}}^{(i+1)}\big| < \delta.\end{align}
 However, the
latter criteria does not necessarily guarantee that the residual
equations are satisfied. The solution is updated from the relation
\begin{align}
{\boldsymbol{x}}^{(i+1)} {\ =\ }{\boldsymbol{x}}^{(i)} + \delta{\boldsymbol{x}}^{(i+1)}.\end{align}

For the logarithm of the concentration with
${\boldsymbol{x}}=\ln{\boldsymbol{y}}$, the solution is updated
according to

\begin{align}
\ln{\boldsymbol{y}}^{(i+1)} {\ =\ }\ln{\boldsymbol{y}}^{(i)} + \delta\ln{\boldsymbol{y}}^{(i+1)},\end{align}


or
\begin{align}
{\boldsymbol{y}}^{(i+1)} {\ =\ }{\boldsymbol{y}}^{(i)} {\rm e}^{\delta\ln{\boldsymbol{y}}^{(i+1)}}.\end{align}


### Example

To illustrate the logarithmic update formulation the simple linear
equation \begin{align}
x{\ =\ }x_0,\end{align}
 is considered. The residual function is given
by \begin{align}
R {\ =\ }x - x_0,\end{align}
 with Jacobian
\begin{align}
J {\ =\ }\frac{{{\partial}}R}{{{\partial}}x} {\ =\ }I.\end{align}
 In the linear
formulation the Newton-Raphson equations are given by
\begin{align}
J\delta x &{\ =\ }-R,\\
\delta x &{\ =\ }-(x-x_0)\\
x{'} &{\ =\ }x + \delta x {\ =\ }x_0.\end{align}
 In the logarithmic formulation
the Jacobian is given by
\begin{align}
J{\ =\ }\frac{{{\partial}}R}{{{\partial}}\ln x} {\ =\ }x,\end{align}
 and the
Newton-Raphson equations are now nonlinear becoming
\begin{align}
J^i\delta \ln x^{i+1} {\ =\ }-R^i,\end{align}
 with the solution update
\begin{align}
\ln x^{i+1} {\ =\ }\ln x^i + \delta \ln x^{i+1},\end{align}
 or
\begin{align}
x^{i+1} {\ =\ }x^i {{\rm{e}}}^{\delta \ln x^{i+1}}.\end{align}
 It follow that
\begin{align}
x^i \delta \ln x^{i+1} {\ =\ }-(x^i-x_0),\end{align}
 with the solution
\begin{align}
\delta \ln x^{i+1} {\ =\ }\frac{x_0-x^i}{x^i},\end{align}
 and thus
\begin{align}
x^{i+1} {\ =\ }x^i \exp \left(\frac{x_0- x^{i}}{x^i}\right).\end{align}
 Given
that a solution $x$ exists it follows that
\begin{align}
\lim_{i\rightarrow\infty} x^{i} &\rightarrow x,\\
\lim_{i\rightarrow\infty} \frac{x^{i+1}}{x^{i}} &\rightarrow 1,\\
\lim_{i\rightarrow\infty} \exp \left(\frac{x_0- x^{i}}{x^i}\right) &\rightarrow 1,\\
\lim_{i\rightarrow\infty} x^{i} &\rightarrow x_0.\end{align}

### Multirate Sorption

The residual function incorporating the multirate sorption model can be
further simplified by solving analytically the finite difference form of
kinetic sorption equations. This is possible when these equations are
linear in the sorbed concentration $S_{j{{\alpha}}}$ and because they do
not contain a flux term. Thus discretizing Eqn.\ref{sja} in time using the fully
implicit backward Euler method gives
\begin{align}
\frac{S_{j{{\alpha}}}^{t+\Delta t}-S_{j{{\alpha}}}^t}{\Delta t} {\ =\ }k_{{\alpha}}^{} \big(f_{{\alpha}}^{} S_{j{{\alpha}}}^{\rm eq} - S_{j{{\alpha}}}^{t+\Delta t}\big).\end{align}

Solving for $S_{j{{\alpha}}}^{t+\Delta t}$ yields \begin{align}
\label{sjadt}
S_{j{{\alpha}}}^{t+\Delta t} {\ =\ }\frac{S_{j{{\alpha}}}^t + k_{{\alpha}}^{} \Delta t f_{{\alpha}}^{} S_j^{\rm eq}}{1+k_{{\alpha}}\Delta t}.\end{align}

From this expression the reaction rate can be calculated as
\begin{align}
\frac{S_{j{{\alpha}}}^{t+\Delta t}-S_{j{{\alpha}}}^t}{\Delta t} {\ =\ }\frac{k_{{\alpha}}}{1+k_{{\alpha}}\Delta t} \big(f_{{\alpha}}^{} S_{j{{\alpha}}}^{\rm eq} - S_{j{{\alpha}}}^t\big).\end{align}

The right-hand side of this equation is a known function of the solute
concentration and thus by substituting into Eqn.\ref{totj} eliminates the
appearance of the unknown sorbed concentration. Once the transport
equations are solved over a time step, the sorbed concentrations can be
computed from Eqn.\ref{sjadt}.

Operator Splitting
------------------

Operator splitting involves splitting the reactive transport equations
into a nonreactive part and a part incorporating reactions. This is
accomplished by writing Eqns.\ref{rteqn} as the two coupled equations
\begin{align}
\frac{{{\partial}}}{{{\partial}}t}\big(\varphi \sum_{{\alpha}}s_{{\alpha}}\Psi_j^{{\alpha}}\big) +
\nabla\cdot\sum_{{\alpha}}\big({\boldsymbol{q}}_{{\alpha}}- \varphi s_{{\alpha}}{\boldsymbol{D}}_{{\alpha}}{\boldsymbol{\nabla}}\big)\Psi_j^{{\alpha}}{\ =\ }Q_j,\end{align}

and
\begin{align}
\frac{d}{d t}\big(\varphi \sum_{{\alpha}}s_{{\alpha}}\Psi_j^{{\alpha}}\big) {\ =\ }- \sum_m\nu_{jm} I_m -\frac{{{\partial}}S_j}{{{\partial}}t},\end{align}

The first set of equations are linear in $\Psi_j$ (for
species-independent diffusion coeffients) and solved over over a time
step $\Delta t$ resulting in $\Psi_j^*$. The result for $\Psi_j^*$ is
inverted to give the concentrations $C_j^*$ by solving the equations
\begin{align}
\Psi_j^* {\ =\ }C_j^* + \sum_i \nu_{ji} C_i^*,\end{align}
where the secondary species concentrations $C_i^{*}$ are nonlinear functions of the primary
species concentrations $C_j^{*}$. With this result the second set of
equations are solved implicitly for $C_j$ at $t+\Delta t$ using
$\Psi_j^*$ for the starting value at time $t$.

### Constant $K_d$

As a simple example of operator splitting consider a single component
system with retardation described by a constant $K_d$. According to this
model the sorbed concentration $S$ is related to the aqueous
concentration by the linear equation \begin{align}
\label{skd}
S {\ =\ }K_d C.\end{align}
 The governing equation is given by
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi C + {\boldsymbol{\nabla}}\cdot\big({\boldsymbol{q}}C -\varphi D {\boldsymbol{\nabla}}C\big) {\ =\ }-\frac{{{\partial}}S}{{{\partial}}t}.\end{align}

If $C(x,\,t;\, {\boldsymbol{q}},\,D)$ is the solution to the case with
no retardation (i.e. $K_d=0$), then
$C(x,\,t;\, {\boldsymbol{q}}/R,\,D/R)$ is the solution with retardation
$(K_d>0)$, with \begin{align}
R = 1+\frac{1}{\varphi}K_d.\end{align}
 Thus propagation of a
front is retarded by the retardation factor $R$.

In operator splitting form this equation becomes
\begin{align}
\frac{{{\partial}}}{{{\partial}}t} \varphi C + {\boldsymbol{\nabla}}\cdot\big({\boldsymbol{q}}C -\varphi D {\boldsymbol{\nabla}}C\big) {\ =\ }0,\end{align}

and \begin{align}
\frac{d}{d t} \varphi C {\ =\ }-\frac{d S}{d t}.\end{align}
 The solution to
the latter equation is given by
\begin{align}
\varphi C^{t+\Delta t} - \varphi C^* {\ =\ }-\big(S^{t+\Delta t} - S^t\big),\end{align}

where $C^*$ is the solution to the nonreactive transport equation. Using
Eqn.\ref{skd}, this result can be written as
\begin{align}
C^{t+\Delta t} {\ =\ }\frac{1}{R} C^* + \left(1-\frac{1}{R}\right) C^t.\end{align}

Thus for $R=1$, $C^{t+\Delta t}=C^*$ and the solution advances
unretarded. As $R\rightarrow\infty$, $C^{t+\Delta t} \rightarrow C^t$
and the front is fully retarded.

Appendix C: Reaction Sandbox {#appendix-c-reaction-sandbox }
============================

Background
----------

Researchers often have a suite of reactions tailored to a unique problem
scenario, but these reaction networks only exist in their respective
research codes. The “reaction sandbox” provides these researchers with a
venue for implementing user-defined reactions within PFLOTRAN. Reaction
networks developed within the reaction sandbox can leverage existing
biogeochemical capability within PFLOTRAN (e.g. equilibrium aqueous
complexation, mineral precipitation–dissolution, etc.) or function
independently. Please note that although the reaction sandbox
facilitates the integration of user-defined reactions, the process still
requires a basic understanding of PFLOTRAN and its approach to solving
reaction through the Newton-Raphson method. For instance, one must
understand the purpose and function of the rt\_auxvar and global\_auxvar
objects.

Implementation
--------------

The core framework of reaction sandbox leverages Fortran 2003
object–oriented extendable derived types and methods and consists of two
modules:

-   Reaction\_Sandbox\_module (reaction\_sandbox.F90)

-   Reaction\_Sandbox\_Base\_class (reaction\_sandbox\_base.F90).

To implement a new reaction within the reaction sandbox, one creates a
new class by extending the Reaction\_Sandbox\_Base\_class and adds the
new class to the Reaction\_Sandbox\_module. The following steps
illustrate this process through the creation of the class
Reaction\_Sandbox\_Example\_class that implements a first order decay
reaction.

1.  Copy reaction\_sandbox\_template.F90 to a new filename (e.g.
    reaction\_sandbox\_example.F90).

2.  Replace all references to Template/template with the new
    reaction name.

    -   `Template` $\rightarrow$ `Example`

    -   `template` $\rightarrow$ `example`

3.  Add necessary variables to the module and/or the extended
    derived type.

        character(len=MAXWORDLENGTH) :: species_name
        PetscInt :: species_id
        PetscReal :: rate_constant

4.  Add the necessary functionality within the following subroutines:

    1.  ExampleCreate: Allocate the reaction object, initializing all
        variables to zero and nullifying arrays. **Be sure to
        nullify ExampleCreate%next which comes from the
        base class.** E.g.,

              allocate(ExampleCreate)
              ExampleCreate%species_name = ''
              ExampleCreate%species_id = 0
              ExampleCreate%rate_constant = 0.d0
              nullify(ExampleCreate%next)


    2.  ExampleRead: Read parameters in from the input file block
        `EXAMPLE`. E.g.,

              ...
              case('SPECIES_NAME')
                call InputReadWord(input,option,this%species_name, &
                                   PETSC_TRUE)
                call InputErrorMsg(input,option,'species_name', &
                               'CHEMISTRY,REACTION_SANDBOX,EXAMPLE')
              ...


    3.  ExampleSetup: Construct the reaction network (e.g. array
        allocation, establishing linkages, etc.). E.g.,

              ...
              this%species_id = &
                GetPrimarySpeciesIDFromName(this%species_name, &
                                            reaction,option)
              ...


    4.  ExampleReact: Calculate contribution of reaction to the residual
        (units = moles/sec) and Jacobian (units = kg water/sec). E.g.,

              ...
              Residual(this%species_id) = &
                Residual(this%species_id) - &
                this%rate_constant*porosity* &
                global_auxvar%sat(iphase)*volume*1.d3* &
                rt_auxvar%total(this%species_id,iphase)
              ...
              Jacobian(this%species_id,this%species_id) = &
              Jacobian(this%species_id,this%species_id) + &
                this%rate_constant*porosity* &
                global_auxvar%sat(iphase)*volume*1.d3*
                rt_auxvar%aqueous%dtotal(this%species_id, &
                                         this%species_id,iphase)
              ...


    5.  ExampleDestroy: Deallocate any dynamic memory within the class
        (without deallocating the object itself).

5.  Ensure that the methods within the extended derived type point to
    the proper procedures in the module

          procedure, public :: ReadInput => ExampleRead
          procedure, public :: Setup => ExampleSetup
          procedure, public :: Evaluate => ExampleReact
          procedure, public :: Destroy => ExampleDestroy


6.  Within reaction\_sandbox.F90:

    1.  Add Reaction\_Sandbox\_Example\_class+ to the list of modules to
        be “used” at the top of the file.

    2.  Add a case statement in RSandboxRead2 for the keyword defining
        the new reaction and create the reaction within. I.e.

                case('EXAMPLE')
                  new_sandbox => ExampleCreate()

Appendix D: Python Tools {#appendix-d-python-tools }
========================

Extracting Aqueous Secondary Species, Minerals and Gases from a Thermodynamic Database
--------------------------------------------------------------------------------------

A python script is available to help the user extract secondary species,
gases and minerals from the thermodynamic database for a given set of
primary species. Surface complexation reactions are not included. The
python script can be found in
`./tools/contrib/sec_species/rxn.py` in the PFLOTRAN
mercurial repository. The current implementation is based on the
`hanford.dat` database. Input files are
`aq_sec.dat`, `gases.dat` and
`minerals.dat`. In addition, for each of these files there is
a corresponding file containing a list of species to be skipped:
`aq_skip.dat`, `gas_skip.dat` and
`min.dat`. Before running the script it is advisable to copy
the entire directory `sec_species` to the local hard drive
to avoid conflicts when updating the PFLOTRAN repository. To run the
script simply type in a terminal window:

`python rxn.py`

The user has to edit the `rxn.py` file to set the list of
primary species. For example,

`pri=[’Fe++’,’Fe+++’,’H+’,’H2O’]`

Note that the species H2O must be include in the list of primary
species. Output appears on the screen and also in the file
`chem.out`, a listing of which appears below. The number of
primary and secondary species, gases and minerals is printed out at the
end of the `chem.out` file.

`chem.out`

    PRIMARY_SPECIES
    Fe++
    Fe+++
    H+
    H2O
    /
    SECONDARY_SPECIES
    O2(aq)
    H2(aq)
    Fe(OH)2(aq)
    Fe(OH)2+
    Fe(OH)3(aq)
    Fe(OH)3-
    Fe(OH)4-
    Fe(OH)4--
    Fe2(OH)2++++
    Fe3(OH)4(5+)
    FeOH+
    FeOH++
    HO2-
    OH-
    /
    GASES
    H2(g)
    H2O(g)
    O2(g)
    /
    MINERALS
    Fe
    Fe(OH)2
    Fe(OH)3
    FeO
    Ferrihydrite
    Goethite
    Hematite
    Magnetite
    Wustite
    /
    ================================================
    npri =  4  nsec =  14  ngas =  3  nmin =  9

    Finished!
